using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;


public static class AppData {
	
	private static Dictionary<int, long> m_hardCodedIDS;
	
	private static String m_cmmsUrl;
	
	private const String DATA_URL = "abesupport.myvnc.com:8085";
	
	#if (UNITY_EDITOR)
	public const String FS_SEP = "\\";
	#elif (UNITY_ANDROID)
	public const String FS_SEP = "/";
	#endif
	
	
static AppData()
{
	Popups = new Stack<string>();	
		
	m_cmmsUrl = "";	
		
	CMMS_URL = "abesupport.myvnc.com:8082";
		
	
				
	#if (UNITY_EDITOR)	
	#elif (UNITY_ANDROID)	
	OpenFilePluginAPI.Initialise();	
	#endif		
	
	m_hardCodedIDS = new Dictionary<int, long>();
		
	m_hardCodedIDS.Add(0, 111203);
	m_hardCodedIDS.Add(1, 110208);
	m_hardCodedIDS.Add(2, 111177);
	m_hardCodedIDS.Add(3, 111226);
	m_hardCodedIDS.Add(4, 110448);
	m_hardCodedIDS.Add(5, 110451);
	m_hardCodedIDS.Add(6, 110466);
	m_hardCodedIDS.Add(7, 110471);
	m_hardCodedIDS.Add(8, 110250);
	m_hardCodedIDS.Add(9, 110431);
	m_hardCodedIDS.Add(10, 110435);
	m_hardCodedIDS.Add(11, 110438);
	m_hardCodedIDS.Add(12, 110455);
	m_hardCodedIDS.Add(13, 110458);
	m_hardCodedIDS.Add(14, 110461);
	m_hardCodedIDS.Add(15, 110474);
	m_hardCodedIDS.Add(16, 110477);
	m_hardCodedIDS.Add(17, 110860);
	m_hardCodedIDS.Add(18, 110863);
	m_hardCodedIDS.Add(19, 110866);
	m_hardCodedIDS.Add(20, 110869);
	m_hardCodedIDS.Add(21, 110872);
	m_hardCodedIDS.Add(22, 110875);
	m_hardCodedIDS.Add(23, 110857);		
}
	
	
public static void Clear()
{
	Popups.Clear();
		
	GuiStuff_DisplayEndMsg = false;	
		
	GuiStuff_ClearMiddle_NEEDED = false;
		
	AllowTracking = true;	
}
	
	
public static bool GuiStuff_DisplayEndMsg
{
	get;
	set;
}
	
	
public static bool GuiStuff_ClearMiddle_NEEDED
{
	get;
	set;
}
	
	
public static bool AllowTracking
{
	get;
	set;
}
	
	
public static Stack<String> Popups
{
	get;
	private set;
}
	
	
public static int HardCodedCount
{
	get 
	{
		return 7; 
		//return m_hardCodedIDS.Count; 
	}
}
		
	
public static String ASSET_URL(int id)
{
	return "http://" + CMMS_URL + "/Api/Assets?AssetQRCodeID=" + id.ToString(); 
	//return GetCMMSurl(id);	
}
	

private static String GetCMMSurl(int arId)
{
	long cmmsId = (m_hardCodedIDS.ContainsKey(arId) ? m_hardCodedIDS[arId] : 110208);
		
	return "http://" + CMMS_URL + "/Api/Assets/" + cmmsId.ToString();	
}
	
	
public static String ATTACHMENT_URL(String path)
{
	return "http://" + DATA_URL + "/" + path;	
}

	
/*	
public static String ATTACHMENT_URL(MI_Attachment att)
{
	return ((att.The_Type == MI_Attachment.Type.Text) || (att.The_Type == MI_Attachment.Type.Audio)) ? 
			att.The_Path : "http://" + CMMS_URL + "/" + att.The_Path;	
}
*/
	
public static String TASKS_PER_ASSET_URL(String id)
{
	return "http://" + CMMS_URL	+ "/api/Tasks?$filter=TargetID eq " + id;
		
	//?$filter=TargetID eq " + id.ToString(); //not supported with unity WWW
}

	
public static String CMMS_URL
{
	get 
	{ 
		if ((m_cmmsUrl == "") && File.Exists(Application.persistentDataPath + AppData.FS_SEP + "url.txt"))
		{	
			try
			{
				m_cmmsUrl = File.ReadAllText(Application.persistentDataPath + AppData.FS_SEP + "url.txt");
			}
			catch (Exception e)
			{
				AppData.Popups.Push("Failed to retrieve server url!" + "\n" + e.Message + "\n" + e.StackTrace);	
			}
		}
			
		return m_cmmsUrl;	
	}
	set
	{
		m_cmmsUrl = value;	
			
		m_cmmsUrl = (m_cmmsUrl == null) ? "" : m_cmmsUrl;	
			
		try
		{
			File.WriteAllText(Application.persistentDataPath + AppData.FS_SEP + "url.txt", m_cmmsUrl);	
		}
		catch (Exception e)
		{
			AppData.Popups.Push("Failed to store server url!" + "\n" + e.Message + "\n" + e.StackTrace);	
		}
	}
}
	
	
public static void ScenarioExit()
{
	WebDownloader.CancelCurrentDownload();
	
	if (WebDownloader.NativeDownloader != null)
	{
		WebDownloader.NativeDownloader.CancelCurrentDownload();	
	}
	
	WebDownloader.Clear();
	//MI_Asset_Cache.Clear();
	AppData.Clear();
	TrackedRegistry.Clear();
	Pipe_StateRegistry.Clear();
	Boiler_StateRegistry.Clear();
	
	Application.LoadLevel("menuScene");	
}
	
	
public static void AppExit()
{
	WebDownloader.CancelCurrentDownload();
		
	if (WebDownloader.NativeDownloader != null)
	{
		WebDownloader.NativeDownloader.CancelCurrentDownload();	
	}
		
	WebDownloader.Clear();
	MI_Asset_Cache.Clear();
	AppData.Clear();
	TrackedRegistry.Clear();
	Pipe_StateRegistry.Clear();
	Boiler_StateRegistry.Clear();	
		
	Application.Quit();	
}

	
}//class
