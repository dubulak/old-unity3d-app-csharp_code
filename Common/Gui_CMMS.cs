using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Net;


public class Gui_CMMS : MonoBehaviour {
	
	public Texture2D m_video_large_Img;
	public Texture2D m_pdf_large_Img;
	public Texture2D m_dwg_large_Img;
	public Texture2D m_generic_large_Img;
	
	public Texture2D m_remarks_Img;
	public Texture2D m_fileExplorer_Img;
	public Texture2D m_genericFile_Img;
	public Texture2D m_audioFile_Img;
	public Texture2D m_imageFile_Img;
	public Texture2D m_videoFile_Img;
	public Texture2D m_pdfFile_Img;
	public Texture2D m_dwgFile_Img;
	
	public Texture2D m_decoration_Img;
	public Texture2D m_audPlay_Img;
	public Texture2D m_audStop_Img;
	public Texture2D m_audPause_Img;
	public Texture2D m_appIconSmall_Img;
	public Texture2D m_play_Img;
	public Texture2D m_run_Img;
	public Texture2D m_server_Img;
	public Texture2D m_download_Img;
	public Texture2D m_track_Img;
	public Texture2D m_talk_Img;
	public Texture2D m_noTalk_Img;
	public Texture2D m_black_Img;
	public Texture2D m_exclamation_Img;
	public Texture2D m_red_Img;
	public Texture2D m_blue_Img;
	public Texture2D m_transparent_Img;
	
	public Font font8;
	public Font font10;
	public Font font12;
	public Font font14;
	public Font font16;
	public Font font18;
	public Font font20;
	public Font font22;
	public Font fontHD;
	
	public Texture2D m_arrowUp_Img;
	public Texture2D m_arrowDown_Img;
	public Texture2D m_arrowRight_Img;
	public Texture2D m_arrowLeft_Img;
	public Texture2D m_quit_Img;
	public Texture2D m_greenTick_Img;
	public Texture2D m_redX_Img;
	
	public GUISkin guiSkin;
	
	private Rect m_titleLbl_Rect;
	private Rect m_quitBtn_Rect;
	private Rect m_machineDetailsBtn_Rect;
	private Rect m_attachedFilesBtn_Rect;
	private Rect m_pendingWorkOrdersBtn_Rect;
	private Rect m_scrollView_Rect;
	
	private Rect m_confQuitBox_Rect;
	private Rect m_confYesBtn_Rect;
	private Rect m_confNoBtn_Rect;
		
	private const float GUI_ELEM_WIDTH_PCNT = 0.25f;
	private const float GUI_ELEM_HEIGHT_PCNT = 0.075f;
	private const float GUI_MARGIN_PCNT = 0.012f;
	
	private bool m_confirmQuit;
	
	private MI_Asset m_currAsset;
	
	private bool m_showing_MachineDetails;
	private bool m_showing_Attachments;
	private bool m_showing_WorkOrders;
	private bool m_showing_WorkOrderContent;
	private bool m_showing_AttachmentContent;
	
	private int m_currWorkOrder_Index;
	private int m_currAttachment_Index;
	
	private const float SMALL_FONT_COEFF = 0.012f;
	private const float STD_FONT_COEFF = 0.015625f;
	private const float VIEW_FONT_COEFF = 0.025f;
	private const float TITLE_FONT_COEFF = 0.02f;
	
	private Vector2 m_scrollPosition;
	
	private MachineDetails_Visitor m_machineDetailsVisitor;
	private WorkOrder_Visitor m_workOrderVisitor;
	
	private GUIStyle m_viewBoxStyle;
	private GUIStyle m_entryBtnStyle;
	private GUIStyle m_titleBoxStyle;
	private GUIStyle m_popupBtnStyle;
	private GUIStyle m_transpEntryStyle;
		
	private int m_testIndex;
	
	private bool m_allowPopup;
		
	private GUIStyle m_allowPopup_Style;
	
	private float m_allowPopup_prevTime;
	
	private Texture2D m_regular_Img;
	
	private Rect m_actOnFile_Rect;
	
	private Texture2D m_fullScreenImage;
	
	private GUIStyle m_fullScreenBtnStyle;
	
	private GUIStyle m_serverBtnStyle;
	private GUIStyle m_trackBoxStyle;
	private GUIStyle m_downloadBoxStyle;
	
	private float m_audioSliderValue;
	
	private String m_inputString;
	
	private bool m_textInput;
	
	private GUIStyle m_textViewBoxStyle;
	
	private Dictionary<String, Texture2D> m_icons;
	private Dictionary<String, Texture2D> m_largeIcons;
	
	private GUIStyle m_thumbStyle;
	
	private const float LEFT_EDGE = 0.25f + (2 * GUI_MARGIN_PCNT);
	
	
private Dictionary<int, bool> ShowCMMSinfo
{
	get;
	set;
}
	

void Start() 
{	
	ShowCMMSinfo = new Dictionary<int, bool>();
		
	for (int i = 0; i < 512; i++)
	{
		ShowCMMSinfo.Add(i, false);	
	}
		
	m_largeIcons = new Dictionary<string, Texture2D>();
		
	m_largeIcons.Add("Video", m_video_large_Img);
	m_largeIcons.Add("PDF", m_pdf_large_Img);
	m_largeIcons.Add("AutoCAD", m_dwg_large_Img);
		
	m_icons = new Dictionary<string, Texture2D>();
		
	m_icons.Add("Audio", m_audioFile_Img);
	m_icons.Add("Video", m_videoFile_Img);
	m_icons.Add("PDF", m_pdfFile_Img);
	m_icons.Add("Image", m_imageFile_Img);
	m_icons.Add("AutoCAD", m_dwgFile_Img);
		
	m_textInput = false;
		
	m_inputString = "";
		
	m_audioSliderValue = 0;	
		
	m_fullScreenImage = null;	
		
	m_allowPopup_prevTime = Time.time;	
		
	m_allowPopup = false;	
		
	m_testIndex = 0;	
		
	m_workOrderVisitor = new WorkOrder_Visitor();	
	m_machineDetailsVisitor = new MachineDetails_Visitor();	
		
	m_scrollPosition = Vector2.zero;
		
	m_titleBoxStyle = new GUIStyle(guiSkin.GetStyle("Box"));
	m_titleBoxStyle.font = GetFontForSize(TITLE_FONT_COEFF);
	m_titleBoxStyle.normal.background = m_black_Img;
		
	m_textViewBoxStyle = new GUIStyle(guiSkin.GetStyle("Box"));
	m_textViewBoxStyle.alignment = TextAnchor.UpperLeft;	
	
	m_entryBtnStyle = new GUIStyle(guiSkin.GetStyle("Button"));
	m_entryBtnStyle.font = GetFontForSize(STD_FONT_COEFF);	
		
	m_thumbStyle = new GUIStyle(guiSkin.GetStyle("Button"));
	m_thumbStyle.font = GetFontForSize(STD_FONT_COEFF);
	m_thumbStyle.imagePosition = ImagePosition.ImageAbove;		
		
	m_fullScreenBtnStyle = new GUIStyle(guiSkin.GetStyle("Button"));	
		
	m_transpEntryStyle = new GUIStyle(guiSkin.GetStyle("Box"));	
	m_transpEntryStyle.font = GetFontForSize(STD_FONT_COEFF);
	m_transpEntryStyle.normal.background = m_transparent_Img;	
		
	m_viewBoxStyle = new GUIStyle(guiSkin.GetStyle("Box"));
	m_viewBoxStyle.alignment = TextAnchor.MiddleLeft;	
	m_viewBoxStyle.font = GetFontForSize(STD_FONT_COEFF);
		
	m_popupBtnStyle = new GUIStyle(guiSkin.GetStyle("Button"));
	m_popupBtnStyle.alignment = TextAnchor.MiddleLeft;
	m_popupBtnStyle.imagePosition = ImagePosition.ImageLeft;
	m_popupBtnStyle.font = GetFontForSize(VIEW_FONT_COEFF);	
	m_popupBtnStyle.normal.background = m_red_Img;
		
	m_serverBtnStyle = new GUIStyle(guiSkin.GetStyle("Button"));	
		
	m_trackBoxStyle = new GUIStyle(guiSkin.GetStyle("Box"));
		
	m_downloadBoxStyle = new GUIStyle(guiSkin.GetStyle("Box"));	
		
	m_allowPopup_Style = new GUIStyle(guiSkin.GetStyle("Button"));
	m_regular_Img = m_allowPopup_Style.normal.background;	
		
	m_showing_WorkOrders = false;
	m_showing_Attachments = false;
	m_showing_MachineDetails = false;	
	m_showing_WorkOrderContent = false;	
	m_showing_AttachmentContent = false;	
		
	m_currAsset = null;	
		
	m_confirmQuit = false;	
		
	m_confQuitBox_Rect = new Rect(
			Screen.width * 0.37f, 
			Screen.height * 0.4f, 
			Screen.width * GUI_ELEM_WIDTH_PCNT, 
			Screen.height * GUI_ELEM_HEIGHT_PCNT * 1.5f);
	
	m_confYesBtn_Rect = new Rect(
			Screen.width * 0.405f, 
			Screen.height * 0.52f, 
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.35f, 
			Screen.height * GUI_ELEM_HEIGHT_PCNT * 1.25f);
	
	m_confNoBtn_Rect = new Rect(
			Screen.width * 0.505f, 
			Screen.height * 0.52f, 
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.35f, 
			Screen.height * GUI_ELEM_HEIGHT_PCNT * 1.25f);	
					
	m_titleLbl_Rect = new Rect(
			Screen.width * 0.5f - (Screen.width * GUI_ELEM_WIDTH_PCNT * 0.5f), 
			Screen.height * (GUI_MARGIN_PCNT), 
			Screen.width * GUI_ELEM_WIDTH_PCNT, 
			Screen.height * GUI_ELEM_HEIGHT_PCNT * 0.5f);	

	m_quitBtn_Rect = new Rect(
			Screen.width * (1 - GUI_MARGIN_PCNT - (GUI_ELEM_WIDTH_PCNT * 0.4f)), 
			Screen.height * 0.1f, 
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.4f, 
			Screen.height * GUI_ELEM_HEIGHT_PCNT);	
	
	m_machineDetailsBtn_Rect = new Rect(
			Screen.width * (1 - (GUI_ELEM_WIDTH_PCNT * 0.55f) - GUI_MARGIN_PCNT),
			Screen.height * 0.25f,
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.55f,
			Screen.height * GUI_ELEM_HEIGHT_PCNT);
		
	m_attachedFilesBtn_Rect = new Rect(
			m_machineDetailsBtn_Rect.x,
			m_machineDetailsBtn_Rect.yMax + (Screen.height * GUI_MARGIN_PCNT),
			m_machineDetailsBtn_Rect.width,
			m_machineDetailsBtn_Rect.height);
		
	m_pendingWorkOrdersBtn_Rect = new Rect(
			m_attachedFilesBtn_Rect.x,
			m_attachedFilesBtn_Rect.yMax + (Screen.height * GUI_MARGIN_PCNT),
			m_machineDetailsBtn_Rect.width,
			m_machineDetailsBtn_Rect.height);
		
	m_scrollView_Rect = new Rect(
			Screen.width * LEFT_EDGE,
			m_machineDetailsBtn_Rect.y,
			Screen.width * 0.4f,
			Screen.height * 0.56f);		
}
	
	
private Texture2D GetIcon(String typeSTR)
{
	return m_icons.ContainsKey(typeSTR) ? m_icons[typeSTR] : m_genericFile_Img;	
}
	
	
private Texture2D GetLargeIcon(String typeSTR)
{
	return m_largeIcons.ContainsKey(typeSTR) ? m_largeIcons[typeSTR] : m_generic_large_Img;	
}
				
	
public bool Get_ConfirmQuit()
{
	return m_confirmQuit;	
}
	
	
void GetTextInput()
{
	GUIContent labelGC = new GUIContent("  Server: ", m_server_Img);
		
	Vector2 labelSize = m_trackBoxStyle.CalcSize(labelGC);
		
	Rect labelRect = new Rect(
			Screen.width * 0.1f,
			Screen.height * 0.45f,
			labelSize.x + 10,
			labelSize.y + 10);
		
	GUI.Box(labelRect, labelGC, m_trackBoxStyle);	
		
	GUI.SetNextControlName("TEXTINPUT");
	
	Rect textRect = new Rect(
				Screen.width * 0.2f, 
				Screen.height * 0.45f, 
				Screen.width * 0.6f, 
				labelRect.height);
			
	m_inputString = GUI.TextField(
			textRect, 
			m_inputString, 
			40);
				
	GUI.FocusControl("TEXTINPUT");		
		
	GUIContent okGC = new GUIContent("  OK  ", m_greenTick_Img);
		
	Vector2 okSize = m_entryBtnStyle.CalcSize(okGC);
		
	Rect okRect = new Rect(
			textRect.xMax - (okSize.x + 10),
			textRect.yMax + (Screen.height * GUI_MARGIN_PCNT),
			okSize.x + 10,
			okSize.y + 10);
		
	if (GUI.Button(okRect, okGC, m_entryBtnStyle))
	{
		AppData.CMMS_URL = m_inputString;
			
		m_inputString = "";	
			
		m_textInput = false;	
	}
		
	GUIContent cancelGC = new GUIContent("  Cancel  ", m_redX_Img);	
		
	Vector2 cancelSize = m_entryBtnStyle.CalcSize(cancelGC);
		
	Rect cancelRect = new Rect(
			okRect.x - (cancelSize.x + 10) - (Screen.width * GUI_MARGIN_PCNT),
			okRect.y,
			cancelSize.x + 10,
			cancelSize.y + 10);
		
	if (GUI.Button(cancelRect, cancelGC, m_entryBtnStyle))
	{
		m_inputString = "";	
			
		m_textInput = false;	
	}
}
	
	
private void LoadFakeAsset(int arId)
{
	m_currAsset = new MI_Asset(arId);	
		
	MI_Asset_Cache.Add(m_currAsset);		
			
	WebDownloader.DownloadAssetInfo(m_currAsset);	
}
	
	
public void OnTrackableChange()
{
	if (TrackedRegistry.Tracked_ID != TrackedRegistry.INVALID_ID)
	{
		HandleMarkerTracked(TrackedRegistry.Tracked_ID);	
	}
}
	
	
void HandleMarkerTracked(int arId)
{
	m_showing_AttachmentContent = false;
	m_showing_Attachments = false;
	m_showing_MachineDetails = false;
	m_showing_WorkOrderContent = false;
	m_showing_WorkOrders = false;	
			
	m_currAsset = MI_Asset_Cache.Get(arId);
		
	if (m_currAsset == null)
	{
		m_currAsset	= new MI_Asset(arId);
			
		MI_Asset_Cache.Add(m_currAsset);	
	}
			
	if (!m_currAsset.Complete() && ShowCMMSinfo[arId])
	{
		WebDownloader.DownloadAssetInfo(m_currAsset);	
	}		
}
	

void Update()
{
	WebDownloader.MainLoopOneRound();	
}
	
	
public bool Get_ViewingFullscreen()
{
	return (m_fullScreenImage != null);	
}
	
	
void OnGUI() 
{		
	GUI.skin = guiSkin;	
		
	SelectFont();	

	if (m_fullScreenImage != null)
	{
		GUI_FullScreenImage();	
			
		return;	
	}
			
	if (!m_confirmQuit)
	{
		if (m_currAsset != null)
		{
			GUI.enabled = !AppData.GuiStuff_DisplayEndMsg && !Get_ShowingPopup();
		
			if (ShowCMMSinfo[m_currAsset.AR_ID])
			{
				GUI_MachineDetails();
		
				GUI_AttachedFiles();
		
				GUI_PendingWorkOrders();
			}
			else
			{
				if (TrackedRegistry.Tracked_ID != TrackedRegistry.INVALID_ID)
				{
					GUI_DownloadInfoMsg();	
				}
			}
				
			GUI.enabled = true;			
		}	
			
		GUI_Popup();
		GUI_AllowPopup();	
		GUI_Status();	
	}
		
	GUI_Quit();			
}
	
	
void GUI_DownloadInfoMsg()
{
	GUIContent gc = new GUIContent("  Information available,\n  relative to this marker.\n  Press this button\n  to begin downloading.", m_download_Img);
	Vector2 size = guiSkin.GetStyle("Button").CalcSize(gc);			
		
	Rect rect = new Rect(
			(Screen.width * (1 - GUI_MARGIN_PCNT)) - (size.x + 20),
			m_attachedFilesBtn_Rect.y,
			size.x + 20,
			size.y + 20
			);
		
	if (GUI.Button(rect, gc))
	{
		ShowCMMSinfo[m_currAsset.AR_ID] = true;	
			
		if (!m_currAsset.Complete())
		{
			WebDownloader.DownloadAssetInfo(m_currAsset);	
		}		
	}
}
	
	
public bool Get_ShowingPopup()
{
	return (AppData.Popups.Count != 0) && m_allowPopup;
}
	
	
void JavaCallback(String msg)
{
	String[] tokens = msg.Split(new char[] { '/' });
		
	int id = TrackedRegistry.INVALID_ID;
		
	for (int i = 0; i < tokens.Length; i++)
	{	
		if (Int32.TryParse(tokens[i], out id))
		{
		 	MI_Asset asset = MI_Asset_Cache.Get(id);	
				
			if (asset != null)
			{
				MI_Attachment att = asset.Attachments.Find(x => x.LocalPath == msg);
					
				if (att != null)
				{
					att.LoadThumbnail();
						
					break;	
				}
				else
				{
					AppData.Popups.Push("Failed to retrieve attachment from path: " + msg);	
				}
			}
			else
			{
				AppData.Popups.Push("Failed to retrieve asset from path: " + msg);
			}
		}
	}
		
	if (id == TrackedRegistry.INVALID_ID)
	{
		AppData.Popups.Push("Failed to retrieve id from path: " + msg);	
	}
}
	
	
void GUI_FullScreenImage()
{
	GUIContent gc = new GUIContent(m_fullScreenImage);
		
	Vector2 size = m_fullScreenBtnStyle.CalcSize(gc);	
		
	Rect btnRect = new Rect(
			(Screen.width * 0.5f) - (size.x * 0.5f),
			(Screen.height * 0.5f) - (size.y * 0.5f),
			size.x,
			size.y
			);
	
	Rect scrollViewRect = new Rect(0, 0, Screen.width, Screen.height);
	scrollViewRect.y = (btnRect.yMax > Screen.height) ? 0 : btnRect.y;
	scrollViewRect.height = (btnRect.yMax > Screen.height) ? Screen.height : btnRect.height + 20;
	scrollViewRect.width = (btnRect.xMax > Screen.width) ? Screen.width : btnRect.width + 20;	
	scrollViewRect.x = (btnRect.xMax > Screen.width) ? 0 : btnRect.x;	
					
	m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, btnRect);
		
	if (GUI.Button(btnRect, gc, m_fullScreenBtnStyle))
	{
		m_fullScreenImage = null;	
	}
	
	GUI.EndScrollView();	
}
	
	
void GUI_Markers()
{
	for (int i = 0; i < 7; i++)
	{
		Rect r = new Rect(
			m_titleLbl_Rect.xMax + (Screen.width * GUI_MARGIN_PCNT) + (i * Screen.width * GUI_ELEM_WIDTH_PCNT * 0.1f),
			Screen.height * GUI_MARGIN_PCNT,
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.1f,
			Screen.height * GUI_ELEM_HEIGHT_PCNT);	
	
		if (GUI.Button(r, i.ToString()))
		{
			HandleMarkerTracked(i);
		}
	}
}
	
	
void GUI_MarkerCounter()
{
	Rect r = new Rect(
			m_titleLbl_Rect.xMax + (Screen.width * GUI_MARGIN_PCNT),
			Screen.height * GUI_MARGIN_PCNT,
			Screen.width * GUI_ELEM_WIDTH_PCNT * 0.3f,
			Screen.height * GUI_ELEM_HEIGHT_PCNT);	
	
	if (GUI.Button(r,  (m_currAsset != null) ? m_currAsset.AR_ID.ToString() : "<none>"))
	{
		LoadFakeAsset(m_testIndex);	
			
		m_testIndex++;
			
		m_testIndex %= AppData.HardCodedCount;		
	}
}
	
	
void GUI_Popup()
{
	if ((AppData.Popups.Count != 0) && m_allowPopup)
	{
		AppData.GuiStuff_ClearMiddle_NEEDED = true;	
			
		m_showing_WorkOrders = false;
		m_showing_AttachmentContent = false;
		m_showing_Attachments = false;
		m_showing_MachineDetails = false;
		m_showing_WorkOrderContent = false;
			
	 	GUIContent gc = new GUIContent(AppData.Popups.Peek().Clone().ToString(), m_exclamation_Img);
			
		Vector2 size = m_popupBtnStyle.CalcSize(gc);	
			
		Rect actualRect = new Rect(
				(Screen.width * 0.5f) - (size.x * 0.5f),
				(Screen.height * 0.5f) - (size.y * 0.5f),
				size.x,
				size.y);
			
		Rect scrollViewRect = new Rect(m_scrollView_Rect);
		scrollViewRect.y = actualRect.y;
		scrollViewRect.height = actualRect.height + 20;
		scrollViewRect.width = (actualRect.xMax > m_scrollView_Rect.xMax) ? m_scrollView_Rect.width : actualRect.width;	
		scrollViewRect.x = (actualRect.xMax > m_scrollView_Rect.xMax) ? m_scrollView_Rect.x : actualRect.x;
		
		m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, actualRect);
			
		if (GUI.Button(actualRect, gc, m_popupBtnStyle))
		{
			AppData.Popups.Pop();	
		}
			
		GUI.EndScrollView();	
	}
}
		
	
void GUI_AllowPopup()
{
	GUIContent gc = new GUIContent(m_allowPopup ? " Hide errors" : " Show errors", m_allowPopup ? m_noTalk_Img : m_talk_Img);	
		
	Vector2 size = m_popupBtnStyle.CalcSize(gc);
		
	Rect popupRect = new Rect(
			Screen.width * (1 - GUI_MARGIN_PCNT) - (size.x + 10),
			m_trackRect.y - (size.y + 10) - (Screen.height * GUI_MARGIN_PCNT),
			size.x + 10,
			size.y + 10);
		
	if (GUI.Button(popupRect, gc, m_allowPopup_Style))
	{
		m_allowPopup = !m_allowPopup;	
	}
			
	if (!m_allowPopup && (AppData.Popups.Count != 0))
	{
		if ((Time.time - m_allowPopup_prevTime > 1))
		{
			m_allowPopup_prevTime = Time.time;
				
			Texture2D old = m_allowPopup_Style.normal.background;	
				
			m_allowPopup_Style.normal.background = (old != m_red_Img) ? m_red_Img : m_regular_Img;	
		}
	}
		
	if (m_allowPopup)
	{
		m_allowPopup_Style.normal.background = m_regular_Img;	
	}
}
	

void GUI_Quit()
{
	if (m_confirmQuit)
	{	
		GUI.Box(m_confQuitBox_Rect, new GUIContent("    Quitting scenario.\n    Are you sure ?", m_quit_Img));	
		
		if (GUI.Button(m_confYesBtn_Rect, new GUIContent("  Yes", m_greenTick_Img)))
		{
			AppData.ScenarioExit();
		}
			
		if (GUI.Button(m_confNoBtn_Rect, new GUIContent("  No", m_redX_Img)))
		{
			m_confirmQuit = false;	
		}
	}
	else 
	{
		if (GUI.Button(m_quitBtn_Rect, new GUIContent("  Quit", m_quit_Img)))
		{
			m_confirmQuit = true;	
		}
	}	
}
	
	
private Rect m_trackRect;
void GUI_Status()
{
	/*	
	GUIContent serverCont = new GUIContent("  Server: " + AppData.CMMS_URL, m_server_Img);
		
	Vector2 serverSize = m_serverBtnStyle.CalcSize(serverCont);
		
	Rect serverRect = new Rect(
			Screen.width * GUI_MARGIN_PCNT,
			Screen.height * ((2 * GUI_MARGIN_PCNT) + GUI_ELEM_HEIGHT_PCNT),
			serverSize.x + 10,
			serverSize.y + 10);
		
	if (GUI.Button(serverRect, serverCont, m_serverBtnStyle))
	{
		m_textInput = true;	
			
		m_inputString = AppData.CMMS_URL;	
	}
	
	*/	
		
	GUIContent downCont = new GUIContent("  Downloading: " + 
			((WebDownloader.DownloadMsg != null) ? 
				(WebDownloader.DownloadMsg  + ": " + WebDownloader.Progress.ToString("F") + " %") :
			"<nothing>"), 
			m_download_Img);
		
	Vector2 downSize = m_downloadBoxStyle.CalcSize(downCont);	
		
	Rect downRect = new Rect(
			Screen.width * (1 - GUI_MARGIN_PCNT) - (downSize.x + 10),
			Screen.height * (1 - GUI_MARGIN_PCNT) - (downSize.y + 10),
			downSize.x + 10,
			downSize.y + 10
			);
		
	GUI.Box(downRect, downCont, m_downloadBoxStyle);	

	GUIContent trackCont = new GUIContent("Tracking: " + (AppData.AllowTracking ? (TrackedRegistry.HasTrackable() ? ("marker " + TrackedRegistry.Tracked_ID) : "nothing") : "DISABLED"), m_track_Img);	
		
	Vector2 trackSize = m_trackBoxStyle.CalcSize(trackCont);
		
	m_trackRect = new Rect(
			(Screen.width * (1 - GUI_MARGIN_PCNT)) - (trackSize.x + 10),
			downRect.y - (trackSize.y + 10),
			trackSize.x + 10,
			trackSize.y + 10);
		
	GUI.Box(m_trackRect, trackCont, m_trackBoxStyle);	

		
	//DrawDecor(serverRect.xMax, downRect.x, downRect.yMax - (downRect.height * 0.5f));
	
	//DrawDecor(downRect.xMax, trackRect.x, downRect.yMax - (downRect.height * 0.5f));	
}
	
	
void DrawDecor(float start, float end, float y)
{
	for (float i = start; i < end; i += m_decoration_Img.width)
	{
		GUI.DrawTexture(new Rect(
				i,
				y - (m_decoration_Img.height * 0.5f),
				(i + m_decoration_Img.width < end) ? m_decoration_Img.width : (end - i),
				m_decoration_Img.height
				), 
				m_decoration_Img);	
	}	
}
	
	
void GUI_MachineDetails()
{
	GUIContent btngc = new GUIContent("  Machine Details", m_showing_MachineDetails ? m_arrowLeft_Img : m_arrowRight_Img);
	
	float currProgress = m_currAsset.MachineDetailsProgress;
		
	if (currProgress != MI_Asset.PROGRESS_COMPLETE)
	{
		GUI.DrawTexture(new Rect(m_machineDetailsBtn_Rect.x, 
								 m_machineDetailsBtn_Rect.y,
								 m_machineDetailsBtn_Rect.width * (currProgress * 0.01f),
								 m_machineDetailsBtn_Rect.height), m_blue_Img);
					
		GUI.Box(m_machineDetailsBtn_Rect, btngc, m_transpEntryStyle);	
	}
	else
	{
		if (GUI.Button(m_machineDetailsBtn_Rect, btngc))
		{
			m_showing_MachineDetails = !m_showing_MachineDetails;
			m_showing_Attachments = false;
			m_showing_WorkOrders = false;
			m_showing_WorkOrderContent = false;	
			m_showing_AttachmentContent = false;	
				
			if (m_showing_MachineDetails)
			{
				AppData.GuiStuff_ClearMiddle_NEEDED = true;	
			}
		}	
	}
	
	if (m_showing_MachineDetails && m_currAsset.MachineDetails_Ready)
	{
		m_machineDetailsVisitor.Clear();
			
		m_currAsset.MachineDetails.Accept(m_machineDetailsVisitor);	
			
		GUIContent gc = new GUIContent(m_machineDetailsVisitor.GetString());
			
		Vector2 size = m_viewBoxStyle.CalcSize(gc);	
			
		Rect actualRect = new Rect(m_scrollView_Rect);
		
		actualRect.height = size.y + 10;
		actualRect.width = size.x + 10;	
		actualRect.x = m_machineDetailsBtn_Rect.x - (Screen.width * GUI_MARGIN_PCNT) - actualRect.width;	
			
		Rect scrollViewRect = new Rect(m_scrollView_Rect);
		scrollViewRect.x = actualRect.x;	
		scrollViewRect.width = actualRect.width + 20;	
			
		m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, actualRect);
			
		GUI.Box(actualRect, gc, m_viewBoxStyle);	
			
		GUI.EndScrollView();
	}
}
	

void GUI_AttachedFiles()
{
	GUIContent btngc = new GUIContent("  Attachments", m_showing_Attachments ? m_arrowLeft_Img : m_arrowRight_Img);
		
	float machProgress = m_currAsset.MachineDetailsProgress;
		
	if (machProgress != MI_Asset.PROGRESS_COMPLETE)
	{
		GUI.DrawTexture(new Rect(m_attachedFilesBtn_Rect.x, 
								 m_attachedFilesBtn_Rect.y,
								 m_attachedFilesBtn_Rect.width * (machProgress * 0.01f),
								 m_attachedFilesBtn_Rect.height), m_blue_Img);
					
		GUI.Box(m_attachedFilesBtn_Rect, btngc, m_transpEntryStyle);	
	}
	else
	{	
		if (GUI.Button(m_attachedFilesBtn_Rect, btngc))
		{
			m_showing_MachineDetails = false;
			m_showing_Attachments = !m_showing_Attachments;
			m_showing_WorkOrders = false;	
			m_showing_WorkOrderContent = false;	
			m_showing_AttachmentContent = false;	
				
			if (m_showing_Attachments)
			{
				AppData.GuiStuff_ClearMiddle_NEEDED = true;	
			}
		}
			
		if (m_currAsset.Attachments.Count > 0)
		{
			float totalProgress = 0;
			
			m_currAsset.Attachments.ForEach(x => 
			{
				if (x.The_Type != MI_Attachment.Type.Video)
				{
					totalProgress += x.Progress; 
				}
			});
			
			int nvCount = 0;
				
			m_currAsset.Attachments.ForEach(x => 
			{
				if (x.The_Type != MI_Attachment.Type.Video)
				{
					nvCount++;	
				}
			});
				
			if (nvCount != 0)
			{
				totalProgress /= (float)nvCount;
			}
			else
			{
				totalProgress = MI_Asset.PROGRESS_COMPLETE;	
			}
				
				
			if (totalProgress != MI_Asset.PROGRESS_COMPLETE)
			{
				GUI.DrawTexture(
					new Rect(
						m_attachedFilesBtn_Rect.x + m_attachedFilesBtn_Rect.width * 0.1f,
						m_attachedFilesBtn_Rect.y + m_attachedFilesBtn_Rect.height * 0.75f,
						m_attachedFilesBtn_Rect.width * 0.8f * (totalProgress * 0.01f),
						m_attachedFilesBtn_Rect.height * 0.1f
					), 
					m_blue_Img);	
			}
		}
	}
		
	if (m_showing_Attachments)
	{
		String maxString = "";
			
		m_currAsset.Attachments.ForEach(x =>
			{
				String s = x.TypeSTR + ": " + x.Description;
				
				maxString = (maxString.Length < s.Length) ? s : maxString;
			});
			
		GUIContent maxC = new GUIContent(maxString, m_genericFile_Img);
			
		Vector2 btnSize = m_entryBtnStyle.CalcSize(maxC);
		btnSize.x += 20;
		btnSize.y = Screen.height * GUI_ELEM_HEIGHT_PCNT;	
			
		Rect actualRect = new Rect(m_scrollView_Rect);
		
		actualRect.height = ((btnSize.y + 5) * m_currAsset.Attachments.Count) + 10;
		actualRect.width = btnSize.x + 10;	
		actualRect.x = m_machineDetailsBtn_Rect.x - (Screen.width * GUI_MARGIN_PCNT) - actualRect.width;	
			
		Rect scrollViewRect = new Rect(m_scrollView_Rect);
		scrollViewRect.x = actualRect.x;
		scrollViewRect.width = actualRect.width + 20;	
			
		m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, actualRect);	
			
		for (int i = 0; i < m_currAsset.Attachments.Count; i++)
		{
			Rect entryRect = new Rect(actualRect.x, actualRect.y + (i * (btnSize.y + 5)), btnSize.x, btnSize.y);	
			
			GUIContent entryContent = new GUIContent("  " + m_currAsset.Attachments[i].TypeSTR + ": " + m_currAsset.Attachments[i].Description, GetIcon(m_currAsset.Attachments[i].TypeSTR));	
				
			float currProgress = m_currAsset.Attachments[i].Progress;
				
			if (currProgress != MI_Asset.PROGRESS_COMPLETE)
			{
				GUI.DrawTexture(new Rect(entryRect.x, 
										 entryRect.y,
										 entryRect.width * (currProgress * 0.01f),
										 entryRect.height), m_blue_Img);
					
				GUI.Box(entryRect, entryContent, m_transpEntryStyle);	
			}
			else
			{
				if (GUI.Button(
					entryRect, 
					entryContent, 
					m_entryBtnStyle))
				{
					m_showing_MachineDetails = false;
					m_showing_Attachments = false;
					m_showing_WorkOrders = false;	
					m_showing_WorkOrderContent = false;
					m_showing_AttachmentContent = true;	
					
					m_currAttachment_Index = i;
				}
			}
		}
			
		GUI.EndScrollView();		
	}
		
	if (m_showing_AttachmentContent) 
	{	
		MI_Attachment att = m_currAsset.Attachments[m_currAttachment_Index];
				
		GUIContent descrGC = new GUIContent("  " + att.Description, GetIcon(att.TypeSTR));	
		Vector2 descrSize = m_viewBoxStyle.CalcSize(descrGC);	
			
		Rect descrRect = new Rect(
				Screen.width * 0.5f,
				m_machineDetailsBtn_Rect.y,
				descrSize.x + 10,
				descrSize.y + 10);
			
		GUI.Box(descrRect, descrGC, m_viewBoxStyle);	
			
		GUIContent remGC = new GUIContent("  Remarks: " + (!String.IsNullOrEmpty(att.Remarks) ? att.Remarks : "<none>"), m_remarks_Img);
		Vector2 remSize = m_viewBoxStyle.CalcSize(remGC);	
			
		Rect remarksRect = new Rect(
				descrRect.x,
				descrRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 1),
				remSize.x + 10,
				remSize.y + 10);
			
		GUI.Box(remarksRect, remGC, m_viewBoxStyle);
			
		GUIContent fileGC = new GUIContent("  Filename: " + att.FileName, m_genericFile_Img);
		Vector2 fileSize = m_viewBoxStyle.CalcSize(fileGC);
			
		Rect fileRect = new Rect(
				descrRect.x,
				remarksRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 1),
				fileSize.x + 10,
				fileSize.y + 10);
		
		GUI.Box(fileRect, fileGC, m_viewBoxStyle);	
			
		GUIContent fm = new GUIContent("\nView folder", m_fileExplorer_Img);	
		Vector2 fmSize = m_thumbStyle.CalcSize(fm);	
			
		Rect openFolderRect = new Rect(
				fileRect.x + (Screen.width * (0.15f + (GUI_MARGIN_PCNT * 4))),
				fileRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 6) + (Screen.width * 0.15f * 0.5f) - ((Screen.width * 0.15f * 0.67f) * 0.5f),
				Screen.width * 0.15f * 0.67f,
				Screen.width * 0.15f * 0.67f);
			
		if (att.The_Type != MI_Attachment.Type.Video)
		{
			if (GUI.Button(openFolderRect, fm, m_thumbStyle))
			{
				//install OI File Manager in tablet
				
				#if (UNITY_EDITOR) || (UNITY_STANDALONE)
				#elif (UNITY_ANDROID)
				OpenFilePluginAPI.OpenFolder(att.LocalDir);
				#endif
			}
		}
			
		m_actOnFile_Rect = new Rect(
				descrRect.x,
				openFolderRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 2),
				Screen.width * GUI_ELEM_WIDTH_PCNT * 1.2f,
				Screen.height * GUI_ELEM_HEIGHT_PCNT);
	
			
		if (att.The_Type == MI_Attachment.Type.Image)	
		{	
			if (att.Ready)
			{
				GUIContent gc = new GUIContent("\nView fullscreen", att.The_Texture);
				
				Rect r1 = new Rect(
					descrRect.x, 
					fileRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 6), 
					Screen.width * 0.15f, 
					Screen.width * 0.15f);
					
				if (GUI.Button(r1, gc, m_thumbStyle))
				{
					m_fullScreenImage = att.The_Texture;	
				}
			}
			else
			{
				AppData.Popups.Push("Attachment not available");
			}
		}
		/*	
		else if (att.The_Type == MI_Attachment.Type.Text)
		{
			if (att.Ready)
			{
				GUIContent gc = new GUIContent(att.Text);
				Vector2 size = guiSkin.GetStyle("Box").CalcSize(gc);
			
				Rect r1 = new Rect(
					Screen.width * 0.5f - (size.x * 0.5f), 
					openFolderRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 4), 
					size.x, 
					size.y);
					
				Rect scrollViewRect = new Rect(m_scrollView_Rect);
				scrollViewRect.y = r1.y;
				scrollViewRect.height = (r1.yMax > m_scrollView_Rect.yMax) ? (m_scrollView_Rect.height - (r1.y - m_scrollView_Rect.y)) : r1.height + 20;
				scrollViewRect.width = (r1.xMax > m_scrollView_Rect.xMax) ? m_scrollView_Rect.width : r1.width + 20;	
				scrollViewRect.x = (r1.xMax > m_scrollView_Rect.xMax) ? m_scrollView_Rect.x : r1.x;	
					
				m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, r1);	
				
				GUI.Box(r1, gc, m_textViewBoxStyle);
					
				GUI.EndScrollView();	
			}
			else
			{
				AppData.Popups.Push("Attachment not available");	
			}
		}
		*/
		else if (att.The_Type == MI_Attachment.Type.Video)
		{		
			GUIContent vidGC = new GUIContent("\nStream video\nfullscreen", (att.The_Texture != null) ? att.The_Texture : m_video_large_Img);
				
			Vector2 vidSize = m_entryBtnStyle.CalcSize(vidGC);
				
			Rect vidRect = new Rect(
					descrRect.x,
					fileRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 6),
					Screen.width * 0.15f ,
					Screen.width * 0.15f );	
				
			if (GUI.Button(vidRect, vidGC, m_thumbStyle))
			{	
					
				//if (!File.Exists(att.LocalPath))
				//{
				//	att.SaveToFile();	
				//}
				
					
				if (att.Ready)
				{			
					#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)	
					Application.OpenURL("file://" + att.LocalPath);	
					#elif (UNITY_ANDROID)
					//Handheld.PlayFullScreenMovie("file://" + att.LocalPath, Color.black, FullScreenMovieControlMode.Full);
					Handheld.PlayFullScreenMovie(AppData.ATTACHMENT_URL(att.The_Path), Color.black, FullScreenMovieControlMode.Full);
					#endif
				}
				else
				{			
					AppData.Popups.Push("Attachment not available");	
				}
			}
		}
		else if (att.The_Type == MI_Attachment.Type.Audio)
		{
			AudioSource source = GameObject.Find("ARCamera").audio;
				
			if (source.clip != att.Audio_Clip)
			{
				source.Stop();
					
				source.clip = att.Audio_Clip;	
			}
			
			GUIContent playpauseGC = new GUIContent(source.isPlaying ? m_audPause_Img : m_audPlay_Img);	
			
			GUIContent stopGC = new GUIContent(m_audStop_Img);
				
			Vector2 ppSize = m_entryBtnStyle.CalcSize(playpauseGC);
				
			Vector2 stopSize = m_entryBtnStyle.CalcSize(stopGC);	
				
			Rect sliderRect = new Rect(
					openFolderRect.x - (Screen.width * 4 * GUI_MARGIN_PCNT) - (Screen.width * GUI_ELEM_WIDTH_PCNT * 1f),
					openFolderRect.yMax - (openFolderRect.height * 0.5f),
					Screen.width * GUI_ELEM_WIDTH_PCNT * 1f,
					Screen.height * GUI_ELEM_HEIGHT_PCNT * 0.5f
					);	
				
			Rect playRect = new Rect(
					(sliderRect.x + (sliderRect.width * 0.5f)) - ((ppSize.x + stopSize.x + (Screen.width * GUI_MARGIN_PCNT * 2)) * 0.5f),
					sliderRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 2),
					ppSize.x,
					ppSize.y);
				
			Rect stopRect = new Rect(
					playRect.xMax + (Screen.width * GUI_MARGIN_PCNT * 2),
					playRect.y,
					stopSize.x,
					stopSize.y);
				
					
			float prevValue = m_audioSliderValue;	
				
			m_audioSliderValue = GUI.HorizontalSlider(sliderRect, m_audioSliderValue % att.Audio_Clip.length, 0, att.Audio_Clip.length);	
				
			if (m_audioSliderValue != prevValue)
			{
				bool wasPlaying = source.isPlaying;
					
				source.Pause();	
				source.Play();	
					
				source.time = m_audioSliderValue;	
					
				if (!wasPlaying)
				{
					source.Pause();	
				}
			}
			else
			{
				m_audioSliderValue = source.time;	
			}
				
			if (GUI.Button(playRect, playpauseGC))
			{
				if (source.isPlaying)
				{
					source.Pause();	
				}
				else
				{
					source.Play();	
				}
			}
				
			if (GUI.Button(stopRect, stopGC))
			{
				source.Stop();
					
				m_audioSliderValue = 0;	
			}	
		}
		else 
		{
			GUIContent extGC = new GUIContent("\nOpen externally", GetLargeIcon(att.TypeSTR));	
			
			//Vector2 extSize = m_entryBtnStyle.CalcSize(extGC); 
				
				
			Rect extRect = new Rect(
					descrRect.x,
					fileRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 6) /*m_actOnFile_Rect.y*/,
					Screen.width * 0.15f /*fextSize.x + 10*/,
					Screen.width * 0.15f /*fextSize.y + 10*/);
				
			if (GUI.Button(extRect, extGC, m_thumbStyle))
			{	
				if (!File.Exists(att.LocalPath))
				{
					att.SaveToFile();	
				}		
						
				if (att.Ready)
				{
					#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)		
					Application.OpenURL("file://" + att.LocalPath);
					#elif (UNITY_ANDROID)	
					OpenFilePluginAPI.OpenFileWithExternalApp("file://" + att.LocalPath, att.Ending);							
					#endif	
				}
				else
				{
					AppData.Popups.Push("Attachment not available");	
				}
			}
		}
	}
}
	
	
void GUI_PendingWorkOrders()
{	
	GUIContent btngc = new GUIContent("  Work Orders", m_showing_WorkOrders ? m_arrowLeft_Img : m_arrowRight_Img);	
		
	float currProgress = m_currAsset.WorkOrdersProgress;
		
	if (currProgress != MI_Asset.PROGRESS_COMPLETE)
	{
		GUI.DrawTexture(new Rect(m_pendingWorkOrdersBtn_Rect.x, 
								 m_pendingWorkOrdersBtn_Rect.y,
								 m_pendingWorkOrdersBtn_Rect.width * (currProgress * 0.01f),
								 m_pendingWorkOrdersBtn_Rect.height), m_blue_Img);
					
		GUI.Box(m_pendingWorkOrdersBtn_Rect, btngc, m_transpEntryStyle);	
	}
	else
	{	
		if (GUI.Button(m_pendingWorkOrdersBtn_Rect, btngc))
		{
			m_showing_MachineDetails = false;
			m_showing_Attachments = false;
			m_showing_WorkOrders = !m_showing_WorkOrders;	
			m_showing_WorkOrderContent = false;	
			m_showing_AttachmentContent = false;	
				
			if (m_showing_WorkOrders)
			{
				AppData.GuiStuff_ClearMiddle_NEEDED = true;	
			}
		}
	}
		
	if (m_showing_WorkOrders)
	{
		String maxString = "";
			
		m_currAsset.WorkOrders.ForEach(x => 
			{
				String s = x.Description;
				
				maxString = (maxString.Length < s.Length) ? s : maxString;
			});
		
		GUIContent maxC = new GUIContent(maxString);
			
		Vector2 btnSize = m_entryBtnStyle.CalcSize(maxC);	
		btnSize.x += 20;
		btnSize.y = Screen.height * GUI_ELEM_HEIGHT_PCNT;
			
		Rect actualRect = new Rect(m_scrollView_Rect);
		
		actualRect.height = ((btnSize.y + 5) * m_currAsset.WorkOrders.Count) + 10;
		actualRect.width = btnSize.x + 10;	
		actualRect.x = m_machineDetailsBtn_Rect.x - (Screen.width * GUI_MARGIN_PCNT) - actualRect.width;	
			
		if (actualRect.xMax > (Screen.width * (1 - GUI_MARGIN_PCNT)))
		{
			actualRect.x = (Screen.width * (1 - GUI_MARGIN_PCNT)) - actualRect.width;	
		}
			
		Rect scrollViewRect = new Rect(m_scrollView_Rect);
		scrollViewRect.x = actualRect.x;
		scrollViewRect.width = actualRect.width + 20;	
			
		m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, actualRect);	
			
		for (int i = 0; i < m_currAsset.WorkOrders.Count; i++)
		{
			if (GUI.Button(
					new Rect(actualRect.x, actualRect.y + (i * (btnSize.y + 5)), btnSize.x, btnSize.y), 
					new GUIContent(m_currAsset.WorkOrders[i].Description), 
					m_entryBtnStyle))
			{
				m_showing_MachineDetails = false;
				m_showing_Attachments = false;
				m_showing_WorkOrders = false;	
				m_showing_WorkOrderContent = true;
				m_showing_AttachmentContent = false;	
					
				m_currWorkOrder_Index = i;	
			}
		}
			
		GUI.EndScrollView();	
	}
		
	if (m_showing_WorkOrderContent)
	{
		m_workOrderVisitor.Clear();
			
		m_currAsset.WorkOrders[m_currWorkOrder_Index].Accept(m_workOrderVisitor);
			
		GUIContent gc = new GUIContent(m_workOrderVisitor.GetString());
			
		Vector2 size = m_viewBoxStyle.CalcSize(gc);	
			
		Rect actualRect = new Rect(m_scrollView_Rect);
		
		actualRect.height = size.y + 10;
		actualRect.width = size.x + 10;
		actualRect.x = m_machineDetailsBtn_Rect.x - (Screen.width * GUI_MARGIN_PCNT) - actualRect.width;	
			
		if (actualRect.xMax > (Screen.width * (1 - GUI_MARGIN_PCNT)))
		{
			actualRect.x = (Screen.width * (1 - GUI_MARGIN_PCNT)) - actualRect.width;	
		}
			
		Rect scrollViewRect = new Rect(m_scrollView_Rect);
		scrollViewRect.x = actualRect.x - 20;	
		scrollViewRect.width = actualRect.width + 20;	
			
		m_scrollPosition = GUI.BeginScrollView(scrollViewRect, m_scrollPosition, actualRect);
			
		GUI.Box(actualRect, gc, m_viewBoxStyle);	
			
		GUI.EndScrollView();	
	}
}
	

public void ClearMiddle()
{
	m_showing_MachineDetails = false;
	m_showing_Attachments = false;
	m_showing_WorkOrders = false;	
	m_showing_WorkOrderContent = false;
	m_showing_AttachmentContent = false;
}
	
	
Font GetFontForSize(float coeff)
{
	int s = (int)(Screen.width * coeff);	

	Font x = font20;
		
	if (s <= 9)
		x = font8;	
	else if ((s > 9) && (s <= 11))
		x = font10;
	else if ((s > 11) && (s <= 13))
		x = font12;
	else if ((s > 13) && (s <= 15))
		x = font14;
	else if ((s > 15) && (s <= 17))
		x = font16;
	else if ((s > 17) && (s <= 19))
		x = font16;
	else if ((s > 19) && (s <= 21))
		x = font18;
	else if ((s > 21) && (s <= 23))
		x = font20;
	else if ((s > 23) && (s <= 25))
		x = font22;
	else if (s > 25)
		x = fontHD;
		
	return x;	
}
	
	
void SelectFont()
{
	Font x = GetFontForSize(STD_FONT_COEFF);
			
	GUI.skin.button.font = 	x;
	GUI.skin.label.font = x;
	GUI.skin.font = x;
	GUI.skin.box.font = x;		
}
	
	
}//class
