using System.Collections.Generic;
using System;


public static class TrackedRegistry {

	private static List<int> m_currentTrackables;
	
	public const int INVALID_ID = -1;
	
	
static TrackedRegistry()
{
	m_currentTrackables = new List<int>();	
}
	
	
public static void Clear()
{
	m_currentTrackables.Clear();	
}
	
	
public static int Tracked_ID
{
	get { return (m_currentTrackables.Count > 0) ? m_currentTrackables[0] : INVALID_ID; }
}

	
public static void AddTrackable(int id)
{
	m_currentTrackables.Clear(); //enforce single-marker tracked policy	
		
	if (!m_currentTrackables.Contains(id))
		m_currentTrackables.Add(id);
}
	
	
public static void RemoveTrackable(int id)
{
	if (m_currentTrackables.Contains(id))
		m_currentTrackables.Remove(id);
}
	
	
public static bool HasTrackable(int id)
{
	return m_currentTrackables.Contains(id);		
}

	
public static bool HasTrackable()
{
	return m_currentTrackables.Count > 0;	
}

		
}//class
