using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class NativeDownloader : MonoBehaviour {
	
	private Stack<DownloadItem> m_downloads;
	
	private WWW m_request;
	
	private float m_lastTime;
	
	
public DownloadItem Item
{
	get;
	private set;
}
	
	
void Start()
{
	m_lastTime = Time.time;
		
	Item = null;
		
	m_request = null;
		
	Downloading = false;
		
	m_downloads = new Stack<DownloadItem>();
		
	WebDownloader.NativeDownloader = this;
}

	
void Update()
{
	if (Time.time - m_lastTime < 1)
	{
		return;	
	}
		
	m_lastTime = Time.time;	
		
	if (!Downloading && !WebDownloader.Downloading && (m_downloads.Count > 0))
	{
		StartCoroutine("ActualDownload");	
	}
}
	
		
public bool Downloading
{
	get;
	private set;
}
	
	
public void CancelCurrentDownload()
{		
	Downloading = false;
		
	if (Item != null)
	{
		m_downloads.Push(Item);
			
		Item = null;
			
		if (m_request != null)
		{
			m_request.Dispose();
		
			m_request = null;
		}	
	}
}
	
	
public void Download(int arId, String url, String description, Action<WWW> callback)
{
	DownloadItem di = new DownloadItem(arId, url, description, callback);
		
	m_downloads.Push(di);
}
		
	
IEnumerator ActualDownload()
{
	Downloading = true;	
	
	Item = m_downloads.Pop();
		
	MI_Asset asset = MI_Asset_Cache.Get(Item.AR_ID);	
		
	MI_Attachment att = asset.Attachments.Find(x => AppData.ATTACHMENT_URL(x.The_Path) == Item.URL);	
		
	WebDownloader.Progress = 0;
	WebDownloader.DownloadMsg = Item.Description;	
		
	m_request = new WWW(Item.URL);	
		
	while (!m_request.isDone)
	{
		att.Progress = WebDownloader.Progress = m_request.progress * 100;
		
		yield return null;
	}
	
	if (!String.IsNullOrEmpty(m_request.error))
	{
		String error = "Failed to download: " + Item.Description + "\n" + m_request.error;
			
		if (!AppData.Popups.Contains(error))
		{
			AppData.Popups.Push(error);	
		}
			
		m_downloads.Push(Item);		
	}
	else
	{
		att.Progress = WebDownloader.Progress = 100;	
				
		Item.Callback(m_request);	
	}
		
	m_request = null;	
		
	Item = null;	
		
	Downloading = false;
		
	yield break;	
}

	
}//class
