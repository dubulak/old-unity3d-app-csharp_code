using UnityEngine;
using System.Collections;


public class ResetCameraHandle : MonoBehaviour {
	
	private Vector3 m_startPosition;
	private Vector3 m_startAngles;
	

void Start() 
{
	m_startPosition = transform.position;
	m_startAngles = transform.eulerAngles;
}

	
public void Reset()
{
	transform.position = m_startPosition;
	transform.eulerAngles = m_startAngles;
}

	
}//class
