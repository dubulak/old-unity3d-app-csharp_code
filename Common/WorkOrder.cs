using System.Collections.Generic;
using System;


public class WorkOrder {

	public static readonly String[] m_statusDesc;
		
	
static WorkOrder()
{
	m_statusDesc = new string[5];
		
	m_statusDesc[0] = "Open";
	m_statusDesc[1] = "Executing";
	m_statusDesc[3] = "Partially Completed";
	m_statusDesc[4] = "Closed";
}
	
	
public void Accept(Visitor visitor)
{	
	visitor.Visit(this);
}
	
	
public String RegistrationDate
{
	get;
	set;
}
	

public String Type
{
	get;
	set;
}
	
	
public String Description
{
	get;
	set;
}
	
	
public String Applicant
{
	get;
	set;
}
	
		
public String Status
{
	get;
	set;
}
	
	
}//class
