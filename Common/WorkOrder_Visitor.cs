using System.Collections.Generic;
using System;


public class WorkOrder_Visitor : Visitor {

	private Dictionary<String, String> m_workOrder_Content;

	
public WorkOrder_Visitor()
{
	m_workOrder_Content = new Dictionary<string, string>();
}
	
	
public void Clear()
{
	m_workOrder_Content.Clear();
}
	
	
public String GetString()
{
	String content = "";
		
	foreach (KeyValuePair<String, String> kvp in m_workOrder_Content)
	{		
		if (kvp.Value.Length > 50)
		{
			content += kvp.Key + ":";
				
			string[] tokens = kvp.Value.Split(' ');
				
			int lineLength = 0;
			int i = 0;
			
			while (i < tokens.Length)
			{
				do
				{
					content += " " + tokens[i];
					
					lineLength += tokens[i].Length + 1;
						
					i++;	
				}
				while ((i < tokens.Length) && (lineLength < 50));
					
				content += "\n";
					
				lineLength = 0;	
			}				
		}
		else
		{
			content += kvp.Key + ": " + kvp.Value + "\n";
		}
	}
		
	if (content.Length > 0)
	{
		content = content.Remove(content.Length - 1);	
	}	
		
	return content;
}
		
	
public override void Visit(WorkOrder workOrder)
{
	m_workOrder_Content.Add("Registration date", workOrder.RegistrationDate);
	m_workOrder_Content.Add("Type", workOrder.Type);	
	m_workOrder_Content.Add("Description", workOrder.Description);
	m_workOrder_Content.Add("Applicant", workOrder.Applicant);
	m_workOrder_Content.Add("Status", workOrder.Status);
}	
	

public override void Visit(MI_Attachment attachment)
{
	throw new NotImplementedException();
}	
	
	
public override void Visit(MachineDetails machineDetails)
{
	throw new NotImplementedException();
}	
			

public override void Visit(MI_TechnicalAttribute ta)
{
	throw new NotImplementedException ();
}	
	
	
}//class
