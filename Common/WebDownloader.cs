using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Net;
using System.Threading;
using UnityEngine;


public static class WebDownloader {
	
	private static Stack<MI_Asset> m_downloads;
	
	private static MI_Asset m_downloadAsset;
	
	private const int BUFFER_SIZE = 1448;	

	private static HttpWebRequest m_webRequest;
		
	private const int TIMEOUT = 20 * 1000;
	
	private static float m_lastTime;
	
	private enum Phase { MachineDetails, WorkOrders };
	
	private static Phase m_phase;
		
	
static WebDownloader()
{
	m_lastTime = Time.time;
		
	m_downloads = new Stack<MI_Asset>();
		
	Downloading = false;
		
	DownloadMsg = null;	
		
	m_downloadAsset = null;	
		
	m_webRequest = null;
}
		
		
public static void Clear()
{
	m_lastTime = Time.time;
	
	m_downloads.Clear();
		
	Downloading = false;
		
	DownloadMsg = null;	
		
	m_downloadAsset = null;	
		
	m_webRequest = null;
}
	
	
public static bool Downloading
{
	get;
	private set;
}

	
public static NativeDownloader NativeDownloader
{
	get;
	set;
}
	
	
public static float Progress
{
	get;
	set;
}
	
	
public static String DownloadMsg
{
	get;
 	set;
}
		

static void ProgressFunc(int totalBytes, double pctComplete, double transferRate)
{
	Progress = (float)pctComplete;   
		
	if (m_phase == Phase.MachineDetails)
	{
		m_downloadAsset.MachineDetailsProgress = Progress;
	}
	else if (m_phase == Phase.WorkOrders)
	{
		m_downloadAsset.WorkOrdersProgress = Progress;
	}
}
	
	
private static void DownloadAttachments()
{
	for (int i = m_downloadAsset.Attachments.Count - 1; i >= 0; i--)
	{
		MI_Attachment att = m_downloadAsset.Attachments[i];
			
		if ((att.Progress != MI_Asset.PROGRESS_COMPLETE) && (att.The_Type != MI_Attachment.Type.Video))
		{
			NativeDownloader.Download(m_downloadAsset.AR_ID, AppData.ATTACHMENT_URL(att.The_Path), att.Description +  " [marker " + m_downloadAsset.AR_ID + "]", att.SaveContent);	
		}	
	}	
		
	m_downloadAsset = null;	
}
	

public static void MainLoopOneRound()
{
	if (Time.time - m_lastTime < 1)
	{
		return;	
	}
		
	m_lastTime = Time.time;	
		
	if ((m_downloads.Count > 0) && (m_webRequest == null))
	{
		if (NativeDownloader.Downloading)
		{
			if ((NativeDownloader.Item != null) && 
				(NativeDownloader.Item.AR_ID == m_downloads.Peek().AR_ID))
			{
				m_downloads.Pop();
					
				return;	
			}
				
			NativeDownloader.CancelCurrentDownload();	
		}
			
		if (Downloading)
		{	
			CancelCurrentDownload();	
		}
			
		m_downloadAsset = m_downloads.Pop();
				
		if (m_downloadAsset.MachineDetailsProgress != MI_Asset.PROGRESS_COMPLETE)	
		{	
			Downloading = true;	
			
			m_phase = Phase.MachineDetails;
				
			WebDownloader.StartWebRequest(AppData.ASSET_URL(m_downloadAsset.AR_ID), 
						"machine details [marker " + m_downloadAsset.AR_ID + "]", 
						new Action<WebRequestState>(WebDownloader.RetrieveMachineDetails));
		}
		else if (m_downloadAsset.WorkOrdersProgress != MI_Asset.PROGRESS_COMPLETE)
		{
			Downloading = true;
				
			m_phase = Phase.WorkOrders;	
				
			StartWebRequest(AppData.TASKS_PER_ASSET_URL(m_downloadAsset.CMMS_ID), 
						"work orders [marker " + m_downloadAsset.AR_ID + "]", 
						new Action<WebRequestState>(RetrieveTasks));		
		}
		else
		{
			DownloadAttachments();		
		}	
	}
}
	
	
public static void DownloadAssetInfo(MI_Asset asset)
{
	if (!m_downloads.Contains(asset) && (m_downloadAsset != asset))	
	{		
		m_downloads.Push(asset);	
	}
}
	
		
public static void CancelCurrentDownload()
{
	Downloading = false;
		
	if (m_webRequest != null)
	{
		m_webRequest.Abort();
			
		m_webRequest = null;	
	}		
		
	if (m_downloadAsset != null)
	{
		if (!m_downloads.Contains(m_downloadAsset))
		{
			m_downloads.Push(m_downloadAsset);
		}
			
		m_downloadAsset = null;
	}
}
	
	
private static void TimeoutCallback(object unused, bool timedOut) 
{ 
	if (timedOut) 
	{
		String error = ((m_downloadAsset == null) ? "" : ("Error while downloading info for marker " + m_downloadAsset.AR_ID + "\n")) + "Communication with server timed out!";
			
    	CancelCurrentDownload();
		
		if (!AppData.Popups.Contains(error))
		{
			AppData.Popups.Push(error);	
		}
    }
}
	
	
private static void StartWebRequest(String url, String msg, Action<WebRequestState> doneCallback)
{	
	Progress = 0;
	DownloadMsg = msg;	
		
	try 
	{	
		m_webRequest = (HttpWebRequest)HttpWebRequest.Create(url);	
			
		m_webRequest.Accept = "application/xml";
	
		m_webRequest.Headers["Authorization"] = " Basic " + Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes("admin:admin"));		

		HttpWebRequestState reqState = new HttpWebRequestState(BUFFER_SIZE);
    	reqState.request = m_webRequest;
    	reqState.progCB = new ProgressDelegate(ProgressFunc);
    	reqState.doneCB = new DoneDelegate(doneCallback);
    	reqState.transferStart = DateTime.Now;
	
		IAsyncResult result = null;	
			
        result = (IAsyncResult)m_webRequest.BeginGetResponse(GetResponse, reqState);
			
		ThreadPool.RegisterWaitForSingleObject(
				result.AsyncWaitHandle, 
				new WaitOrTimerCallback(TimeoutCallback), 
				null, 
				TIMEOUT, 
				true);
	
	}
	catch (Exception e)
	{
		String error = ((m_downloadAsset == null) ? "" : ("Error while downloading info for marker " + m_downloadAsset.AR_ID + "\n")) + "Error requesting data from server" + "\n" + e.Message;
			
		if (!AppData.Popups.Contains(error))
		{
			AppData.Popups.Push(error);
		}
			
		CancelCurrentDownload();	
	}
}
		
	
static void ReadCallback(IAsyncResult asyncResult)
{
	try
 	{
    	WebRequestState reqState = ((WebRequestState)(asyncResult.AsyncState));

        Stream responseStream = reqState.streamResponse;	

        int bytesRead = responseStream.EndRead(asyncResult);

        if (bytesRead > 0)
        {
			for (int i = reqState.bytesRead; i < (reqState.bytesRead + bytesRead); i++)
			{
				reqState.content[i] = reqState.bufferRead[i - reqState.bytesRead];	
			}
				
            // Report some progress, including total # bytes read, % complete, and transfer rate
            reqState.bytesRead += bytesRead;
            double pctComplete = ((double)reqState.bytesRead / (double)reqState.totalBytes) * 100.0f;

            // Note: bytesRead/totalMS is in bytes/ms.  Convert to kb/sec.
            TimeSpan totalTime = DateTime.Now - reqState.transferStart;
            double kbPerSec = (reqState.bytesRead * 1000.0f) / (totalTime.TotalMilliseconds * 1024.0f);

            reqState.progCB(reqState.bytesRead, pctComplete, kbPerSec);
				
            // Kick off another read
            IAsyncResult ar = responseStream.BeginRead(reqState.bufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallback), reqState);
				
			ThreadPool.RegisterWaitForSingleObject(
				ar.AsyncWaitHandle, 
				new WaitOrTimerCallback(TimeoutCallback), 
				null, 
				TIMEOUT, 
				true);	
        }
        else
        {
            responseStream.Close();
            reqState.response.Close();
				
			m_webRequest = null;	
				
            reqState.doneCB(reqState);
        }
    }
    catch (Exception e)
    {
		String error = ((m_downloadAsset == null) ? "" : ("Error while downloading info for marker " + m_downloadAsset.AR_ID + "\n")) + "Error reading server data" + "\n" + e.Message + "\n" + e.StackTrace;	
			
		if (!AppData.Popups.Contains(error))
		{
        	AppData.Popups.Push(error);
		}
				
		CancelCurrentDownload();	
    }
}
	
	
static void GetResponse(IAsyncResult result)
{
	WebRequestState reqState = ((WebRequestState)(result.AsyncState));
    
	WebRequest req = reqState.request;
		
	HttpWebResponse response = null;
		
	Stream responseStream = null;
		
	try 
	{
		response = req.EndGetResponse(result) as HttpWebResponse;	
		
		reqState.response = response;	
			
		responseStream = reqState.response.GetResponseStream();
                    
		reqState.streamResponse = responseStream;	
			
		reqState.totalBytes = reqState.response.ContentLength;
			
		reqState.content = new byte[reqState.totalBytes];	
			
		IAsyncResult ar = responseStream.BeginRead(reqState.bufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallback), reqState);	
	}
	catch (Exception e)
	{
		String error = ((m_downloadAsset == null) ? "" : ("Error while downloading info for marker " + m_downloadAsset.AR_ID + "\n")) + "Error receiving data from server" + "\n" + e.Message;	
			
		if (response != null)
		{
			error += "\n" + response.StatusCode + "\n" + response.StatusDescription + "\n";		
		}	
			
		if (!AppData.Popups.Contains(error))
		{
			AppData.Popups.Push(error);	
		}
			
		CancelCurrentDownload();	
	}		
}	

	
public static void RetrieveMachineDetails(WebRequestState state)
{
	HttpWebRequestState webstate = state as HttpWebRequestState;
		
	XmlDocument doc = new XmlDocument();
			
	String data = "";
		
	try 
	{	
		data = Encoding.UTF8.GetString(webstate.content, 0, (int)webstate.totalBytes);
			
		doc.LoadXml(data);
	}
	catch (Exception e)
	{
		AppData.Popups.Push("Error parsing asset [marker: " + m_downloadAsset.AR_ID + "] data" + "\n" + e.Message + "\n" + e.StackTrace);	
			
		CancelCurrentDownload();	
			
		return;
	}	
		
	m_downloadAsset.MachineDetails_Ready = true;	
		
	if (PopulateAssetInfo(m_downloadAsset, doc))
	{	
		m_phase = Phase.WorkOrders;	
			
		StartWebRequest(AppData.TASKS_PER_ASSET_URL(m_downloadAsset.CMMS_ID), 
						"work orders [marker " + m_downloadAsset.AR_ID + "]", 
						new Action<WebRequestState>(RetrieveTasks));	
	}
	else
	{
		AppData.Popups.Push("No info related to marker " + m_downloadAsset.AR_ID);
			
		m_downloadAsset.WorkOrdersProgress = MI_Asset.PROGRESS_COMPLETE;
			
		m_downloadAsset.WorkOrders_Ready = true;
			
		CancelCurrentDownload();	
	}
}
	
	
public static void RetrieveTasks(WebRequestState state)
{
	HttpWebRequestState webstate = state as HttpWebRequestState;	
		
	XmlDocument doc = new XmlDocument();
		
	String data = "";
		
	try 
	{	
		data = Encoding.UTF8.GetString(webstate.content, 0, (int)webstate.totalBytes);
			
		doc.LoadXml(data);	
	}
	catch (Exception e)
	{
		AppData.Popups.Push("Error parsing asset [marker: " + m_downloadAsset.AR_ID + "] data" + "\n" + e.Message);	
			
		return;
	}	
		
	m_downloadAsset.WorkOrders_Ready = true;
		
	PopulateAssetTasks(m_downloadAsset, doc);	
		
	DownloadAttachments();	
		
	Downloading = false;	
}
		
	
static void PopulateAssetTasks(MI_Asset asset, XmlDocument doc)
{
	XmlNode root = doc.DocumentElement;
		
	XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
		
	nsmgr.AddNamespace("ns", root.NamespaceURI);
		
	foreach (XmlNode n in root.SelectNodes("ns:TaskInfo", nsmgr))
	{
		WorkOrder wo = new WorkOrder();
			
		XmlNode dateRec_node = n.SelectSingleNode("ns:DateRecorded", nsmgr);
			
		if (dateRec_node != null)
		{		
			wo.RegistrationDate = dateRec_node.InnerText;	
		}	
		else
		{
			AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Registration Date' attribute!");	
		}
			
		wo.Description = n.SelectSingleNode("ns:Description", nsmgr).InnerText;	
			
		XmlNode categDescr_Node = n.SelectSingleNode("ns:Category/ns:Description", nsmgr);	
			
		if (categDescr_Node != null)
		{
			wo.Type = categDescr_Node.InnerText;	
		}
		else
		{
			AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Description' attribute!");	
		}	
			
		XmlNode applic_node = n.SelectSingleNode("ns:ApplicantName", nsmgr);
			
		if (applic_node != null)
		{	
			wo.Applicant = applic_node.InnerText;	
		}
		else
		{
			AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Applicant' attribute!");
		}
			
			
		XmlNode status_node = n.SelectSingleNode("ns:Status", nsmgr);
			
		if (status_node != null)
		{	
			wo.Status = status_node.InnerText;
		}
		else
		{
			AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Status' attribute!");	
		}
			
		asset.WorkOrders.Add(wo);	
	}
} 
	
	
static bool PopulateAssetInfo(MI_Asset asset, XmlDocument doc)
{
	XmlNode root = doc.DocumentElement;
		
	if (!root.HasChildNodes)	
	{
		return false;	
	}
	
	XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
		
	nsmgr.AddNamespace("ns", root.NamespaceURI);
		
	XmlNode id_node = root.SelectSingleNode("ns:ID", nsmgr);	
		
	if (id_node != null)
	{
		asset.CMMS_ID = id_node.InnerText;	
	}
	else
	{
		AppData.Popups.Push("Asset associated with QRcode " + asset.AR_ID + ", doesn't have 'ID' attribute!");
			
		return false;	
	}	
		
	XmlNode descr_node = root.SelectSingleNode("ns:Description", nsmgr);
		
	if (descr_node != null)
	{
		asset.MachineDetails.Description = descr_node.InnerText;
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Description' attribute!");
	}
		
	XmlNode guarant_node = root.SelectSingleNode("ns:GuaranteeExpiryDate", nsmgr);	
	
	if (guarant_node != null)
	{		
		asset.MachineDetails.GuaranteeExpiryDate = guarant_node.InnerText;
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'GuaranteeExpiryDate' attribute!");	
	}
	
	XmlNode purchase_node = root.SelectSingleNode("ns:PurchaseCost", nsmgr);
		
	if (purchase_node != null)
	{
		asset.MachineDetails.PurchaseCost = purchase_node.InnerText;
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'PurchaseCost' attribute!");	
	}
		
	XmlNode serial_node = root.SelectSingleNode("ns:SerialNumber", nsmgr);	
		
	if (serial_node != null)
	{
		asset.MachineDetails.SerialNumber = serial_node.InnerText;	
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'SerialNumber' attribute!");	
	}
	
	XmlNode constr_node = root.SelectSingleNode("ns:Constructor", nsmgr);
	
	if (constr_node != null)
	{	
		if (!String.IsNullOrEmpty(constr_node.InnerText))
		{
			XmlNode constrName_Node = root.SelectSingleNode("ns:Constructor/ns:Name", nsmgr);
		
			if (constrName_Node != null)	
			{
				asset.MachineDetails.Manufacturer = constrName_Node.InnerText;
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Manufacturer Name' attribute!");	
			}
		}
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Manufacturer' attribute!");	
	}
	
	
	XmlNode critical_node = root.SelectSingleNode("ns:Criticality", nsmgr);
		
	if (critical_node != null)
	{
		if (!String.IsNullOrEmpty(critical_node.InnerText))
		{
			XmlNode criticalDescr_Node = root.SelectSingleNode("ns:Criticality/ns:Description", nsmgr);
		
			if (criticalDescr_Node != null)
			{
				asset.MachineDetails.Criticality = criticalDescr_Node.InnerText;
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Criticality Description' attribute!");	
			}
		}
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Criticality' attribute!");	
	}
		
	
	XmlNode type_node = root.SelectSingleNode("ns:Type", nsmgr);
		
	if (type_node != null)
	{
		if (!String.IsNullOrEmpty(type_node.InnerText))
		{
			XmlNode typeDescr_Node = root.SelectSingleNode("ns:Type/ns:Description", nsmgr);
		
			if (typeDescr_Node != null)
			{
				asset.MachineDetails.MachineType = typeDescr_Node.InnerText;
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Type Description' attribute!");
			}
		}
	}
	else
	{
		AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Type' attribute!");	
	}
		
		
	XmlNodeList TAVs = root.SelectNodes("ns:TechnicalAttributeValues/ns:EntityTechnicalAttributeValue", nsmgr);	
		
	asset.MachineDetails.TechnicalAttributes = new List<MI_TechnicalAttribute>();
		
	if (TAVs.Count > 0)
	{	
		foreach (XmlNode n in TAVs)
		{
			MI_TechnicalAttribute ta = new MI_TechnicalAttribute();
					
			XmlNode alias_node = n.SelectSingleNode("ns:EntityTypeConfigurationTechnicalAttribute/ns:Alias", nsmgr);	
			
			if (alias_node != null)
			{
				ta.Alias = alias_node.InnerText;
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute Alias' attribute!");	
			}
				
			XmlNode valtyp_node = n.SelectSingleNode("ns:EntityTypeConfigurationTechnicalAttribute/ns:TechnicalAttribute/ns:ValueType", nsmgr);
				
			if (valtyp_node != null)
			{	
				ta.ValueType = MI_TechnicalAttribute.ParseType(valtyp_node.InnerText);
				
				switch(ta.ValueType)
				{
					case MI_TechnicalAttribute.Valtype.Boolean: 
					{
						XmlNode bv_node = n.SelectSingleNode("ns:BooleanValue", nsmgr);
						
						if (bv_node != null)
						{
							ta.Value = bv_node.InnerText;
						}
						else
						{
							AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute BooleanValue' attribute!");
						}
						
						break;
					}
					case MI_TechnicalAttribute.Valtype.Characters:
					{
						XmlNode cv_node = n.SelectSingleNode("ns:CharactersValue", nsmgr);
						
						if (cv_node != null)
						{
							ta.Value = cv_node.InnerText;
						}
						else
						{
							AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute CharactersValue' attribute!");
						}
						
						break;
					}
					case MI_TechnicalAttribute.Valtype.Numeric:
					{
						XmlNode nv_node = n.SelectSingleNode("ns:NumbersValue", nsmgr);
						
						if (nv_node != null)
						{
							ta.Value = nv_node.InnerText;
						}
						else
						{
							AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute NumbersValue' attribute!");
						}
						
						break;
					}
					case MI_TechnicalAttribute.Valtype.Date:
					{
						XmlNode dv_node = n.SelectSingleNode("ns:DateValue", nsmgr);
						
						if (dv_node != null)
						{
							ta.Value = dv_node.InnerText;
						}
						else
						{
							AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute DateValue' attribute!");
						}
						
						break;
					}
					case MI_TechnicalAttribute.Valtype.Lookup:
					{
						XmlNode lv_node = n.SelectSingleNode("ns:LookupValue", nsmgr);
						
						if (lv_node != null)
						{
							XmlNode lv_val_node = n.SelectSingleNode("ns:LookupValue/ns:Value", nsmgr);
							
							if (lv_val_node != null)
							{
								ta.Value = lv_val_node.InnerText;
							}
							else
							{
								AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'LookupValue Value' attribute!");
							}
						}
						else
						{
							AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute LookupValue' attribute!");
						}
						
						break;
					}
					case MI_TechnicalAttribute.Valtype.Error:
					{
						AppData.Popups.Push("Error parsing machine details" + "\n" + "Unknown value type in technical attribute for asset (ID =" + asset.CMMS_ID + ")");
					
						break;
					}
				};
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'TechnicalAttribute ValueType' attribute!");
			}
				
			asset.MachineDetails.TechnicalAttributes.Add(ta);	
		}
	}
		
	XmlNodeList ATs = root.SelectNodes("ns:Attachments/ns:EntityAttachment", nsmgr);	
			
	if (ATs.Count > 0)
	{	
		foreach (XmlNode n in ATs)
		{
			MI_Attachment att = new MI_Attachment(asset.AR_ID);
				
			XmlNode at_descr_node = n.SelectSingleNode("ns:Description", nsmgr);
			
			if (at_descr_node != null)
			{		
				att.Description = at_descr_node.InnerText;	
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Attachment Description' attribute!");	
			}
				
			XmlNode at_remarks_node = n.SelectSingleNode("ns:Remarks", nsmgr);
				
			if (at_remarks_node != null)
			{
				att.Remarks = at_remarks_node.InnerText;	
			}
			else
			{	
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Attachment Remarks' attribute!");	
			}
				
			XmlNode at_path_node = n.SelectSingleNode("ns:Path", nsmgr);	
				
			if (at_path_node != null)
			{
				att.The_Path = at_path_node.InnerText;
					
				string[] tokens = att.The_Path.Split(new string[] { "." }, StringSplitOptions.None);	
			
				att.Ending = (tokens.Length > 0) ? tokens[tokens.Length - 1] : "";	
				
				att.DetermineType();	
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Attachment Path' attribute!");	
			}
			
			XmlNode at_fl_node = n.SelectSingleNode("ns:FileName", nsmgr);
				
			if (at_fl_node != null)
			{
				att.FileName = at_fl_node.InnerText;	
			}
			else
			{
				AppData.Popups.Push("Asset [" + asset.CMMS_ID + "] doesn't have 'Attachment FileName' attribute!");		
			}
				
			asset.Attachments.Add(att);	
		}
	}	
	/*else 
	{
		if (asset.AR_ID == 5)	
			AddFakeAudioAttachment(asset);	
	}*/
		
	return true;	
}
	
	
static void AddFakeAudioAttachment(MI_Asset asset)
{
	MI_Attachment att = new MI_Attachment(asset.AR_ID);
		
	att.Description = "wav audio file";
		
	att.Ending = "wav";
		
	att.DetermineType();
		
	att.FileName = "Example.wav";
		
	att.Remarks = "play audio files inside app";
		
	att.The_Path = "http://160.40.50.160/armain/Example.wav";	
		
	asset.Attachments.Add(att);		
}
	
	
static void AddFakeTextAttachment(MI_Asset asset)
{
	MI_Attachment att = new MI_Attachment(asset.AR_ID);
		
	att.Description = "test txt file";
		
	att.Ending = "txt";
		
	att.DetermineType();
		
	att.FileName = "3dappdescription.txt";
		
	att.Remarks = "display txt files inside app";
		
	att.The_Path = "http://160.40.50.160/armain/3dappdescription.txt";	
		
	asset.Attachments.Add(att);	
}
	
	
}//class
