using System.Collections.Generic;
using System;


public class MI_TechnicalAttribute {

	public enum Valtype { 
		Numeric = 1, 
		Characters = 2,
		Date = 3,
		Boolean = 4,
		Lookup = 5,
		Error = 6
	};
		
	private static Dictionary<String, Valtype> m_strings;
	
	
static MI_TechnicalAttribute()
{
	m_strings = new Dictionary<string, Valtype>();
		
	m_strings.Add("Characters", Valtype.Characters);
	m_strings.Add("Numeric", Valtype.Numeric);
	m_strings.Add("Date", Valtype.Date);
	m_strings.Add("Boolean", Valtype.Boolean);
	m_strings.Add("Lookup", Valtype.Lookup);
}
	
	
public void Accept(Visitor visitor)
{
	visitor.CurrTA = this;	
		
	visitor.Visit(this);	
}
	
	
public static Valtype ParseType(String input)
{		
	return m_strings.ContainsKey(input) ? m_strings[input] : Valtype.Error;
}
	
	
public String Alias
{
	get;
	set;
}

	
public Valtype ValueType
{
	get;
	set;
}
	
	
public String Value
{
	get;
	set;
}
		
	
}//class
