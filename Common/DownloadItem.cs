using System.Collections.Generic;
using System;
using UnityEngine;


public class DownloadItem {


public DownloadItem(int arId, String url, String description, Action<WWW> callback)
{
	AR_ID = arId;
		
	URL = url;
	
	Description = description;
		
	Callback = callback;	
}
	
	
public int AR_ID
{
	get;
	private set;
}
	
	
public String URL
{
	get;
	private set;
}
	

public String Description
{
	get;
	set;
}

	
public Action<WWW> Callback
{
	get;
	private set;
}
	
	
}//class
