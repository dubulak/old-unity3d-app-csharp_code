using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO;
using System.Text;


public class MI_Attachment {
	
	public enum Type { Image, Audio, Video, Text, AutoCAD, Unhandled };
	
	private const String JPG_end = "jpg";
	private const String PNG_end = "png";
	
	private const String MP4_end = "mp4";
	private const String THREEGP_end = "3gp";
	
	private const String MP3_end = "mp3";
	private const String WAV_end = "wav"; 
	
	private const String TXT_end = "txt";
	
	private const String DWG_end = "dwg";
	
	
	private static Dictionary<String, String> m_typesSTR;
	
	
static MI_Attachment()
{
	m_typesSTR = new Dictionary<string, string>();	
		
	m_typesSTR.Add("xml", "Xml");	
	m_typesSTR.Add("txt", "Text");	
	m_typesSTR.Add("pdf", "PDF");
	m_typesSTR.Add("doc", "Word");
	m_typesSTR.Add("docx", "Word");	
	m_typesSTR.Add("xls", "Excel");	
	m_typesSTR.Add("xlt", "Excel");
	m_typesSTR.Add("xla", "Excel");
	m_typesSTR.Add("ppt", "Powerpoint");
	m_typesSTR.Add("pot", "Powerpoint");
	m_typesSTR.Add("pps", "Powerpoint");
	m_typesSTR.Add("ppa", "Powerpoint");
	m_typesSTR.Add("pptx", "Powerpoint");
	m_typesSTR.Add("potx", "Powerpoint");
	m_typesSTR.Add("ppsx", "Powerpoint");	
	m_typesSTR.Add("odt", "Open-Document");
	m_typesSTR.Add("fodt", "Open-Document");
	m_typesSTR.Add("odp", "Open-Presentation");
	m_typesSTR.Add("fodp", "Open-Presentation");
	m_typesSTR.Add("ods", "Open-Spreadsheet");
	m_typesSTR.Add("fods", "Open-Spreadsheet");
	m_typesSTR.Add("odg", "Open-Graphics");
	m_typesSTR.Add("fodg", "Open-Graphics");
	m_typesSTR.Add("mov", "Video");
	m_typesSTR.Add("flv", "Video");
	m_typesSTR.Add("avi", "Video");
	m_typesSTR.Add("wmv", "Video");	
	m_typesSTR.Add("dwg", "AutoCAD");
	m_typesSTR.Add("zip", "Zip");
	m_typesSTR.Add("gif", "Image");		
	m_typesSTR.Add("bmp", "Image");	
	m_typesSTR.Add("png", "Image");
	m_typesSTR.Add("jpg", "Image");
	m_typesSTR.Add("mkv", "Video");
	m_typesSTR.Add("mp4", "Video");
	m_typesSTR.Add("3gp", "Video");
	m_typesSTR.Add("mp3", "Audio");
	m_typesSTR.Add("wav", "Audio");
	m_typesSTR.Add("ogg", "Audio");	
}
	
	
public void DetermineType()
{
	if ((Ending == JPG_end) || (Ending == PNG_end))
	{
		The_Type = Type.Image;	
	}
	else if ((Ending == MP4_end) || (Ending == THREEGP_end))
	{
		The_Type = Type.Video;	
			
		Ready = true;	
			
		Progress = MI_Asset.PROGRESS_COMPLETE;	
			
		LocalDir = Application.persistentDataPath + AppData.FS_SEP + AR_ID.ToString();	
	}
	else if ((Ending == MP3_end) || (Ending == WAV_end))
	{
		The_Type = Type.Audio;
	}
	else if (Ending == TXT_end)
	{
		The_Type = Type.Text;	
	}
	else if (Ending == DWG_end)
	{
		The_Type = Type.AutoCAD;	
	}
	else 
	{
		The_Type = Type.Unhandled;		
	}		
}
	
	
public String TypeSTR
{
	get { return m_typesSTR.ContainsKey(Ending) ? m_typesSTR[Ending] : "Unknown type"; }
}
	
	
public bool Ready
{
	get;
	set;
}
		
	
public void SaveContent(WWW w)
{	
	if (The_Type == Type.Image)
	{
		Content = w.bytes;
			
		The_Texture = w.texture;
			
		SaveToFile();	
		
		Ready = true;	
	}
	else if (The_Type == Type.Audio)
	{
		Content = w.bytes;
			
		SaveToFile();
			
		Audio_Clip = w.audioClip;
			
		Ready = true;	
	}
	else if (The_Type == Type.Text)
	{
		Content = w.bytes;
			
		SaveToFile();	
			
		Text = Encoding.UTF8.GetString(Content);		
			
		Ready = true;	
	}
	else if (The_Type == Type.AutoCAD)
	{
		Content = w.bytes;	
			
		SaveToFile();
	}
	else if (The_Type == Type.Video)
	{
		Content = w.bytes;	
			
		SaveToFile();
			
		#if (UNITY_EDITOR) || (UNITY_STANDALONE)
		#elif (UNITY_ANDROID)
		CreateVideoThumbnail();
		#endif	
	}
	else
	{
		Content = w.bytes;	
			
		SaveToFile();	
	}		
}
			
	
private void CreateVideoThumbnail()
{	
	String folder = LocalDir + AppData.FS_SEP + "Thumbnails";
		
	try
	{
		if (!Directory.Exists(folder))
		{
			Directory.CreateDirectory(folder);
		}
			
		OpenFilePluginAPI.SaveFrameFromVideo(LocalPath, Thumbpath);		
	}
	catch (Exception e)
	{
		AppData.Popups.Push("Failed to create folder <" + folder + "> !");
	}
}
	
	
private String Thumbpath
{
	get { return LocalDir + AppData.FS_SEP + "Thumbnails" + AppData.FS_SEP + FileName + ".png"; }
}
	
	
public void LoadThumbnail()
{
	The_Texture = new Texture2D(2, 2);	
		
	try
	{
		The_Texture.LoadImage(File.ReadAllBytes(Thumbpath));	
	}
	catch (Exception e)
	{
		AppData.Popups.Push("Failed to load thumbnail for <" + Description + "> !" + "\n" + e.Message + "\n" + e.StackTrace);	
	}
}
	
	
public void SaveToFile()
{
	String dir_path = Application.persistentDataPath + AppData.FS_SEP + AR_ID.ToString();
		
	try
	{
		if (!Directory.Exists(dir_path))
		{
			Directory.CreateDirectory(dir_path);
		}
		
		LocalDir = dir_path;	
			
		LocalPath = dir_path + AppData.FS_SEP + FileName;
		
		File.WriteAllBytes(LocalPath, Content);	
			
		Ready = true;	
	}
	catch (Exception e)
	{
		AppData.Popups.Push("Failed to save <" +  Description + "> to disk!" + "\n" + e.Message + "\n" + e.StackTrace);	
			
		Ready = false;	
	}
}
		
	
public int AR_ID
{
	get;
	private set;
}
	
	
public String LocalDir
{
	get;
	private set;
}
	
	
public String LocalPath
{
	get;
	private set;
}
	
	
public Type The_Type
{
	get;
	private set;
}
	
	
public String FileName
{
	get;
	set;
}
	
	
public String The_Path
{
	get;
	set;
}
	

public String Description
{
	get;
	set;
}
	

public String Remarks
{
	get;
	set;
}
	
	
public String Ending
{
	get;
	set;
}
	
	
private byte[] Content
{
	get;
	set;
}
	
	
public Texture2D The_Texture
{
	get;
	private set;
}
	
	
public AudioClip Audio_Clip
{
	get;
	private set;
}
	
	
public float Progress
{
	get;
	set;
}
	
	
public String Text
{
	get;
	set;
}
	
		
public MI_Attachment(int arId)
{
	AR_ID = arId;
		
	Ready = false;
		
	Progress = 0;	
		
	The_Type = Type.Unhandled;	
		
	The_Texture = null;	
}
	
	
}//class
