using System.Collections.Generic;
using System;


public class MI_Asset {
		
	public const float PROGRESS_COMPLETE = 100;
	
	
public int AR_ID
{
	get;
	private set;
}

	
public String CMMS_ID
{
	get;
	set;
}

	
public bool Complete()
{
	bool attsComplete = true;
		
	if ((Attachments != null) && (Attachments.Exists(x => (x.Progress != PROGRESS_COMPLETE))))	
	{
		attsComplete = false;	
	}
		
	return  (MachineDetailsProgress == PROGRESS_COMPLETE) && 
			(WorkOrdersProgress == PROGRESS_COMPLETE) && 
			attsComplete;	
}
	
	
public bool MachineDetails_Ready
{
	get;
	set;
}
	

public bool WorkOrders_Ready
{
	get;
	set;
}
	
	
public MachineDetails MachineDetails
{
	get;
	set;
}
	

public List<MI_Attachment> Attachments
{
	get;
	set;
}
	

public List<WorkOrder> WorkOrders
{
	get;
	set;
}
	
	
public float WorkOrdersProgress
{
	get;
	set;
}
	
	
public float MachineDetailsProgress
{
	get;
	set;
}
	
	
public MI_Asset(int arId)
{	
	AR_ID = arId;
		
	WorkOrdersProgress = 0;	
		
	MachineDetailsProgress = 0;	
		
	MachineDetails_Ready = false;
	
	WorkOrders_Ready = false;
		
	MachineDetails = new MachineDetails();
		
	Attachments = new List<MI_Attachment>();
		
	WorkOrders = new List<WorkOrder>();
}
	
	
}//class
