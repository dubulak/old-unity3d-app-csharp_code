using System.Collections.Generic;
using System;


public class MachineDetails_Visitor : Visitor {

	private Dictionary<String, String> m_machDet_Content;
	private Dictionary<MI_TechnicalAttribute, Dictionary<String, String>> m_TAs_Content;
	
	
public MachineDetails_Visitor()
{
	m_machDet_Content = new Dictionary<string, string>();
	m_TAs_Content = new Dictionary<MI_TechnicalAttribute, Dictionary<string, string>>();	
}
	
	
public String GetString()
{
	String content = "";
		
	foreach (KeyValuePair<String, String> kvp in m_machDet_Content)
	{
		if (kvp.Key == "Serial number")
		{
			content += "Technical attributes:" + "\n";
				
			foreach (KeyValuePair<MI_TechnicalAttribute, Dictionary<String, String>> taKvp in m_TAs_Content)
			{
				content += "    " + (taKvp.Value.ContainsKey("Alias") ? taKvp.Value["Alias"] : "<missing name>") + ": ";
					
				content += (taKvp.Value.ContainsKey("Value") ? taKvp.Value["Value"] : "<missing value>") + "\n";	
			}
		}
			
		content += kvp.Key + ": " + kvp.Value + "\n";
	}
				
	return content;
}
	
	
public void Clear()
{
	m_machDet_Content.Clear();
	m_TAs_Content.Clear();	
}
	
	
public override void Visit(MachineDetails machineDetails)
{
	m_machDet_Content.Add("Description", machineDetails.Description);
	m_machDet_Content.Add("Type", machineDetails.MachineType);	
	m_machDet_Content.Add("Serial number", machineDetails.SerialNumber);
	m_machDet_Content.Add("Purchase cost", machineDetails.PurchaseCost);
	m_machDet_Content.Add("Guarantee expire date", machineDetails.GuaranteeExpiryDate); 
	m_machDet_Content.Add("Manufacturer", machineDetails.Manufacturer);
	m_machDet_Content.Add("Criticality", machineDetails.Criticality);	
}	
				
	
public override void Visit(MI_TechnicalAttribute ta)
{
	Dictionary<String, String> dict = new Dictionary<String, String>();
		
	dict.Add("Alias", ta.Alias);
	dict.Add("Value", ta.Value);	
			
	m_TAs_Content.Add(ta, dict);
}	
	
	
public override void Visit(MI_Attachment attachment)
{
	throw new NotImplementedException();
}

	
public override void Visit(WorkOrder workOrder)
{
	throw new NotImplementedException();
}
	
	
}//class
