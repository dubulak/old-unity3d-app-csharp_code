using System.Collections.Generic;
using System;


public static class MI_Asset_Cache {

	private static Dictionary<int, MI_Asset> m_cache;
	
	
static MI_Asset_Cache()
{
	m_cache = new Dictionary<int, MI_Asset>();	
}
	
	
public static void Clear()
{
	m_cache.Clear();	
}
	
	
public static void Add(MI_Asset asset)
{
	if (!m_cache.ContainsKey(asset.AR_ID))	
	{
		m_cache.Add(asset.AR_ID, asset);	
	}
}
	
	
public static MI_Asset Get(int arId)
{
	return (m_cache.ContainsKey(arId) ? m_cache[arId] : null);	
}
	
	
}//class
