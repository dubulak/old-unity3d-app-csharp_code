using System.Collections.Generic;
using System;


public class MachineDetails {
		
	
public void Accept(Visitor visitor)
{
	if (TechnicalAttributes != null)
	{
		TechnicalAttributes.ForEach(x => x.Accept(visitor));	
	}
		
	visitor.Visit(this);
}

	
public String Description
{
	get;
	set;
}
	

public String MachineType
{
	get;
	set;
}
	
	
public List<MI_TechnicalAttribute> TechnicalAttributes
{
	get;
	set;
}
	

public String SerialNumber
{
	get;
	set;		
}
	
	
public String PurchaseCost
{
	get;
	set;
}
	

public String GuaranteeExpiryDate
{
	get;
	set;
}
	
	
public String Manufacturer
{
	get;
	set;
}
	
	
public String Criticality
{
	get;
	set;
}
	
	
}//class
