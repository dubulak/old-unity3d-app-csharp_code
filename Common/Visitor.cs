using System.Collections.Generic;
using System;


public abstract class Visitor {
	
	
public MI_TechnicalAttribute CurrTA
{
	get;
	set;
}

	
public abstract void Visit(MachineDetails machineDetails);
	
public abstract void Visit(MI_Attachment attachment);
	
public abstract void Visit(WorkOrder workOrder);

public abstract void Visit(MI_TechnicalAttribute ta);	
	
}//class
