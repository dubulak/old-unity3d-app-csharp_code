using System;
using UnityEngine;
using System.Collections.Generic;


public static class OpenFilePluginAPI {
	
	private static AndroidJavaObject m_currentActivity;
	
	private static Dictionary<String, String> m_mimes;
	
	
static OpenFilePluginAPI()
{
	m_mimes = new Dictionary<string, string>();
	
	m_mimes.Add("xml", "text/xml");	
	m_mimes.Add("txt", "text/plain");	
	m_mimes.Add("pdf", "application/pdf");
	m_mimes.Add("doc", "application/msword");
	m_mimes.Add("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");	
	m_mimes.Add("xls", "application/vnd.ms-excel");	
	m_mimes.Add("xlt", "application/vnd.ms-excel");
	m_mimes.Add("xla", "application/vnd.ms-excel");
	m_mimes.Add("ppt", "application/vnd.ms-powerpoint");
	m_mimes.Add("pot", "application/vnd.ms-powerpoint");
	m_mimes.Add("pps", "application/vnd.ms-powerpoint");
	m_mimes.Add("ppa", "application/vnd.ms-powerpoint");
	m_mimes.Add("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
	m_mimes.Add("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
	m_mimes.Add("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");	
	m_mimes.Add("odt", "application/vnd.oasis.opendocument.text");
	m_mimes.Add("fodt", "application/vnd.oasis.opendocument.text");
	m_mimes.Add("odp", "application/vnd.oasis.opendocument.presentation");
	m_mimes.Add("fodp", "application/vnd.oasis.opendocument.presentation");
	m_mimes.Add("ods", "application/vnd.oasis.opendocument.spreadsheet");
	m_mimes.Add("fods", "application/vnd.oasis.opendocument.spreadsheet");
	m_mimes.Add("odg", "application/vnd.oasis.opendocument.graphics");
	m_mimes.Add("fodg", "application/vnd.oasis.opendocument.graphics");
	m_mimes.Add("mov", "video/quicktime");
	m_mimes.Add("flv", "video/x-flv");
	m_mimes.Add("avi", "video/x-msvideo");
	m_mimes.Add("wmv", "video/x-ms-wmv");	
	m_mimes.Add("dwg", "application/dwg");
	m_mimes.Add("zip", "application/x-compressed");
	m_mimes.Add("gif", "image/gif");		
	m_mimes.Add("bmp", "image/bmp");
}
	
	
public static void Initialise()
{		
	m_currentActivity = null;	
		
	AndroidJavaClass jc = null;
		
	try
	{
		jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
   
  		m_currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity"); 	
	}
	catch (Exception e)
	{
		AppData.Popups.Push("OpenFilePluginAPI init failed!" + "\n" +  e.Message + "\n" + e.StackTrace);	
	}
}
	
	
private static String GetMIMEtype(String ending)
{
	return m_mimes.ContainsKey(ending) ? m_mimes[ending] : "application/octet-stream";
}
	
	
public static void OpenFileWithExternalApp(String uri, String ending)
{	
	if (m_currentActivity != null)
	{
		String mimeType = GetMIMEtype(ending);	
			
		try
		{
			m_currentActivity.Call("OpenFile", uri, mimeType);	
		}
		catch (Exception e)
		{
			AppData.Popups.Push("OpenFile <" + uri + "> failed!" + "\n" + e.Message + "\n" + e.StackTrace);	
		}
	}
	else
	{
		AppData.Popups.Push("OpenFile failed: Java plugin not initialised!");	
	}
}
	
	
public static void OpenFolder(String folderPath)
{
	if (m_currentActivity != null)
	{
		try
		{
			m_currentActivity.Call("OpenFolder", folderPath);	
		}
		catch (Exception e)
		{
			AppData.Popups.Push("OpenFolder <" + folderPath + "> failed!" + "\n" + e.Message + "\n" + e.StackTrace);	
		}
	}
	else
	{
		AppData.Popups.Push("OpenFolder failed: Java plugin not initialised!");	
	}	
}
	
	
public static void SaveFrameFromVideo(String path, String framePath)
{
	if (m_currentActivity != null)
	{
		try
		{
			m_currentActivity.Call("SaveFrameFromVideo", path, framePath);	
		}
		catch (Exception e)
		{
			AppData.Popups.Push("SaveFrameFromVideo <" + path + "> failed!" + "\n" + e.Message + "\n" + e.StackTrace);	
		}
	}
	else
	{
		AppData.Popups.Push("SaveFrameFromVideo failed: Java plugin not initialised!");	
	}			
}
	
	
}//class
