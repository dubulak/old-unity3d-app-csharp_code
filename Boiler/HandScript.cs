using UnityEngine;
using System.Collections;
using System;


public class HandScript : MonoBehaviour {

	private Action m_action;
	
	private Vector3 m_gotoPosition;
	
	private long m_steps;
	
	private bool m_moving;
	
	
void Start()
{
	Utils.SetVisible(transform, false);
		
	m_moving = false;
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Hidden_State));
}


private void Hidden_State()
{
	Utils.SetVisible(transform, false);
			
	m_moving = false;
}
	
	
public void Move(Vector3 gotoPosition, Action doneCallback)
{
	m_gotoPosition = gotoPosition;
		
	transform.position = new Vector3(m_gotoPosition.x, m_gotoPosition.y, m_gotoPosition.z - 0.3f);
			
	m_action = doneCallback;
		
	m_steps = 0;
		
	m_moving = true;
		
	Utils.SetVisible(transform, true);	
}
	
	
void Update()
{
	if (m_moving)
	{
		m_steps++;
			
		transform.position = Vector3.Lerp(transform.position, m_gotoPosition, Time.deltaTime * m_steps * 0.1f);	
			
		m_moving = (transform.position != m_gotoPosition); 	
			
		if (!m_moving)
		{
			Utils.SetVisible(transform, false);	
				
			if (m_action != null)
			{
				m_action.Invoke();	
			}
		}
	}
}
	
	
}//class
