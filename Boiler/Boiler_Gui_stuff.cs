using UnityEngine;
using System.Collections.Generic;
using System;


public class Boiler_Gui_stuff : MonoBehaviour {
	
	public Transform m_boiler;
	public Transform m_oilTank;
	public Transform m_hand;
	public Transform m_buttonPosition;
	public Transform m_valvePosition;
	
	public Font font8;
	public Font font10;
	public Font font12;
	public Font font14;
	public Font font16;
	public Font font18;
	public Font font20;
	public Font font22;
	public Font fontHD;
	
	public Texture2D m_ok_Img;
	public Texture2D m_restart_Img;
	public Texture2D m_info_Img;
	
	public Texture2D m_playBlue_Img;
	public Texture2D arrowDown;
	public Texture2D arrowUp;
	public Texture2D arrowRight;
	public Texture2D quit;
	public Texture2D arrowBlue;
	public Texture2D arrowBlueLeft;
	public Texture2D m_redButton_Img;
	
	public Transform tools;
	public Transform arrow;
	
	public Transform m_markerButton;
	public Transform m_markerValve;
	public Transform m_markerBurner;
	public Transform m_markerDoor;
	public Transform m_markerInternals;
	public Transform m_markerScrew;
	public Transform m_markerFlue;	
	
	public GUISkin guiSkin;
	
	private bool m_displayingSteps;
	private bool m_displayingInfo;
	private bool m_displayMsgBox;
	private bool m_displayPlayBtn;
	
	private Dictionary<Boiler_StateRegistry.Step, String> m_descriptions;
	private Dictionary<Boiler_StateRegistry.Step, Action> m_actions;
	private Dictionary<Boiler_StateRegistry.Step, Transform> m_markers;
	private Dictionary<Boiler_StateRegistry.Step, String> m_titles;
	
	private Boiler_StateRegistry.Step m_currIndex;
		
	private String m_msg;
		
	private const float GUI_MARGIN_PCNT = 0.012f;
	
	private Gui_CMMS m_Gui_CMMS;
	
	private Transform m_button;
	private Transform m_valve;
	private Transform m_oilBurner;
	private Transform m_door;
	private Transform m_vacuumCleaner;
	private Transform m_screwdriver;
	private Transform m_gasAnalyzer;
	
	
void Start() 
{				
	m_displayingSteps = false;
	m_displayingInfo = true;
	m_displayMsgBox = false;
	m_displayPlayBtn = true;
	AppData.GuiStuff_DisplayEndMsg = false;
		
		
	m_descriptions = new Dictionary<Boiler_StateRegistry.Step, string>();
		
	m_descriptions.Add(Boiler_StateRegistry.Step.TurnOffBoiler, "Turn boiler off\n\nLocate the power switch on the boiler's control panel.\n\nTurn the boiler off.");
	m_descriptions.Add(Boiler_StateRegistry.Step.CutOffOilSupply, "Cut off the oil supply\n\nClose the valve controlling the oil supply between the burner and the oil tank.");
	m_descriptions.Add(Boiler_StateRegistry.Step.RemoveBurner, "Remove burner\n\nLoosen the bolts connecting the burner to the boiler and remove the burner.");
	m_descriptions.Add(Boiler_StateRegistry.Step.OpenDoor, "Open the boiler's door\n\nLocate the handle on the right side of the boiler's door and open the door.\n\n");
	m_descriptions.Add(Boiler_StateRegistry.Step.CleanBoiler, "Clean the boiler\n\nUse a vacuum cleaner or a water hose to clean the internals of the boiler.");
	m_descriptions.Add(Boiler_StateRegistry.Step.CloseDoor, "Close the boiler's door\n\nClose the door and secure it by adjusting the handle.");
	m_descriptions.Add(Boiler_StateRegistry.Step.InsertBurner, "Connect burner\n\nConnect the burner back to the boiler and tighten the bolts.");
	m_descriptions.Add(Boiler_StateRegistry.Step.RestoreOilSupply, "Restore the oil supply\n\nOpen the valve controlling the oil supply between the burner and the oil tank.");	
	m_descriptions.Add(Boiler_StateRegistry.Step.RegulateBurner, "Regulate burner\n\nUse a screwdriver to tighten/loosen the screw regulating the burner.");
	m_descriptions.Add(Boiler_StateRegistry.Step.CheckGases, "Check gases\n\nConnect a flue gas analyzer to the flue on the back of the boiler.\n\nThe boiler's efficiency should exceed 90%.");
		
	m_titles = new Dictionary<Boiler_StateRegistry.Step, String>();
		
	foreach (KeyValuePair<Boiler_StateRegistry.Step, String> kvp in m_descriptions)
	{
		m_titles.Add(kvp.Key, kvp.Value.Split('\n')[0]);	
	}
			
	m_markers = new Dictionary<Boiler_StateRegistry.Step, Transform>();
		
	m_markers.Add(Boiler_StateRegistry.Step.TurnOffBoiler, m_markerButton);
	m_markers.Add(Boiler_StateRegistry.Step.CutOffOilSupply, m_markerValve);
	m_markers.Add(Boiler_StateRegistry.Step.RemoveBurner, m_markerBurner);
	m_markers.Add(Boiler_StateRegistry.Step.OpenDoor, m_markerDoor);
	m_markers.Add(Boiler_StateRegistry.Step.CleanBoiler, m_markerInternals);
	m_markers.Add(Boiler_StateRegistry.Step.CloseDoor, m_markerDoor);
	m_markers.Add(Boiler_StateRegistry.Step.InsertBurner, m_markerBurner);
	m_markers.Add(Boiler_StateRegistry.Step.RestoreOilSupply, m_markerValve);
	m_markers.Add(Boiler_StateRegistry.Step.RegulateBurner, m_markerScrew);
	m_markers.Add(Boiler_StateRegistry.Step.CheckGases, m_markerFlue);
		
	m_actions = new Dictionary<Boiler_StateRegistry.Step, Action>();
		
	m_actions.Add(Boiler_StateRegistry.Step.TurnOffBoiler, new Action(TurnOffBoiler));
	m_actions.Add(Boiler_StateRegistry.Step.CutOffOilSupply, new Action(CutOffOilSupply));
	m_actions.Add(Boiler_StateRegistry.Step.RemoveBurner, new Action(RemoveBurner));
	m_actions.Add(Boiler_StateRegistry.Step.OpenDoor, new Action(OpenDoor));
	m_actions.Add(Boiler_StateRegistry.Step.CleanBoiler, new Action(CleanBoiler));
	m_actions.Add(Boiler_StateRegistry.Step.CloseDoor, new Action(CloseDoor));
	m_actions.Add(Boiler_StateRegistry.Step.InsertBurner, new Action(InsertBurner));
	m_actions.Add(Boiler_StateRegistry.Step.RestoreOilSupply, new Action(RestoreOilSupply));
	m_actions.Add(Boiler_StateRegistry.Step.RegulateBurner, new Action(RegulateBurner));
	m_actions.Add(Boiler_StateRegistry.Step.CheckGases, new Action(CheckGases));
		
	m_currIndex = Boiler_StateRegistry.Step.TurnOffBoiler;
		
	AppData.AllowTracking = (m_markers[m_currIndex] != null);	
		
	m_button = m_boiler.FindChild("inclined_plane").FindChild("button");
	m_valve = m_oilTank.FindChild("Hose").FindChild("valve");	
	m_oilBurner = m_boiler.FindChild("oilBurner");	
	m_door = m_boiler.FindChild("doorParent");	
	m_vacuumCleaner = m_boiler.FindChild("Handy Cleaner");
	m_screwdriver = m_oilBurner.FindChild("Screwdriver");
	m_gasAnalyzer = m_boiler.FindChild("gadget");	
		
	HandleTracking();	
		
	m_Gui_CMMS = transform.GetComponent<Gui_CMMS>();	
}
	
	
private void HandleTracking()
{
	if (!AppData.AllowTracking)
	{
		TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Stop();
			
		Camera.main.GetComponent<ResetCameraHandle>().Reset();	
	}
	else
	{
		TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Start();	
	}
}
	
	
void TurnOffBoiler()
{
	m_hand.GetComponent<HandScript>().Move(m_buttonPosition.position, new Action(PushButton));
}
	
	
void PushButton()
{
	m_button.GetComponent<ButtonScript>().Push(new Action(DoneCallback));		
}
	
	
void CutOffOilSupply()
{
	m_hand.GetComponent<HandScript>().Move(m_valvePosition.position, new Action(CloseValve));
}
	
	
void CloseValve()
{
	m_valve.GetComponent<Boiler_ValveHandle>().Close(new Action(DoneCallback));			
}
	
	
void RemoveBurner()
{
	m_oilBurner.GetComponent<BurnerScript>().RemoveBurner(new Action(DoneCallback));	
}
	

void OpenDoor()
{
	m_door.GetComponent<DoorHandle>().Open(new Action(DoneCallback));	
}
	

void CleanBoiler()
{
	m_vacuumCleaner.GetComponent<VacuumScript>().Clean(new Action(DoneCallback));	
}
	
	
void CloseDoor()
{
	m_door.GetComponent<DoorHandle>().Close(new Action(DoneCallback));	
}
	
	
void InsertBurner()
{
	m_oilBurner.GetComponent<BurnerScript>().ConnectBurner(new Action(DoneCallback));	
}
	
	
void RestoreOilSupply()
{
	m_hand.GetComponent<HandScript>().Move(m_valvePosition.position, new Action(OpenValve));
}
	
	
void OpenValve()
{
	m_valve.GetComponent<Boiler_ValveHandle>().Open(new Action(DoneCallback));			
}
	
	
void RegulateBurner()
{
	m_screwdriver.GetComponent<ScrewdriverScript>().Screw(new Action(DoneCallback));	
}
	

void CheckGases()
{
	m_gasAnalyzer.GetComponent<FlueGasAnalyzerScript>().Connect(new Action(DoneCallback));
}
	
	
void EndCallback()
{
	DoneCallback();
		
	AppData.GuiStuff_DisplayEndMsg = true;
		
	m_Gui_CMMS.ClearMiddle();	
}
	
		
private void RestoreState(Boiler_StateRegistry.Step step)
{		
	AppData.AllowTracking = (m_markers[m_currIndex] != null);
		
	Boiler_StateRegistry.RestoreState(step);	
		
	HandleTracking();	
				
	m_displayPlayBtn = true;
	m_displayMsgBox = false;
	m_displayingInfo = true;
	m_displayingSteps = false;	
	AppData.GuiStuff_DisplayEndMsg = false;	
}
	
	
public void OnTrackableChange()
{
	bool tmp = AppData.GuiStuff_DisplayEndMsg;
			
	RestoreState(m_currIndex);
		
	if (m_displayMsgBox && ((m_markers[m_currIndex] == null) || (TrackedRegistry.Tracked_ID == m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID)))
		m_displayMsgBox = false;

	if (!m_displayMsgBox)
		m_displayPlayBtn = true;	
		
	AppData.GuiStuff_DisplayEndMsg = tmp;
}
	
	
public void DoneCallback()
{
	m_displayPlayBtn = true;	
}
	
	
void OutOfSight(Transform t)
{
	t.position = tools.FindChild("pos_outOfSight").position;
}
	

void DoNothing(){}	

	
void OnGUI() 
{		
	GUI.skin = guiSkin;	
		
	int s = (int)(Screen.width * 0.015625);	

	Font x = font20;
		
	if (s <= 9)
		x = font8;	
	else if ((s > 9) && (s <= 11))
		x = font10;
	else if ((s > 11) && (s <= 13))
		x = font12;
	else if ((s > 13) && (s <= 15))
		x = font14;
	else if ((s > 15) && (s <= 17))
		x = font16;
	else if ((s > 17) && (s <= 19))
		x = font16;
	else if ((s > 19) && (s <= 21))
		x = font18;
	else if ((s > 21) && (s <= 23))
		x = font20;
	else if ((s > 23) && (s <= 25))
		x = font22;
	else if (s > 25)
		x = fontHD;
		
	GUI.skin.button.font = 	x;
	GUI.skin.label.font = x;
	GUI.skin.font = x;
	GUI.skin.box.font = x;
		
	if (!m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_Title();	
	}
		
	if (!m_Gui_CMMS.Get_ConfirmQuit() && !m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_std();
		
		if (m_displayingSteps)
			GUI_steps();
		
		if (!m_displayingSteps)
			GUI_info();
		
		if (m_displayMsgBox)
			GUI_msgBox();
		
		if (AppData.GuiStuff_DisplayEndMsg)
			GUI_EndMsg();
	}
		
	if (AppData.GuiStuff_ClearMiddle_NEEDED)
	{
		ClearMiddle();
			
		AppData.GuiStuff_ClearMiddle_NEEDED = false;
	}	
}

	
void Update()
{
	if (!m_Gui_CMMS.Get_ConfirmQuit() && !m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_arrow();
	}
}
		
	
void GUI_EndMsg()
{
	m_Gui_CMMS.ClearMiddle();	

	GUIContent msg_gc = new GUIContent(" The steam pipe replacement \n process is complete!", m_info_Img);	
	Vector2 msg_size = guiSkin.GetStyle("Box").CalcSize(msg_gc);	
		
	Rect msgRect = new Rect(
			(Screen.width * 0.5f) - ((msg_size.x + 20) * 0.5f), 
			(Screen.height * 0.5f) - ((msg_size.y + 20) * 0.5f), 
			msg_size.x + 20, 
			msg_size.y + 20);		
		
	GUI.Box(msgRect, msg_gc);
	
	GUIContent restart_gc = new GUIContent(" Restart ", m_restart_Img);
	Vector2 restart_size = guiSkin.GetStyle("Button").CalcSize(restart_gc);
		
	Rect restartRect = new Rect(
			msgRect.x + (msgRect.width * 0.5f) - (Screen.width * GUI_MARGIN_PCNT * 0.5f) - (restart_size.x + 10), 
			msgRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f), 
			restart_size.x + 10, 
			restart_size.y + 10);
	
	if (GUI.Button(restartRect, restart_gc))
	{
		AppData.GuiStuff_DisplayEndMsg = false;
			
		m_currIndex = Boiler_StateRegistry.Step.TurnOffBoiler;
			
		RestoreState(m_currIndex);
	}
	
	GUIContent ok_gc = new GUIContent(" OK", m_ok_Img);
	Vector2 ok_size = guiSkin.GetStyle("Button").CalcSize(ok_gc);
		
	Rect okRect = new Rect(
			msgRect.x + (msgRect.width * 0.5f) + (Screen.width * GUI_MARGIN_PCNT * 0.5f), 
			msgRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f), 
			restartRect.width, 
			ok_size.y + 10);
		
	if (GUI.Button(okRect, ok_gc))
	{
		AppData.GuiStuff_DisplayEndMsg = false;	
	}
}
	
	
void GUI_msgBox()
{
	if (GUI.Button(new Rect(Screen.width * 0.4f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.1f), m_msg))
	{
		m_displayMsgBox = false;
		m_displayPlayBtn = true;
	}
}
	 	 
	
void GUI_arrow()
{
	if (m_displayMsgBox)
	{
		if ((m_markers[m_currIndex] != null) && (TrackedRegistry.Tracked_ID != TrackedRegistry.INVALID_ID))
		{
			arrow.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.35f, Camera.main.nearClipPlane * 4));
				
			arrow.LookAt(m_markers[m_currIndex]);	
		}
		else
		{
			OutOfSight(arrow);	
		}
	}
	else
	{
		OutOfSight(arrow);	
	}
}
	
	
void GUI_Title()
{
	Rect titleRect = new Rect(Screen.width * 0.4f, Screen.height * 0.02f, Screen.width * 0.2f, Screen.height * 0.05f);
		
	GUI.Box(titleRect, "Boiler cleaning");		
}
	
	
private Rect m_stepsRect;
	
void GUI_std()
{
	Rect playRect = new Rect(Screen.width * 0.45f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.067f);
	Rect quitRect = new Rect((Screen.width * (1 - GUI_MARGIN_PCNT)) - Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.067f);	
	Rect prevRect = new Rect(Screen.width * GUI_MARGIN_PCNT, Screen.height * 0.1f, Screen.width * 0.04f, Screen.height * 0.067f);
	Rect nextRect = new Rect(prevRect.xMax, Screen.height * 0.1f, Screen.width * 0.04f, Screen.height * 0.067f);
	m_stepsRect = new Rect(nextRect.xMax, Screen.height * 0.1f, Screen.width * 0.075f, Screen.height * 0.067f);		

	GUI.enabled = (m_currIndex != Boiler_StateRegistry.Step.TurnOffBoiler);
		
	if (GUI.Button(prevRect, new GUIContent(arrowBlueLeft)))
	{
		m_currIndex--;	
			
		RestoreState(m_currIndex);
	}
		
	GUI.enabled = true;	
		
		
	GUI.enabled = (m_currIndex != Boiler_StateRegistry.Step.CheckGases);	
	
	if (GUI.Button(nextRect, new GUIContent(arrowBlue)))
	{
		m_currIndex++;
			
		RestoreState(m_currIndex);
	}
	
	GUI.enabled = true;	
		
	if (GUI.Button(m_stepsRect, new GUIContent("  Steps", m_displayingSteps ? arrowDown : arrowUp)))
	{
		m_displayingSteps = !m_displayingSteps;		
	}
		
	if (m_displayPlayBtn)
	{
		GUI.enabled = !m_Gui_CMMS.Get_ShowingPopup();
			
		if (GUI.Button(playRect, new GUIContent("  Show me!", m_playBlue_Img)))
		{
			m_Gui_CMMS.ClearMiddle();
				
			if ((m_markers[m_currIndex] == null) || TrackedRegistry.HasTrackable(m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID))
			{
				RestoreState(m_currIndex);
				m_displayPlayBtn = false;
				//m_displayingInfo = false; //only for PC-Test
				m_actions[m_currIndex].Invoke();	
			}
			else
			{
				m_msg = "Need to track marker " + m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID + " first!";
				m_displayMsgBox = true;	
				m_displayPlayBtn = false;	
			}	
		}
			
		GUI.enabled = true;	
	}
}

		
private void ClearMiddle()
{
	AppData.GuiStuff_DisplayEndMsg = false;	
	m_displayMsgBox = false;
	m_displayPlayBtn = true;		
}
	
	
void GUI_steps()
{
	Rect r1 = new Rect(Screen.width * 0.05f, Screen.height * 0.2f, Screen.width * 0.25f, Screen.height * 0.1f);	
		
	foreach (KeyValuePair<Boiler_StateRegistry.Step, String> kvp in m_titles)	
	{
		int i = (int)kvp.Key;	
			
		if (GUI.Button(
				new Rect(Screen.width * GUI_MARGIN_PCNT, m_stepsRect.yMax + (i * (r1.height / 2)), r1.width, r1.height / 2), 
				(m_currIndex == kvp.Key) ? new GUIContent("  Step " + (i + 1) + ": " + kvp.Value, m_redButton_Img) :
				new GUIContent("Step " + (i + 1) + ": " + kvp.Value)))
		{
			m_currIndex = kvp.Key;
				
			RestoreState(m_currIndex);
		}
	}
}
	

void GUI_info()
{
	Rect r1 = new Rect(Screen.width * GUI_MARGIN_PCNT, Screen.height * 0.25f, Screen.width * 0.25f, Screen.height * 0.067f);
	Rect r2 = new Rect(r1.x, r1.y + r1.height + Screen.height * 0.01f, r1.width, Screen.height * 0.4f);
	Rect r3 = new Rect(r2.x + Screen.width * 0.02f, r2.y + Screen.height * 0.02f, r2.width - (2 * Screen.width * 0.02f), r2.height - (2 * Screen.height * 0.02f));
		
	if (GUI.Button(r1, new GUIContent("  Step " + (int)(m_currIndex + 1) + ": " + m_titles[m_currIndex], m_displayingInfo ? arrowDown : arrowUp)))
	{
		m_displayingInfo = !m_displayingInfo;	
	}
		
	if (m_displayingInfo)
	{
		GUI.Box(r2, new GUIContent());		
			
		GUI.Label(r3, m_descriptions[m_currIndex]);		
	}	
}

	
}//class
