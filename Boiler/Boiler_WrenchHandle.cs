using UnityEngine;
using System.Collections.Generic;
using System;


public class Boiler_WrenchHandle : MonoBehaviour {

	private Action m_action;	
	
	private int m_timesPlayed;
	
	private Vector3 m_startPosition;
	
	private Vector3 m_startAngles;
	
	private Vector3 m_awayPosition;
	
	private long m_steps;
	
	private bool m_screwing;
	
	
void Start() 
{
	m_timesPlayed = 0;
		
	m_startPosition = transform.localPosition;	
		
	m_startAngles = transform.localEulerAngles;	
		
	m_awayPosition = transform.parent.FindChild("AwayPos").localPosition;
		
	SetVisible(false);	
		
	Approaching = false;	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Hidden_State));
}
			
	
private void Hidden_State()
{
	SetVisible(false);
		
	animation.Stop();
		
	Approaching = false;
}
	
	
public void SetVisible(bool visible)
{
	Renderer[] rends = transform.FindChild("innerWrench").GetComponentsInChildren<Renderer>();	

	foreach (Renderer r in rends)
	{
		r.enabled = visible;	
	}		
		
	transform.renderer.enabled = false;	
}
	
	
void OnAnimationFinished()
{
	m_timesPlayed++;
		
	if (m_timesPlayed == 3)
	{
		SetVisible(false);	
			
		if (m_action != null)
		{
			m_action.Invoke();	
		}
	}
}
	
	
private bool Approaching
{
	get;
	set;
}

	
public void Screw(Action action)
{
	m_screwing = true;	
		
	m_timesPlayed = 0;
		
	m_action = action;
		
	Approaching = true;
		
	m_steps = 0;
		
	transform.localPosition = m_awayPosition;
		
	transform.localEulerAngles = m_startAngles;
		
	SetVisible(true);	
}
	
	
public void Unscrew(Action action)
{
	m_screwing = false;	
		
	m_timesPlayed = 0;	
		
	m_action = action;	
		
	Approaching = true;	
		
	m_steps = 0;	
		
	transform.localPosition = m_awayPosition;
		
	transform.localEulerAngles = m_startAngles;
		
	SetVisible(true);
}
		
	
void Update()
{
	if (Approaching)
	{
		m_steps++;	
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_startPosition, Time.deltaTime * m_steps * 0.1f);
			
		Approaching = (transform.localPosition != m_startPosition);	
			
		if (!Approaching)
		{
			Animation an = GetComponent<Animation>();
				
			m_timesPlayed = 0;	
				
			String clip = (m_screwing) ? "WrenchScrew" : "WrenchUnScrew";	
	
			an.PlayQueued(clip, QueueMode.PlayNow);
			an.PlayQueued(clip, QueueMode.CompleteOthers);	
			an.PlayQueued(clip, QueueMode.CompleteOthers);	
		}
	}
}
	

}//class
