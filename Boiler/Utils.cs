using UnityEngine;
using System.Collections.Generic;
using System;


public static class Utils {
	
	
public static void SetVisible(Transform t, bool visible)
{
	Renderer[] rends = t.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in rends)
	{
		r.enabled = visible;	
	}
}
	
	
}//class
