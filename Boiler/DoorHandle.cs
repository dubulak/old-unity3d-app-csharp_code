using UnityEngine;
using System.Collections;
using System;


public class DoorHandle : MonoBehaviour {

	private Action m_doneCallback;
	
	private Vector3 m_closed_handle_angles;
	
	private Vector3 m_open_handle_angles;
	
	private bool m_openingHandle;
	
	private bool m_closingHandle;

	private Transform m_parentHandle;
	
	private float m_handleTime;
	
	private bool m_closing;
	
	private Vector3 m_startAngles;
	
	private Vector3 m_openAngles;
	
	
void Start()
{		
	m_closing = false;
		
	m_handleTime = 0;	
		
	m_openingHandle = false;	
		
	m_closingHandle = false;	
		
	m_doneCallback = null;
		
	m_startAngles = transform.localEulerAngles;	
	
	m_openAngles = new Vector3(0, 90, 0);
	
	m_parentHandle = transform.FindChild("ParentHandle");	
		
	m_closed_handle_angles = m_parentHandle.localEulerAngles;	
		
	m_open_handle_angles = new Vector3(0, 0, 270);	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Closed_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.CleanBoiler, new Action(Opened_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.InsertBurner, new Action(Closed_State));	
}

	
private void Opened_State()
{
	animation.Stop();
		
	m_openingHandle = false;
		
	m_closingHandle = false;
		
	m_parentHandle.localEulerAngles = m_open_handle_angles;	
		
	transform.localEulerAngles = m_openAngles;	
}
	

private void Closed_State()
{
	animation.Stop();
		
	m_openingHandle = false;
		
	m_closingHandle = false;
		
	m_parentHandle.localEulerAngles = m_closed_handle_angles;	
		
	transform.localEulerAngles = m_startAngles;	
}
	
	
public void Open(Action doneCallback)
{
	if (animation.isPlaying)
	{
		return;	
	}
		
	m_doneCallback = doneCallback;	
		
	OpenHandle();
}
	
	
private void OpenHandle()
{
	if (m_openingHandle || m_closingHandle)
	{
		return;	
	}
		
	m_parentHandle.localEulerAngles = m_closed_handle_angles;
		
	m_handleTime = 0;	
		
	m_openingHandle = true;
}
	
	
private void CloseHandle()
{
	if (m_closingHandle || m_openingHandle)
	{
		return;
	}
		
	m_parentHandle.localEulerAngles = m_open_handle_angles;
		
	m_handleTime = 0;
		
	m_closingHandle = true;
}
	
	
public void Close(Action doneCallback)
{
	if (animation.isPlaying)
	{
		return;	
	}
		
	m_closing = true;	
		
	m_doneCallback = doneCallback;
	
	animation.Play("CloseDoor");
}
	
	
void OnAnimFinished()
{
	if (m_closing)
	{
		CloseHandle();	
	
		m_closing = false;
	}
		
	if (m_doneCallback != null)
	{
		m_doneCallback.Invoke();	
	}
}

	
void Update()
{
	if (m_openingHandle)
	{
		m_handleTime += Time.deltaTime;	
			
		m_parentHandle.Rotate(Vector3.back, 90 * Time.deltaTime);	
			
		if (m_handleTime >= 1)
		{
			m_openingHandle = false;	
				
			animation.Play("OpenDoor");	
		}
	}
		
	if (m_closingHandle)
	{
		m_handleTime += Time.deltaTime;
			
		m_parentHandle.Rotate(Vector3.forward, 90 * Time.deltaTime);
			
		if (m_handleTime >= 1)
		{
			m_closingHandle = false;
		}
	}
}
	
	
}//class
