using UnityEngine;
using System.Collections;
using System;


public class BurnerScript : MonoBehaviour {

	private Transform m_boltLeft;
	private Transform m_boltRight;
	
	private Transform m_wrenchLeft;
	private Transform m_wrenchRight;
	
	private bool m_removing;
	private bool m_connecting;
	
	private Vector3 m_endPosition;
	
	private Vector3 m_startPosition;
	
	private Action m_doneCallback;
	
	private long m_steps;
	
	
void Start()
{
	m_removing = false;	
	m_connecting = false;	
		
	m_boltLeft = transform.FindChild("boltLeft");
	m_boltRight = transform.FindChild("boltRight");	
		
	m_wrenchLeft = transform.FindChild("WrenchLeft");
	m_wrenchRight = transform.FindChild("WrenchRight");
		
	m_startPosition = transform.localPosition;	
		
	float distance = transform.renderer.bounds.size.y;	
		
	m_endPosition = new Vector3(m_startPosition.x, m_startPosition.y, m_startPosition.z - distance);	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Connected_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.OpenDoor, new Action(Disconnected_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.RestoreOilSupply, new Action(Connected_State));
}
	
	
private void Connected_State()
{
	Utils.SetVisible(transform, true);
	
	SetLeftBoltVisible(true);
	SetRightBoltVisible(true);
		
	SetWrenchesVisible(false);
		
 	Utils.SetVisible(transform.FindChild("Screwdriver"), false);
		
	transform.localPosition = m_startPosition;	
		
	m_removing = false;
		
	m_connecting = false;
}

	
private void Disconnected_State()
{
	Utils.SetVisible(transform, false);
		
	transform.localPosition = m_endPosition;
		
	m_removing = false;
		
	m_connecting = false;	
}
	
	
public void SetLeftBoltVisible(bool visible)
{
	Utils.SetVisible(m_boltLeft, visible);
}
		
	
public void SetRightBoltVisible(bool visible)
{
	Utils.SetVisible(m_boltRight, visible);
}
	
	
private void SetWrenchesVisible(bool visible)
{
	m_wrenchLeft.GetComponent<Boiler_WrenchHandle>().SetVisible(visible);
	m_wrenchRight.GetComponent<Boiler_WrenchHandle>().SetVisible(visible);
}
	
	
public void RemoveBurner(Action doneCallback)
{
	m_doneCallback = doneCallback;
		
	m_wrenchLeft.GetComponent<Boiler_WrenchHandle>().Unscrew(new Action(Seq2_Remove));	
}
	
	
private void StartRemoving()
{		
	m_removing = true;		
		
	m_steps = 0;	
}
	
	
public void ConnectBurner(Action doneCallback)
{
	m_doneCallback = doneCallback;
		
	Utils.SetVisible(transform, true);
	
	SetLeftBoltVisible(false);
	SetRightBoltVisible(false);
		
	SetWrenchesVisible(false);
		
 	Utils.SetVisible(transform.FindChild("Screwdriver"), false);
		
	transform.localPosition = m_endPosition;	
		
	m_connecting = true;
		
	m_steps = 0;	
}
	
	
void Update()
{
	if (m_removing)
	{
		m_steps++;	
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_endPosition, Time.deltaTime * m_steps * 0.1f);
			
		m_removing = (transform.localPosition != m_endPosition);	
			
		if (!m_removing)
		{
			Utils.SetVisible(transform, false);
				
			if (m_doneCallback != null)
			{
				m_doneCallback.Invoke();	
			}
		}
	}
		
	if (m_connecting)
	{
		m_steps++;
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_startPosition, Time.deltaTime * m_steps * 0.1f);
			
		m_connecting = (transform.localPosition != m_startPosition);
			
		if (!m_connecting)
		{			
			Seq2_Connect();
		}
	}
}
	
			
private void Seq2_Connect()
{
	SetRightBoltVisible(true);
		
	m_wrenchRight.GetComponent<Boiler_WrenchHandle>().Screw(new Action(Seq3_Connect));	
}
	
	
private void Seq3_Connect()
{
	SetLeftBoltVisible(true);
		
	m_wrenchLeft.GetComponent<Boiler_WrenchHandle>().Screw(m_doneCallback);
}
	
	
private void Seq2_Remove()
{
	Utils.SetVisible(m_boltLeft, false);			
		
	m_wrenchRight.GetComponent<Boiler_WrenchHandle>().Unscrew(new Action(Seq3_Remove));	
}
	
	
private void Seq3_Remove()
{
	Utils.SetVisible(m_boltRight, false);
		
	StartRemoving();	
}
	
	
}//class
