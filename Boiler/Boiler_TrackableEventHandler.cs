/*==============================================================================
            Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary
==============================================================================*/

using UnityEngine;
using System;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class Boiler_TrackableEventHandler : MonoBehaviour,
                                            ITrackableEventHandler
{
	public Transform m_machine;
	
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
    #endregion // PRIVATE_MEMBER_VARIABLES



    #region UNTIY_MONOBEHAVIOUR_METHODS
    
    void Start()
    {	
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        OnTrackingLost();
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

	
    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
		TrackedRegistry.AddTrackable(mTrackableBehaviour.GetComponent<MarkerBehaviour>().Marker.MarkerID);
		
		GameObject.Find("GUI_holder").GetComponent<Boiler_Gui_stuff>().OnTrackableChange();
		GameObject.Find("GUI_holder").GetComponent<Gui_CMMS>().OnTrackableChange();
		
		m_machine.position = transform.FindChild("pos").position;
    }
	
	
    private void OnTrackingLost()
    {
		TrackedRegistry.RemoveTrackable(mTrackableBehaviour.GetComponent<MarkerBehaviour>().Marker.MarkerID);
		
		GameObject.Find("GUI_holder").GetComponent<Boiler_Gui_stuff>().OnTrackableChange();
		GameObject.Find("GUI_holder").GetComponent<Gui_CMMS>().OnTrackableChange();
		
		Camera.mainCamera.GetComponent<ResetCameraHandle>().Reset();
    }

    #endregion // PRIVATE_METHODS
}
