using UnityEngine;
using System.Collections;
using System;


public class Boiler_ValveHandle : MonoBehaviour {

	private int m_timesPlayed;
	private Action m_doneCallback;
	
	private enum Status { Opening, Closing };
	
	private Status m_status;
	
	private Transform m_tail;
	
	
void Start() 
{
	m_doneCallback = null;	
		
	m_timesPlayed = 0;
		
	m_tail = transform.parent.FindChild("group_21");	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Opened_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.RemoveBurner, new Action(Closed_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.RegulateBurner, new Action(Opened_State));
}
	
	
private void Opened_State()
{
	animation.Stop();
		
	Utils.SetVisible(m_tail, true);
}
	

private void Closed_State()
{
	animation.Stop();
		
	Utils.SetVisible(m_tail, false);	
}


public void Close(Action doneCallback)
{
	m_doneCallback = doneCallback;
		
	m_status = Status.Closing;	
		
	Animation an = GetComponent<Animation>();
		
	m_timesPlayed = 0;
	
	an.PlayQueued("CloseValve", QueueMode.PlayNow);
	an.PlayQueued("CloseValve", QueueMode.CompleteOthers);
}
	
	
public void Open(Action doneCallback)
{
	m_doneCallback = doneCallback;
	
	m_status = Status.Opening;	
		
	Utils.SetVisible(m_tail, true);	
		
	Animation an = GetComponent<Animation>();
		
	m_timesPlayed = 0;
	
	an.PlayQueued("OpenValve", QueueMode.PlayNow);
	an.PlayQueued("OpenValve", QueueMode.CompleteOthers);
}
		
	
void OnAnimationFinished()
{
	m_timesPlayed++;
		
	if (m_timesPlayed % 2 == 0)
	{	
		if (m_status == Status.Closing)
		{
			Utils.SetVisible(m_tail, false);		
		}
		
		if (m_doneCallback != null)
		{
			m_doneCallback.Invoke();	
		}
	}
}
	
	
}//class
