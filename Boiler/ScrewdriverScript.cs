using UnityEngine;
using System.Collections;
using System;


public class ScrewdriverScript : MonoBehaviour {
	
	private Action m_doneCallback;
	
	private Transform m_screw;
	
	private Vector3 m_startPosition;
	
	private Vector3 m_endPosition;
	
	private Vector3 m_screw_startPosition;
	
	private Vector3 m_screw_endPosition;
	
	private Vector3 m_away_pos;
	
	private bool m_screwing;
	
	private bool m_approaching;
	
	private long m_steps;
	
	
void Start()
{
	m_approaching = false;	
		
	m_screwing = false;	
		
	m_screw = transform.parent.FindChild("RegScrew");
		
	m_startPosition = transform.localPosition;
		
	float distance = m_screw.FindChild("NurbsCircle").renderer.bounds.size.x * 0.5f;
		
	m_endPosition = new Vector3(m_startPosition.x - distance, m_startPosition.y, m_startPosition.z);	
		
	m_away_pos = new Vector3(m_startPosition.x + (distance * 5), m_startPosition.y, m_startPosition.z);	
		
	m_screw_startPosition = m_screw.localPosition;
		
	m_screw_endPosition = new Vector3(m_screw_startPosition.x - distance, m_screw_startPosition.y, m_screw_startPosition.z);
		
	Utils.SetVisible(transform, false);	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Unregulated_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.CheckGases, new Action(Regulated_State));	
}

	
private void Unregulated_State()
{
	Utils.SetVisible(transform, false);
		
	m_approaching = false;
		
	m_screwing = false;
		
	m_screw.localPosition = m_screw_startPosition;	
}
	

private void Regulated_State()
{
	Utils.SetVisible(transform, false);
		
	m_approaching = false;
	
	m_screwing = false;
		
	m_screw.localPosition = m_screw_endPosition;
}
	
	
public void Screw(Action doneCallback)
{
	m_doneCallback = doneCallback;	
		
	Utils.SetVisible(transform, true);
		
	transform.localPosition = m_away_pos;	
		
	m_approaching = true;	
		
	m_steps = 0;	
}
	
	
void Update()
{
	if (m_approaching)
	{
		m_steps++;
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_startPosition, Time.deltaTime * m_steps * 0.1f);	
			
		m_approaching = (transform.localPosition != m_startPosition);
			
		if (!m_approaching)
		{
			m_screwing = true;	
				
			m_steps = 0;	
		}
	}
		
	if (m_screwing)
	{
		m_steps++;
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_endPosition, Time.deltaTime * m_steps * 0.1f);
			
		transform.RotateAroundLocal(Vector3.right, Time.deltaTime);	
		
		m_screw.RotateAroundLocal(Vector3.right, Time.deltaTime);
			
		m_screw.localPosition = Vector3.Lerp(m_screw.localPosition, m_screw_endPosition, Time.deltaTime * m_steps * 0.1f);	
			
		m_screwing = (transform.localPosition != m_endPosition);
			
		if (!m_screwing)
		{
			Utils.SetVisible(transform, false);	
				
			if (m_doneCallback != null)
			{
				m_doneCallback.Invoke();	
			}
		}
	}
}
	
	
	
}//class
