using UnityEngine;
using System.Collections;
using System;


public class ButtonScript : MonoBehaviour {

	private Transform m_movable;
	
	private Action m_doneCallback;
	
	private bool m_goingDown;
	
	private bool m_playing;
	
	private Vector3 m_inPOS;
	
	private Vector3 m_outPOS;
	
	private long m_steps;
	
	
void Start()
{
	m_playing = false;
		
	m_movable = transform.FindChild("Model").FindChild("mesh2");
		
	float half = m_movable.renderer.bounds.size.x * 1.4f;
		
	m_outPOS = m_movable.localPosition;	
		
	m_inPOS = new Vector3(m_movable.localPosition.x + half, m_movable.localPosition.y, m_movable.localPosition.z);	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Init_State));	
}

	
private void Init_State()
{
	m_playing = false;
		
	m_movable.localPosition = m_outPOS;	
}
	
	
public void Push(Action doneCallback)
{
	m_doneCallback = doneCallback;
		
	m_steps = 0;
		
	m_goingDown = true;	
		
	m_playing = true;	
}
	
	
void Update()
{
	if (m_playing)
	{
		if (m_goingDown)
		{
			m_steps++;	
				
			m_movable.localPosition = Vector3.Lerp(m_movable.localPosition, m_inPOS, Time.deltaTime * m_steps * 0.1f);
				
			m_goingDown = (m_movable.localPosition != m_inPOS);	
		}
		else
		{
			m_steps++;	
				
			m_movable.localPosition = Vector3.Lerp(m_movable.localPosition, m_outPOS, Time.deltaTime * m_steps * 0.1f);
				
			m_playing = (m_movable.localPosition != m_outPOS);
				
			if (!m_playing)
			{
				if (m_doneCallback != null)
				{
					m_doneCallback.Invoke();	
				}
			}
		}
	}
}
	
	
}//class
