using UnityEngine;
using System.Collections;
using System;


public class VacuumScript : MonoBehaviour {
	
	private Action m_doneCallback;
	
	private int m_currSoot;
	
	private Transform m_sp;
	
	private bool m_cleaning;
	
	private Vector3 m_startPosition;
	
	private long m_steps;
	
	
void Start()
{	
	m_sp = transform.parent.FindChild("SootParent");
		
	m_currSoot = 0;
		
	Utils.SetVisible(transform, false);
		
	m_startPosition = transform.localPosition;	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Dirty_State));
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.CloseDoor, new Action(Clean_State));
}
	

private void Dirty_State()
{
	Utils.SetVisible(transform, false);
		
	transform.localPosition = m_startPosition;
		
	m_cleaning = false;
		
	for (int i = 0; i < m_sp.GetChildCount(); i++)
	{
		Utils.SetVisible(m_sp.GetChild(i), true);	
	}
}
	

private void Clean_State()
{
	Utils.SetVisible(transform, false);
		
	transform.localPosition = m_startPosition;
		
	m_cleaning = false;
		
	for (int i = 0; i < m_sp.GetChildCount(); i++)
	{
		Utils.SetVisible(m_sp.GetChild(i), false);	
	}	
}
	
	
public void Clean(Action doneCallback)
{
	if (m_cleaning)
	{
		return;
	}
		
	m_doneCallback = doneCallback;
		
	Utils.SetVisible(transform, true);
		
	transform.localPosition = m_startPosition;	
		
	m_currSoot = 0;	
		
	m_cleaning = true;
		
	m_steps = 0;	
}
	
	
void Update()
{
	if (m_cleaning)
	{
		m_steps++;	
			
		Transform child = m_sp.GetChild(m_currSoot);
			
		Vector3 target = new Vector3(child.position.x + 0.4f, child.position.y + 1.15f, child.position.z - 0.5f);
			
		transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * m_steps * 0.1f);
			
		if (transform.position == target)
		{
			Utils.SetVisible(child, false);
				
			m_currSoot++;	
		}
			
		m_cleaning = (m_currSoot < m_sp.GetChildCount());
			
		if (!m_cleaning)
		{
			Utils.SetVisible(transform, false);	
				
			if (m_doneCallback != null)
			{
				m_doneCallback.Invoke();	
			}
		}
	}
}
	
	
}//class
