using UnityEngine;
using System.Collections;
using System;


public class FlueGasAnalyzerScript : MonoBehaviour {

	private Action m_doneCallback;
	
	private bool m_connecting;
	
	private Vector3 m_awayPos;
	
	private Vector3 m_startPosition;
	
	private long m_steps;
	
	
void Start()
{
	m_connecting = false;
		
	m_startPosition = transform.localPosition;	
		
	m_awayPos = new Vector3(m_startPosition.x, m_startPosition.y + 1, m_startPosition.z);	
		
	Utils.SetVisible(transform, false);	
		
	Boiler_StateRegistry.Register(transform, Boiler_StateRegistry.Step.TurnOffBoiler, new Action(Disconnected_State));	
}
	
	
private void Disconnected_State()
{
	transform.localPosition = m_startPosition;
		
	Utils.SetVisible(transform, false);
		
	m_connecting = false;
}

	
public void Connect(Action doneCallback)
{
	m_doneCallback = doneCallback;	
		
	transform.localPosition = m_awayPos;
		
	Utils.SetVisible(transform, true);	
		
	m_steps = 0;	
		
	m_connecting = true;	
}
	
	
void Update()
{
	if (m_connecting)
	{
		++m_steps;	
			
		transform.localPosition = Vector3.Lerp(transform.localPosition, m_startPosition, Time.deltaTime * m_steps * 0.1f);
			
		m_connecting = (transform.localPosition != m_startPosition);
			
		if (!m_connecting)
		{
			if (m_doneCallback != null)
			{
				m_doneCallback.Invoke();	
			}
		}
	}
}
	
	
}//class
