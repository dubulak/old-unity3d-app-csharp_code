using UnityEngine;
using System.Collections;


public class Menu_GUI_stuff : MonoBehaviour {

	public GUISkin m_guiSkin;
	
	public Texture2D m_quit_Img;
	public Texture2D m_broom_Img;
	public Texture2D m_tools_Img;
	public Texture2D m_machine_Img;
	public Texture2D m_white_Img;
	public Texture2D m_background_Img;
	
	public Font m_font8;
	public Font m_font10;
	public Font m_font12;
	public Font m_font14;
	public Font m_font16;
	public Font m_font18;
	public Font m_font20;
	public Font m_font22;
	public Font m_fontHD;
	
	private const float GUI_MARGIN_PCNT = 0.012f;
	
	private bool m_loading;
	
	
void Start()
{
	m_loading = false;
}

	
void OnGUI()
{
	GUI.skin = m_guiSkin;	
		
	int s = (int)(Screen.width * 0.015625);	

	Font x = m_font20;
		
	if (s <= 9)
		x = m_font8;	
	else if ((s > 9) && (s <= 11))
		x = m_font10;
	else if ((s > 11) && (s <= 13))
		x = m_font12;
	else if ((s > 13) && (s <= 15))
		x = m_font14;
	else if ((s > 15) && (s <= 17))
		x = m_font16;
	else if ((s > 17) && (s <= 19))
		x = m_font16;
	else if ((s > 19) && (s <= 21))
		x = m_font18;
	else if ((s > 21) && (s <= 23))
		x = m_font20;
	else if ((s > 23) && (s <= 25))
		x = m_font22;
	else if (s > 25)
		x = m_fontHD;
		
	GUI.skin.button.font = 	x;
	GUI.skin.label.font = x;
	GUI.skin.font = x;
	GUI.skin.box.font = x;
		
		
	GUIContent title_gc = new GUIContent(" AR MainCODE ");
	Vector2 title_size = m_guiSkin.GetStyle("Box").CalcSize(title_gc);
		
	Rect title_rect = new Rect(
			(Screen.width * 0.5f) - ((title_size.x + 10) * 0.5f),
			Screen.height * GUI_MARGIN_PCNT * 2,
			title_size.x + 10,
			title_size.y + 10
			);
	
	GUIContent info_gc = new GUIContent("  Machine info", m_machine_Img);
	Vector2 info_size = m_guiSkin.GetStyle("Button").CalcSize(info_gc);
		
	GUIContent piping_gc = new GUIContent("  Pipe replacement", m_tools_Img);	
	Vector2 piping_size = m_guiSkin.GetStyle("Button").CalcSize(piping_gc);
		
	GUIContent boiler_gc = new GUIContent("  Boiler cleaning", m_broom_Img);
	Vector2 boiler_size = m_guiSkin.GetStyle("Button").CalcSize(boiler_gc);
		
	float maxWidth = Mathf.Max(new float[] { info_size.x + 10, piping_size.x + 10, boiler_size.x + 10 });	
		
	
	GUIContent choose_gc = new GUIContent(" Select scenario ");
	GUIStyle stl = new GUIStyle(m_guiSkin.GetStyle("Label"));
	stl.normal.textColor = Color.white;
		
	Vector2 choose_size = stl.CalcSize(choose_gc);	
		
	Rect choose_rect = new Rect(
			(Screen.width * 0.5f) - ((choose_size.x + 10) * 0.5f),
			title_rect.yMax + (Screen.height * GUI_MARGIN_PCNT * 5),
			choose_size.x + 10,
			choose_size.y + 10
			);
	
	Rect line_rect = new Rect(
			(Screen.width * 0.5f) - ((choose_size.x + 10) * 0.5f),
			choose_rect.yMax - 10,
			choose_size.x + 10,
			4
			);
		
			
	Rect info_rect = new Rect(
			(Screen.width * 0.5f) - (maxWidth * 0.5f),
			line_rect.yMax + (Screen.height * GUI_MARGIN_PCNT * 1.5f),
			maxWidth,
			(info_size.y + 10) * 2
			);
		
	Rect piping_rect = new Rect(
			(Screen.width * 0.5f) - (maxWidth * 0.5f),
			info_rect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f),
			maxWidth,
			(piping_size.y + 10) * 2
			);
		
	Rect boiler_rect = new Rect(
			(Screen.width * 0.5f) - (maxWidth * 0.5f),
			piping_rect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f),
			maxWidth,
			(boiler_size.y + 10) * 2
			);
		
		
	GUIContent load_gc = new GUIContent(" LOADING ... ");
	Vector2 load_size = stl.CalcSize(load_gc);
	
	Rect load_rect = new Rect(
			(Screen.width * 0.5f) - ((load_size.x + 10) * 0.5f),
			boiler_rect.yMax + (Screen.height * GUI_MARGIN_PCNT * 4),
			load_size.x + 10,
			load_size.y + 10
			);
	
	
	GUIContent quit_gc = new GUIContent("  Exit", m_quit_Img);
	Vector2 quit_size = m_guiSkin.GetStyle("Button").CalcSize(quit_gc);
		
	Rect quit_rect = new Rect(
			(Screen.width * 0.5f) - (((quit_size.x + 10) * 1.5f) * 0.5f),
			(Screen.height * (1 - (GUI_MARGIN_PCNT * 2))) - ((quit_size.y + 10) * 2),
			(quit_size.x + 10) * 1.5f,
			(quit_size.y + 10) * 2
			);
	
	/////////////////////
		
	GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), m_background_Img);	
	
	GUI.Box(title_rect, title_gc);	
		
	GUI.enabled = !m_loading;	
		
	GUI.Label(choose_rect, choose_gc, stl);
		
	GUI.DrawTexture(line_rect, m_white_Img);	
		
	if (GUI.Button(info_rect, info_gc))
	{
		m_loading = true;
			
		Application.LoadLevel("infoScene");
	}
		
	if (GUI.Button(piping_rect, piping_gc))
	{
		m_loading = true;
		
		Application.LoadLevel("Pipe_mainScene");	
	}
		
	if (GUI.Button(boiler_rect, boiler_gc))
	{
		m_loading = true;
		
		Application.LoadLevel("boilerScene 1");	
	}
			
	if (GUI.Button(quit_rect, quit_gc))
	{
		AppData.AppExit();	
	}
		
	GUI.enabled = true;	
		
	if (m_loading)
	{
		GUI.Label(load_rect, load_gc, stl);	
	}	
}
	
	
	
}//class
