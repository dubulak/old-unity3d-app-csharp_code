using UnityEngine;
using System.Collections;
using System;


public class Screw : MonoBehaviour {

	private Vector3 m_endPosition;
	
	private bool m_goingIn;
	
	public Transform[] children;
	
	public int rotateSpeed = 100;
	
	private Action m_screwCallback;
	private Action m_unscrewCallback;
	
	private Vector3 m_startPosition;
	
	private Vector3 m_initPosition;
	
	private Vector3 m_approachDestination;
	private Action<Transform, Transform> m_approachAction;
	private Transform m_arg1;
	
	
void Start() 
{
	IsPlaying = false;
		
	m_initPosition = m_startPosition = transform.localPosition;
		
	CalculateEndPosition();		
		
	Distance = Vector3.Distance(transform.TransformPoint(m_endPosition), transform.TransformPoint(m_startPosition));	
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
}
		

public void RestoreStartPosition()
{
	m_startPosition = m_initPosition;	
}
	
	
public float Distance
{
	get;
	private set;
}
		
	
public void Stop()
{
	Approaching = false;	
	IsPlaying = false;	
}
	
	
private bool Approaching
{
	get;
	set;
}
	

void Step_Init_State()
{
	Stop();
	transform.localPosition = m_initPosition;		
}


public void SetToEndPosition()
{
	transform.localPosition = m_endPosition;	
}
	
	
public void RegisterStartPosition(Pipe_StateRegistry.Step step)
{
	Pipe_StateRegistry.Register(transform, step, new Action(Step_Init_State));	
}
	
	
public void RegisterOutOfSight(Pipe_StateRegistry.Step step)
{
	Pipe_StateRegistry.Register(transform, step, new Action(OutOfSight));		
}

	
void OutOfSight()
{
	Stop();	
	transform.position = GameObject.Find("/Scene/Tools/pos_outOfSight").transform.position;		
}
	
	
public void ScrewMe(Action action)
{
	m_screwCallback = action;
		
	m_goingIn = true;
	
	PlayAnimation();		
}
	
	
public void UnscrewMe(Action action)
{		
	m_unscrewCallback = action;	
		
	m_goingIn = false;
	
	PlayAnimation();
}
	
	
public void CalculateScrewDirection()
{
	Vector3 tmp = transform.position;
		
	float y = 0;
		
	foreach (Transform t in children)
		y += t.renderer.bounds.size.y;
		
	transform.Translate(Vector3.down * y, Space.Self);
		
	m_startPosition = transform.localPosition;	
		
	transform.position = tmp;				
}
	
	
public void CalculateEndPosition()
{
	Vector3 tmp = transform.position;
		
	float y = 0;
		
	foreach (Transform t in children)
		y += t.renderer.bounds.size.y;	
		
	transform.Translate(((m_goingIn) ? Vector3.down : Vector3.up) * y, Space.Self);
		
	m_endPosition = transform.localPosition;	
		
	transform.position = tmp;		
}
	
	
void PlayAnimation()
{
	IsPlaying = true;		
}
	
	
public bool IsPlaying
{
	get;
	private set;
}
	
	
void DoNothing() {}
	

void Update() 
{
	if (IsPlaying)
	{
		transform.Rotate((m_goingIn) ? Vector3.up: Vector3.down, rotateSpeed * Time.deltaTime);	

		transform.Translate(((m_goingIn) ? Vector3.down : Vector3.up) * 0.1f * Time.deltaTime, Space.Self);	
			
		IsPlaying = (m_goingIn) ? (transform.localPosition.y > m_startPosition.y) : (transform.localPosition.y < m_endPosition.y);
			
		if (!IsPlaying)
		{
			if (m_goingIn)
				m_screwCallback.Invoke();
			else
				m_unscrewCallback.Invoke();
		}
	}
		
	if (Approaching)
	{
		transform.Translate(Vector3.down * Time.deltaTime * 0.5f, Space.Self);
			
		Approaching = (transform.position.y > m_approachDestination.y);	
			
		if (!Approaching)
		{
			m_approachAction.Invoke(transform, m_arg1);	
		}
	}	
}


public void Approach(Action<Transform, Transform> action, Transform arg1, float dist)
{
	m_approachAction = action;
		
	m_arg1 = arg1;	
	
	Vector3 tmp = transform.position;	
		
	transform.Translate(Vector3.up * dist, Space.Self);	
	
	m_approachDestination = tmp;
			
	Approaching = true;	
}
	
	
}//class