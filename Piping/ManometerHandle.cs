using UnityEngine;
using System.Collections;
using System;


public class ManometerHandle : MonoBehaviour {

	private Vector3 m_startPosition;
	private Transform m_startParent;
	
	private Vector3 m_endPosition;
	
	public Transform posMeter;
	public Transform valveUp;
	
	private Action m_goDown_Action;
	private Action m_goUp_Action;
	
	
void Start() 
{
	m_startPosition = transform.position;
	m_startParent = transform.parent;
		
	Stop();	
		
	CalculateEndPosition();	
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.CheckTemperature, new Action(Step_CheckTemp_State));
}

	
void Step_Init_State()
{
	Stop();	
	transform.parent = m_startParent;
	transform.position = m_startPosition;		
}
	
	
void Step_CheckTemp_State()
{
	Stop();	
	transform.position = posMeter.position;		
	transform.parent = valveUp;
}
	
	
private bool GoingDown
{
	get;
	set;
}
	
	
private bool GoingUp
{
	get;
	set;
}

	
private void Stop()
{
	GoingDown = false;
	GoingUp = false;
}
	

public void GoDown(Action action)
{
	transform.parent = valveUp;	
	
	transform.localPosition = m_endPosition;
		
	m_goDown_Action = action;
		
	GoingDown = true;	
}
	

public void GoUp(Action action)
{
	m_goUp_Action = action;
		
	GoingUp = true;	
}
	
	
void Update()
{
	if (GoingDown)
	{
		transform.Translate(Vector3.down * 0.2f * Time.deltaTime, Space.Self);
			
		GoingDown = (transform.localPosition.y > posMeter.localPosition.y);
			
		if (!GoingDown)
		{
			m_goDown_Action.Invoke();	
		}
	}
		
	if (GoingUp)
	{
		transform.Translate(Vector3.up * 0.4f * Time.deltaTime, Space.Self);	
			
		GoingUp = (transform.localPosition.y < m_endPosition.y);
			
		if (!GoingUp)
		{
			m_goUp_Action.Invoke();	
		}
	}
}
	
	
void CalculateEndPosition()
{
	Vector3 tmp = transform.position;
	
	transform.position = posMeter.position;	
	transform.parent = valveUp;	
		
	float y = transform.FindChild("Manometer").FindChild("group_0").renderer.bounds.size.y;
		
	transform.Translate(Vector3.up * y, Space.Self);
		
	m_endPosition = transform.localPosition;
	
	transform.parent = m_startParent;	
	transform.position = tmp;		
}
	
	
}//class
