using UnityEngine;
using System.Collections;
using System;


public class GearHandle : MonoBehaviour {

	private Vector3 m_startPosition;
	private Transform m_startParent;
	private Vector3 m_startAngles;
	
	
void Start()
{
	Stop();	
		
	m_startPosition = transform.position;
	m_startParent = transform.parent;
	m_startAngles = transform.eulerAngles;
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.WearGear, new Action(Step_Init_State));	
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.CloseUpstream, new Action(Step_Init_State));
}


void Step_Init_State()
{
	Stop();
		
	transform.parent = m_startParent;
	transform.position = m_startPosition;		
	transform.eulerAngles = m_startAngles;	
}
	

public bool IsPlaying
{
	get;
	private set;
}
	
	
public void Stop()
{
	IsPlaying = false;	
}
	
	
public void ShowAsBackGround(float width, float height)
{
	transform.parent = null;	
		
	Camera cam = Camera.main;
		
	transform.position = cam.ScreenToWorldPoint(new Vector3(width, height, 12.6f));		
}
	
	
public void ShowOnMiddleOfScreen(float width)
{
	transform.parent = null;	
		
	Camera cam = Camera.main;
		
	transform.position = cam.ScreenToWorldPoint(new Vector3(width, Screen.height * 0.4f, 12));
		
	IsPlaying = true;	
}
	
	
void Update() 
{
	if (IsPlaying)
	{
		transform.Rotate(Vector3.up, 35 * Time.deltaTime);	
	}
}
	
	
}//class
