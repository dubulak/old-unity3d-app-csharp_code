/*==============================================================================
            Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary
==============================================================================*/

using UnityEngine;
using System;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class Pipe_TrackableEventHandler : MonoBehaviour,
                                            ITrackableEventHandler
{
	public Transform tools;
	public Transform piping;
	
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
    #endregion // PRIVATE_MEMBER_VARIABLES



    #region UNTIY_MONOBEHAVIOUR_METHODS
    
    void Start()
    {	
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        OnTrackingLost();
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

	
    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
		TrackedRegistry.AddTrackable(mTrackableBehaviour.GetComponent<MarkerBehaviour>().Marker.MarkerID);
		
		GameObject.Find("GUI_holder").GetComponent<Pipe_Gui_stuff>().OnTrackableChange();
		GameObject.Find("GUI_holder").GetComponent<Gui_CMMS>().OnTrackableChange();
		
		piping.position = transform.FindChild("pos_piping").position;
		piping.eulerAngles = new Vector3(0, 0, 0);
		piping.parent = transform;
    }
	
	
    private void OnTrackingLost()
    {
		TrackedRegistry.RemoveTrackable(mTrackableBehaviour.GetComponent<MarkerBehaviour>().Marker.MarkerID);
		
		GameObject.Find("GUI_holder").GetComponent<Pipe_Gui_stuff>().OnTrackableChange();
		GameObject.Find("GUI_holder").GetComponent<Gui_CMMS>().OnTrackableChange();
		
		piping.parent = null;
		piping.eulerAngles = new Vector3(0, 0, 0);
		piping.position = tools.FindChild("pos_outOfSight").position;
    }

    #endregion // PRIVATE_METHODS
}
