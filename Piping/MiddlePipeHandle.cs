using UnityEngine;
using System.Collections;
using System;


public class MiddlePipeHandle : MonoBehaviour {
	
	private Vector3 m_startPosition;
	private Vector3 m_fall_endPosition;
	private Vector3 m_goAway_endPosition;
	private Vector3 m_incoming_startPosition;
	
	private Vector3 m_startAngles;

	private Action m_fall_callback;
	private Action m_replace_clb;
	private Action m_lift_clb;
	
	
void Start() 
{
	m_startPosition = transform.localPosition;
	m_startAngles = transform.eulerAngles;	
		
	CalculatePositions();	
		
	Stop();
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.DisconnectRelief, new Action(Step_DisconnectRelief_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.DisconnectRight, new Action(Step_DisconnectRight_State));	
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.GetNewPipe, new Action(Step_GetNewPipe_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ConnectRight, new Action(Step_ConnectRight_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ConnectLeft, new Action(Step_ConnectLeft_State));
}
	
	
public void Stop()
{
	IsFalling = false;	
	IsGoingAway = false;	
	IsComingIn = false;	
	IsGoingUp = false;	
}
	
	
void Step_Init_State()
{
	Stop();
		
	transform.localPosition = m_startPosition;		
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}	
		
	transform.FindChild("Particle System").particleSystem.Play();
		
	transform.FindChild("Crack").renderer.enabled = true;	
		
	transform.eulerAngles = m_startAngles;	
}
	
	
void Step_DisconnectRight_State()
{
	transform.localPosition = m_startPosition;
		
	transform.FindChild("Particle System").particleSystem.Stop();		
		
	transform.FindChild("Crack").renderer.enabled = true;	
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}		
		
	transform.eulerAngles = m_startAngles;	
}
	
	
void Step_DisconnectRelief_State()
{
	Stop();
		
	transform.localPosition = m_startPosition;
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}	
		
	transform.FindChild("Particle System").particleSystem.Stop();		
		
	transform.FindChild("Crack").renderer.enabled = true;	
		
	transform.eulerAngles = m_startAngles;	
}
	
	
void Step_ConnectRight_State()
{
	Stop();
		
	transform.localPosition = m_fall_endPosition;
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}	
		
	transform.FindChild("Particle System").particleSystem.Stop();	
		
	transform.FindChild("Crack").renderer.enabled = false;		
		
	transform.eulerAngles = m_startAngles;	
}
	
	
void Step_ConnectLeft_State()
{
	Stop();
		
	transform.localPosition = m_startPosition;
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}		
		
	transform.FindChild("Particle System").particleSystem.Stop();		
		
	transform.FindChild("Crack").renderer.enabled = false;	
		
	transform.eulerAngles = m_startAngles;	
}
	
	
void Step_GetNewPipe_State()
{
	Stop();
		
	transform.localPosition = m_incoming_startPosition;
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = false;		
	}
		
	transform.eulerAngles = m_startAngles;	
		
	transform.FindChild("Particle System").particleSystem.Stop();		
}
		
		
private bool IsFalling
{
	get;
	set;
}
	
	
private bool IsGoingAway
{
	get;
	set;
}

	
private bool IsComingIn
{
	get;
	set;
}
	
	
private bool IsGoingUp
{
	get;
	set;
}

	
private void CalculatePositions()
{
	Vector3 tmp = transform.position;
		
	float y = transform.renderer.bounds.size.y;
		
	transform.Translate(Vector3.down * y, Space.World);
		
	m_fall_endPosition = transform.localPosition;
		
	Vector3 tempDown = transform.position;	
		
	float z = transform.renderer.bounds.size.z;
		
	transform.Translate(Vector3.back * 9 * z, Space.World);	
		
	m_goAway_endPosition = transform.localPosition;	
		
	transform.position = tmp;	
			
	transform.Translate(Vector3.up * 4 * y, Space.World);
		
	m_incoming_startPosition = transform.localPosition;
		
	transform.position = tmp;
}
	
	
public void Fall(Action action)
{
	m_fall_callback = action;
		
	IsFalling = true;		
}
	
	
public void Replace(Action action)
{
	m_replace_clb = action;	
		
	transform.localPosition = m_incoming_startPosition;
		
	transform.eulerAngles = m_startAngles;	
		
	Renderer[] components = transform.GetComponentsInChildren<Renderer>();	
		
	foreach (Renderer r in components)
	{
		r.enabled = true;		
	}	
				
	transform.FindChild("Crack").renderer.enabled = false;	
		
	IsComingIn = true;
}
	
	
public void Lift(Action action)
{
	m_lift_clb = action;
		
	IsGoingUp = true;		
}
	
	
void Update () 
{
	if (IsFalling)
	{
		transform.Translate(Vector3.down * 0.5f * Time.deltaTime, Space.World);	
			
		IsFalling = (transform.localPosition.y > m_fall_endPosition.y);
	
		if (!IsFalling)
		{
			IsGoingAway = true;			
		}
	}
		
	if (IsGoingAway)
	{
		transform.Translate(Vector3.back * 1.5f * Time.deltaTime, Space.World);
			
		transform.Rotate(Vector3.up, 150 * Time.deltaTime);	
			
		IsGoingAway = (transform.localPosition.z >	m_goAway_endPosition.z);
			
		if (!IsGoingAway)
		{
			m_fall_callback.Invoke();	
		}
	}
		
	if (IsComingIn)
	{
		transform.Translate(Vector3.down * 1.0f * Time.deltaTime, Space.World);
				
		IsComingIn = (transform.localPosition.y > m_fall_endPosition.y);	
		
		if (!IsComingIn)
		{
			m_replace_clb.Invoke();				
		}
	}
		
	if (IsGoingUp)
	{
		transform.Translate(Vector3.up * 0.5f * Time.deltaTime, Space.World);
			
		IsGoingUp = (transform.localPosition.y < m_startPosition.y);
			
		if (!IsGoingUp)
		{
			m_lift_clb.Invoke();		
		}
	}
}
	
	
}//class
