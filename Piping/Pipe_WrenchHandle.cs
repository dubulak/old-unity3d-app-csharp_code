using UnityEngine;
using System.Collections.Generic;
using System;


public class Pipe_WrenchHandle : MonoBehaviour {

	private Action m_action;	
	
	private int m_timesPlayed;
	
	private Vector3 m_startPosition;
	
	private Vector3 m_endPosition;
	
	private Vector3 m_setPosition;
	
	private Vector3 m_startAngles;
	
	
void Start() 
{
	m_timesPlayed = 0;
		
	m_startPosition = transform.position;	
		
	m_startAngles = transform.eulerAngles;	
		
	Stop();	
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
}
		
	
public void Step_Init_State()
{
	transform.position = m_startPosition;
	transform.eulerAngles = m_startAngles;	
		
	Stop();
}
	
	
public void Stop()
{
	m_timesPlayed = 0;	
	Approaching = false;
	Retreating = false;	
	GetComponent<Animation>().Stop();
}
	
	
void OnAnimationFinished()
{
	m_timesPlayed++;
		
	if (m_timesPlayed % 2 == 0)
		StartRetreating();
}
	
	
private bool Approaching
{
	get;
	set;
}
	
	
private bool Retreating
{
	get;
	set;
}
	
	
private void StartRetreating()
{
	Retreating = true;	
}
	
	
public void Unscrew(Action action, Vector3 endPosition)
{
	m_action = action;	
		
	m_endPosition = endPosition;	
		
	Approaching = true;	
		
	float z = transform.renderer.bounds.size.z;
		
	transform.position = endPosition;	
		
	transform.Translate(Vector3.back * 2 * z, Space.World);
		
	m_setPosition = transform.position;	
}
	

void Update()
{
	if (Approaching)
	{
		transform.Translate(Vector3.forward * 0.8f * Time.deltaTime, Space.Self);
			
		Approaching = (transform.position.z < m_endPosition.z);
			
		if (!Approaching)
		{
			Animation an = GetComponent<Animation>();
	
			an.PlayQueued(an.clip.name, QueueMode.PlayNow);
			an.PlayQueued(an.clip.name, QueueMode.CompleteOthers);	
		}
	}
		
	if (Retreating)
	{
		transform.Translate(Vector3.back * 0.8f * Time.deltaTime, Space.Self);
			
		Retreating = (transform.position.z > m_setPosition.z);	
			
		if (!Retreating)
		{
			m_action.Invoke();	
		}
	}
}
	

}//class
