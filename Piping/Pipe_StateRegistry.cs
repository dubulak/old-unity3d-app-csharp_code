using UnityEngine;
using System.Collections.Generic;
using System;


public static class Pipe_StateRegistry  {
	
	private static Dictionary<Transform, Dictionary<Step, Action>> m_functions;
	
	
	
	public enum Step {  ReadDocs, 
						WearGear, 
						CloseUpstream, 
						CloseDownstream, 
						OpenRelief, 
						DisconnectRelief,
						CheckPressure,
						CheckTemperature,
						DisconnectLeft,
						DisconnectRight,
						GetNewPipe,
						ConnectRight,
						ConnectLeft,
						ConnectRelief,
						OpenDownStream,
						OpenUpStream
	};
	
	
static Pipe_StateRegistry()
{
	m_functions = new Dictionary<Transform, Dictionary<Step, Action>>();
}
	
	
public static void Clear()
{
	m_functions.Clear();	
}
	
	
public static void Register(Transform t, Step step, Action action)
{
	if (!m_functions.ContainsKey(t))
		m_functions.Add(t, new Dictionary<Step, Action>());
		
	if (m_functions[t].ContainsKey(step))
		Debug.Log(t.ToString() + " already registered step " + step.ToString() + " callback");
		
	m_functions[t].Add(step, action);
}

	
public static void RestoreState(Step step)
{
	foreach (KeyValuePair<Transform, Dictionary<Step, Action>> kvp in m_functions)
	{
		for (Pipe_StateRegistry.Step currStep = step;  currStep >= 0;  --currStep)
		{
			if (kvp.Value.ContainsKey(currStep))
			{
				kvp.Value[currStep].Invoke();
				break;
			}
		}
	}
}
	
	
}//class
