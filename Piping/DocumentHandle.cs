using UnityEngine;
using System.Collections;
using System;


public class DocumentHandle : MonoBehaviour {

	private Vector3 m_startPosition;
	private Transform m_startParent;
	
	private Vector3 m_startAngles_LC;
	private Vector3 m_direction;
	
	
void Start() 
{
	Stop();	
			
	m_startPosition = transform.position;
	m_startParent = transform.parent;
		
	m_startAngles_LC = transform.FindChild("LeftCover").eulerAngles;
	m_direction = Vector3.forward;	
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));	
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.WearGear, new Action(Step_Init_State));
}
	
	
void Step_Init_State()
{
	Stop();
		
	transform.parent = m_startParent;
	transform.position = m_startPosition;	
		
	transform.FindChild("LeftCover").eulerAngles = m_startAngles_LC;
	m_direction = Vector3.forward;
}
	

public bool IsPlaying
{
	get;
	private set;
}
	
	
public void Stop()
{
	IsPlaying = false;	
}	
	
	
void Update() 
{
	if (IsPlaying)
	{
		Transform lc = transform.FindChild("LeftCover");
		
		if ((lc.eulerAngles.z > ((m_startAngles_LC.z + 180) % 360)) &&
			(lc.eulerAngles.z < ((m_startAngles_LC.z + 182) % 360)))
		{
			m_direction = Vector3.back;	
		}
		
		if ((lc.eulerAngles.z > ((m_startAngles_LC.z - 2) % 360)) &&
			(lc.eulerAngles.z < ((m_startAngles_LC.z) % 360)))
		{
			m_direction = Vector3.forward;	
		}
			
		lc.Rotate(m_direction, 20 * Time.deltaTime);	
	}
}
	
	
public void ShowOnMiddleOfScreen(float width)
{
	transform.parent = null;	
		
	Camera cam = Camera.main;
		
	transform.position = cam.ScreenToWorldPoint(new Vector3(width, Screen.height * 0.35f, 12));
		
	IsPlaying = true;	
}
	
	
}//class
