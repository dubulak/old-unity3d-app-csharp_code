using UnityEngine;
using System.Collections;
using System;


public class PipingHandle : MonoBehaviour {

	public Transform tools;
	
	
public void OutOfSight()
{
	transform.parent = null;
	transform.eulerAngles = new Vector3(0, 0, 0);
	transform.position = tools.FindChild("pos_outOfSight").position;
}
	
	
}//class
