using UnityEngine;
using System.Collections.Generic;
using System;


public class Pipe_Gui_stuff : MonoBehaviour {
	
	public Transform piping;
	
	public Font font8;
	public Font font10;
	public Font font12;
	public Font font14;
	public Font font16;
	public Font font18;
	public Font font20;
	public Font font22;
	public Font fontHD;
	
	public Texture2D m_ok_Img;
	public Texture2D m_restart_Img;
	public Texture2D m_info_Img;
	
	public Texture2D m_playBlue_Img;
	public Texture2D m_avatar_Img;
	public Texture2D arrowDown;
	public Texture2D arrowUp;
	public Texture2D arrowRight;
	public Texture2D quit;
	public Texture2D arrowBlue;
	public Texture2D arrowBlueLeft;
	public Texture2D m_redButton_Img;
	
	public Transform markerUp;
	public Transform markerDown;
	public Transform markerRelief;
	
	public Transform valveUp;
	public Transform valveDown;
	public Transform valveRelief;
	
	public Transform middlePipe;
	public Transform tools;
	
	public Transform arrow;
	
	public GUISkin guiSkin;
	
	private bool m_displayingSteps;
	private bool m_displayingInfo;
	private bool m_displayMsgBox;
	private bool m_displayPlayBtn;
	
	private Dictionary<Pipe_StateRegistry.Step, String> m_descriptions;
	private Dictionary<Pipe_StateRegistry.Step, Action> m_actions;
	private Dictionary<Pipe_StateRegistry.Step, Transform> m_markers;
	private Dictionary<Pipe_StateRegistry.Step, String> m_titles;
	
	private Pipe_StateRegistry.Step m_currIndex;
		
	private String m_msg;
	
	private const int RELIEF_TIME = 2;
	private float m_reliefTimer;
	private bool m_relieving;
	
	private int m_whichScrew;
	private bool m_canScrew;
	
	private bool m_disconnectLeft;
	private bool m_disconnectRight;
	
	private bool m_connectRight;
	private bool m_connectLeft;
	
	private const float GUI_MARGIN_PCNT = 0.012f;
	
	private Gui_CMMS m_Gui_CMMS;
	
		
void Start() 
{	
	m_connectLeft = false;
	m_connectRight = false;
		
	m_disconnectLeft = false;
	m_disconnectRight = false;
		
	m_reliefTimer = RELIEF_TIME;	
	m_relieving = false;
		
	m_displayingSteps = false;
	m_displayingInfo = true;
	m_displayMsgBox = false;
	m_displayPlayBtn = true;
	
	AppData.GuiStuff_DisplayEndMsg = false;
		
		
	m_descriptions = new Dictionary<Pipe_StateRegistry.Step, string>();
		
	m_descriptions.Add(Pipe_StateRegistry.Step.ReadDocs, "Check documents\n\nRead the steam pipe maintenance manual to familiarize yourself with the pipe replacement procedure.\n\nCheck the piping schematics to figure out each component's position.");
	m_descriptions.Add(Pipe_StateRegistry.Step.WearGear, "Wear protective gear\n\nPut on all items of protective gear (overalls, boots, goggles, gloves) to protect yourself from injury.");
	m_descriptions.Add(Pipe_StateRegistry.Step.CloseUpstream, "Close upstream valve\n\nLocate the upstream valve and close it by rotating the handle to the right, twice.\n\nThis action will stop the upstream flow of steam.");
	m_descriptions.Add(Pipe_StateRegistry.Step.CloseDownstream, "Close downstream valve\n\nLocate the downstream valve and close it by rotating the handle to the right, twice.\n\nThis action will prevent steam from coming in from downstream.");
	m_descriptions.Add(Pipe_StateRegistry.Step.OpenRelief, "Open relief valve\n\nLocate the relief valve and open it.\n\nThis action will evacuate the pipe from residual steam.");
	m_descriptions.Add(Pipe_StateRegistry.Step.DisconnectRelief, "Disconnect relief valve\n\nUse the wrench to disconnect the relief valve from the pipe.");
	m_descriptions.Add(Pipe_StateRegistry.Step.CheckPressure, "Check pipe pressure\n\nUse the wrench to install the manometer on top of the upstream valve to measure steam pressure inside the pipe.\n\nIt should be close to ambient pressure, before it is safe to disconnect the pipe.");
	m_descriptions.Add(Pipe_StateRegistry.Step.CheckTemperature, "Check pipe temperature\n\nUse the wrench to install the thermometer on top of the downstream valve to measure steam temperature.\n\nIt should be close to to ambient temperature, before it is safe to disconnect the pipe.");	
	m_descriptions.Add(Pipe_StateRegistry.Step.DisconnectLeft, "Disconnect left edge\n\nUse the screwdriver to remove all screws connecting the pipe to the upstream valve.");
	m_descriptions.Add(Pipe_StateRegistry.Step.DisconnectRight, "Disconnect right edge\n\nUse the screwdriver to remove all screws connecting the pipe to the downstream valve.");
	m_descriptions.Add(Pipe_StateRegistry.Step.GetNewPipe, "Get new pipe\n\nPlace a new pipe, where the old one was, to replace it.");	
	m_descriptions.Add(Pipe_StateRegistry.Step.ConnectRight, "Connect right edge\n\nUse the screwdriver to connect the pipe to the downstream valve.");	
	m_descriptions.Add(Pipe_StateRegistry.Step.ConnectLeft, "Connect left edge\n\nUse the screwdriver to connect the pipe to the upstream valve.");
	m_descriptions.Add(Pipe_StateRegistry.Step.ConnectRelief, "Connect relief valve\n\nUse the wrench to install the relief valve on the pipe.");	
	m_descriptions.Add(Pipe_StateRegistry.Step.OpenDownStream, "Open downstream valve\n\nOpen the downstream valve by by rotating the handle to the left, twice.\n\nThis action will allow steam to flow normally when the upstream valve opens.");	
	m_descriptions.Add(Pipe_StateRegistry.Step.OpenUpStream, "Open upstream valve\n\nOpen the upstream valve by by rotating the handle to the left, twice.\n\nThis action will allow steam to flow in from upstream.");
		
	m_titles = new Dictionary<Pipe_StateRegistry.Step, String>();
		
	foreach (KeyValuePair<Pipe_StateRegistry.Step, String> kvp in m_descriptions)
	{
		m_titles.Add(kvp.Key, kvp.Value.Split('\n')[0]);	
	}
			
	m_markers = new Dictionary<Pipe_StateRegistry.Step, Transform>();
		
	m_markers.Add(Pipe_StateRegistry.Step.ReadDocs, null);
	m_markers.Add(Pipe_StateRegistry.Step.WearGear, null);
	m_markers.Add(Pipe_StateRegistry.Step.CloseUpstream, markerUp);
	m_markers.Add(Pipe_StateRegistry.Step.CloseDownstream, markerDown);
	m_markers.Add(Pipe_StateRegistry.Step.OpenRelief, markerRelief);
	m_markers.Add(Pipe_StateRegistry.Step.DisconnectRelief, markerRelief);
	m_markers.Add(Pipe_StateRegistry.Step.CheckPressure, markerUp);
	m_markers.Add(Pipe_StateRegistry.Step.CheckTemperature, markerDown);
	m_markers.Add(Pipe_StateRegistry.Step.DisconnectLeft, markerUp);
	m_markers.Add(Pipe_StateRegistry.Step.DisconnectRight, markerDown);
	m_markers.Add(Pipe_StateRegistry.Step.GetNewPipe, markerRelief);	
	m_markers.Add(Pipe_StateRegistry.Step.ConnectRight, markerDown);
	m_markers.Add(Pipe_StateRegistry.Step.ConnectLeft, markerUp);
	m_markers.Add(Pipe_StateRegistry.Step.ConnectRelief, markerRelief);	
	m_markers.Add(Pipe_StateRegistry.Step.OpenDownStream, markerDown);	
	m_markers.Add(Pipe_StateRegistry.Step.OpenUpStream, markerUp);	
		
	m_actions = new Dictionary<Pipe_StateRegistry.Step, Action>();
		
	m_actions.Add(Pipe_StateRegistry.Step.ReadDocs, new Action(DisplayDocs));
	m_actions.Add(Pipe_StateRegistry.Step.WearGear, new Action(DisplayGear));
	m_actions.Add(Pipe_StateRegistry.Step.CloseUpstream, new Action(CloseValveUpstream));
	m_actions.Add(Pipe_StateRegistry.Step.CloseDownstream, new Action(CloseValveDownStream));
	m_actions.Add(Pipe_StateRegistry.Step.OpenRelief, new Action(OpenReliefValve));
	m_actions.Add(Pipe_StateRegistry.Step.DisconnectRelief, new Action(DisconnectReliefValve));
	m_actions.Add(Pipe_StateRegistry.Step.CheckPressure, new Action(CheckPressure));
	m_actions.Add(Pipe_StateRegistry.Step.CheckTemperature, new Action(CheckTemperature));
	m_actions.Add(Pipe_StateRegistry.Step.DisconnectLeft, new Action(DisconnectLeft));
	m_actions.Add(Pipe_StateRegistry.Step.DisconnectRight, new Action(DisconnectRight));
	m_actions.Add(Pipe_StateRegistry.Step.GetNewPipe, new Action(GetNewPipe));
	m_actions.Add(Pipe_StateRegistry.Step.ConnectRight, new Action(ConnectRight));	
	m_actions.Add(Pipe_StateRegistry.Step.ConnectLeft, new Action(ConnectLeft));
	m_actions.Add(Pipe_StateRegistry.Step.ConnectRelief, new Action(ConnectReliefValve));
	m_actions.Add(Pipe_StateRegistry.Step.OpenDownStream, new Action(OpenDownstreamValve));	
	m_actions.Add(Pipe_StateRegistry.Step.OpenUpStream, new Action(OpenUpstreamValve));	
		
	m_currIndex = Pipe_StateRegistry.Step.ReadDocs;
		
	AppData.AllowTracking = (m_markers[m_currIndex] != null);	
			
	valveUp.FindChild("Screw_br").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.DisconnectRight);
	valveUp.FindChild("Screw_mr").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.DisconnectRight);
	valveUp.FindChild("Screw_fr").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.DisconnectRight);
		
	valveUp.FindChild("Screw_br").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectRelief);
	valveUp.FindChild("Screw_mr").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectRelief);
	valveUp.FindChild("Screw_fr").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectRelief);
		
	valveDown.FindChild("Screw_bl").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.GetNewPipe);	
	valveDown.FindChild("Screw_ml").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.GetNewPipe);
	valveDown.FindChild("Screw_fl").GetComponent<Screw>().RegisterOutOfSight(Pipe_StateRegistry.Step.GetNewPipe);
		
	valveDown.FindChild("Screw_bl").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectLeft);
	valveDown.FindChild("Screw_ml").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectLeft);
	valveDown.FindChild("Screw_fl").GetComponent<Screw>().RegisterStartPosition(Pipe_StateRegistry.Step.ConnectLeft);
			
	HandleTracking();	
		
	m_Gui_CMMS = transform.GetComponent<Gui_CMMS>();	
}
	
	
private void HandleTracking()
{
	if (!AppData.AllowTracking)
	{
		TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Stop();
			
		Camera.main.GetComponent<ResetCameraHandle>().Reset();	
			
		piping.GetComponent<PipingHandle>().OutOfSight();	
	}
	else
	{
		TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Start();	
	}
}
		
	
void OpenUpstreamValve()
{
	Pipe_ValveHandle vh = valveUp.FindChild("Handle").GetComponent<Pipe_ValveHandle>();		
	
	vh.Open(new Action(EndCallback));		
}
	
	
void EndCallback()
{
	DoneCallback();
		
	AppData.GuiStuff_DisplayEndMsg = true;
		
	m_Gui_CMMS.ClearMiddle();	
}
	
	
void OpenDownstreamValve()
{
	Pipe_ValveHandle vh = valveDown.FindChild("Handle").GetComponent<Pipe_ValveHandle>();		
	
	vh.Open(new Action(DoneCallback));	
}
	

void ConnectReliefValve()
{
	valveRelief.GetComponent<ReliefHandle>().GoDown(new Action(ScrewReliefValve));	
}
	
	
void ScrewReliefValve()
{
	Transform wrench = tools.FindChild("Wrench");	
	
	wrench.GetComponent<Pipe_WrenchHandle>().Unscrew(new Action(DoneWrench), middlePipe.FindChild("pos_relief").position);			
}
		
	
void ConnectLeft()
{
	m_connectLeft = true;
	m_canScrew = true;
	m_whichScrew = 1;
}
	
	
void ConnectRight()
{
	middlePipe.GetComponent<MiddlePipeHandle>().Lift(ScrewRight);		
}
	
	
void ScrewRight()
{
	m_connectRight = true;	
	m_canScrew = true;	
	m_whichScrew = 1;	
}
	
	
void GetNewPipe()
{
	middlePipe.GetComponent<MiddlePipeHandle>().Replace(DoneCallback);
}
	
	
void DisplayDocs()
{		
	Transform manual = tools.FindChild("Manual");
	Transform schematics = tools.FindChild("Schematics");
	//Transform cover = tools.FindChild("Cover");		
		
	//cover.GetComponent<GearHandle>().ShowAsBackGround(Screen.width * 0.5f, Screen.height * 0.52f);	
	manual.GetComponent<DocumentHandle>().ShowOnMiddleOfScreen(Screen.width * 0.4f);	
	schematics.GetComponent<DocumentHandle>().ShowOnMiddleOfScreen(Screen.width * 0.62f);
		
	DoneCallback();
}
	
	
void DisplayGear()
{
	Transform boots = tools.FindChild("Boots");
	Transform goggles = tools.FindChild("Goggles");	
	Transform gloves = tools.FindChild("Gloves");	
	Transform suit = tools.FindChild("Suit");	
	//Transform cover = tools.FindChild("Cover");	
		
	//cover.GetComponent<GearHandle>().ShowAsBackGround(Screen.width * 0.5f, Screen.height * 0.52f);		
	boots.GetComponent<GearHandle>().ShowOnMiddleOfScreen(Screen.width * 0.35f);
	goggles.GetComponent<GearHandle>().ShowOnMiddleOfScreen(Screen.width * 0.45f);	
	gloves.GetComponent<GearHandle>().ShowOnMiddleOfScreen(Screen.width * 0.55f);	
	suit.GetComponent<GearHandle>().ShowOnMiddleOfScreen(Screen.width * 0.65f);	
		
	DoneCallback();
}
	
	
private void RestoreState(Pipe_StateRegistry.Step step)
{
	AppData.AllowTracking = (m_markers[m_currIndex] != null);	
		
	m_connectLeft = false;
	m_connectRight = false;
		
	m_disconnectLeft = false;
	m_disconnectRight = false;
		
	Pipe_StateRegistry.RestoreState(step);	
		
	HandleTracking();	
				
	m_displayPlayBtn = true;
	m_displayMsgBox = false;
	m_displayingInfo = true;
	m_displayingSteps = false;	
	
	AppData.GuiStuff_DisplayEndMsg = false;
}
	
	
public void OnTrackableChange()
{
	bool tmp = AppData.GuiStuff_DisplayEndMsg;
		
	RestoreState(m_currIndex);
		
	if (m_displayMsgBox && ((m_markers[m_currIndex] == null) || (TrackedRegistry.Tracked_ID == m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID)))
		m_displayMsgBox = false;

	if (!m_displayMsgBox)
		m_displayPlayBtn = true;	
		
	AppData.GuiStuff_DisplayEndMsg = tmp;
}

	
void Update()
{
	if (m_connectRight)
	{
		if (m_canScrew)
		{
			switch (m_whichScrew)
			{	
				case 1: { 
							valveDown.FindChild("Screw_bl").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveDown, true, "bl"); 
							m_canScrew = false; 
							break; 
						}
				case 2: { 
							valveDown.FindChild("Screw_ml").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveDown, true, "ml"); 
							m_canScrew = false; 
							break; 
						}	
				case 3: { 
							valveDown.FindChild("Screw_fl").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveDown, true, "fl"); 
							m_canScrew = false; 
							break; 
						}
				case 4: {
							OutOfSight(tools.FindChild("Screwdriver"));
							DoneCallback();	
							m_connectRight = false; 
							m_canScrew = false; 
							break; 
						}
				default : break;	
			}
		}
	}
		
	if (m_connectLeft)
	{
		if (m_canScrew)
		{
			switch (m_whichScrew)
			{	
				case 1: { 
							valveUp.FindChild("Screw_br").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveUp, true, "br"); 
							m_canScrew = false; 
							break; 
						}
				case 2: { 
							valveUp.FindChild("Screw_mr").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveUp, true, "mr"); 
							m_canScrew = false; 
							break; 
						}	
				case 3: { 
							valveUp.FindChild("Screw_fr").GetComponent<Screw>().SetToEndPosition();
							ScrewDrive(valveUp, true, "fr"); 
							m_canScrew = false; 
							break; 
						}
				case 4: {
							OutOfSight(tools.FindChild("Screwdriver"));
							DoneCallback();	
							m_connectLeft = false; 
							m_canScrew = false; 
							break; 
						}
				default : break;	
			}	
		}
	}
		
	if (m_relieving)
	{
		m_reliefTimer -= Time.deltaTime;
			
		if (m_reliefTimer <= 0)
		{
			valveRelief.FindChild("Particle System").particleSystem.Stop();
			middlePipe.FindChild("Particle System").particleSystem.Stop();
				
			m_reliefTimer = RELIEF_TIME;
			m_relieving = false;
				
			DoneCallback();
		}
	}
		
		
	if (m_disconnectLeft)
	{
		if (m_canScrew)
		{
			switch (m_whichScrew)
			{
				case 1: { 
							ScrewDrive(valveUp, false, "br"); 
							m_canScrew = false; 
							break; 
						}
				case 2: { 
							OutOfSight(valveUp.FindChild("Screw_br")); 
							ScrewDrive(valveUp, false, "mr"); 
							m_canScrew = false; 
							break; 
						}	
				case 3: { 
							OutOfSight(valveUp.FindChild("Screw_mr"));
							ScrewDrive(valveUp, false, "fr"); 
							m_canScrew = false; 
							break; 
						}
				case 4: { 
							OutOfSight(valveUp.FindChild("Screw_fr"));
							OutOfSight(tools.FindChild("Screwdriver"));
							m_disconnectLeft = false; 
							m_canScrew = false; 
							DoneCallback(); 
							break; 
						}
				default: break;
			}	
		}
	}
		
		
	if (m_disconnectRight)
	{
		if (m_canScrew)
		{
			switch (m_whichScrew)
			{
				case 1: { 
							ScrewDrive(valveDown, false, "bl"); 
							m_canScrew = false; 
							break; 
						}
				case 2: { 
							OutOfSight(valveDown.FindChild("Screw_bl"));
							ScrewDrive(valveDown, false, "ml"); 
							m_canScrew = false; 
							break; 
						}	
				case 3: { 
							OutOfSight(valveDown.FindChild("Screw_ml"));
							ScrewDrive(valveDown, false, "fl"); 
							m_canScrew = false; 
							break; 
						}
				case 4: {
							OutOfSight(valveDown.FindChild("Screw_fl"));
							OutOfSight(tools.FindChild("Screwdriver"));
							middlePipe.GetComponent<MiddlePipeHandle>().Fall(DoneCallback);
							m_disconnectRight = false; 
							m_canScrew = false; 
							break; 
						}
				default : break;
			}	
		}	
	}
		
	if (!m_Gui_CMMS.Get_ConfirmQuit() && !m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_arrow();	
	}
}		
	
	
public void DoneCallback()
{
	m_displayPlayBtn = true;	
}
	
	
void CloseValveUpstream()
{
	Pipe_ValveHandle vh = valveUp.FindChild("Handle").GetComponent<Pipe_ValveHandle>();		
	
	vh.Close(new Action(DoneCallback));
}

	
void CloseValveDownStream()
{
	Pipe_ValveHandle vh = valveDown.FindChild("Handle").GetComponent<Pipe_ValveHandle>();		
	
	vh.Close(new Action(DoneCallback));			
}
	
	
void OpenReliefValve()
{
	valveRelief.FindChild("Particle System").particleSystem.Play();
	
	valveRelief.FindChild("group_7").GetComponent<Animation>().Play();	
		
	m_relieving = true;
}
	
	
void DisconnectReliefValve()
{
	Transform wrench = tools.FindChild("Wrench");	
	
	wrench.GetComponent<Pipe_WrenchHandle>().Unscrew(new Action(DoneWrenchReliefOff), middlePipe.FindChild("pos_relief").position);
}	
	
	
void DoneWrenchReliefOff()
{
	Transform wrench = tools.FindChild("Wrench");
	valveRelief.parent = null;
		
	OutOfSight(wrench);
	OutOfSight(valveRelief);	
		
	DoneCallback();
}


void DoneWrench()
{
	Transform wrench = tools.FindChild("Wrench");
		
	OutOfSight(wrench);
		
	DoneCallback();
}
	
	
void CheckPressure()
{
	Transform manometer = tools.FindChild("manometer");
		
	manometer.position = valveUp.FindChild("pos_meter").position;
	manometer.parent = valveUp;
		
	manometer.GetComponent<ManometerHandle>().GoDown(ScrewManometer);	
}
			
			
void ScrewManometer()
{
	Transform wrench = tools.FindChild("Wrench");
		
	Transform cyl = valveUp.FindChild("Cylinder_vert");
	float y = cyl.renderer.bounds.size.y;	
		
	wrench.GetComponent<Pipe_WrenchHandle>().Unscrew(new Action(DoneWrench), cyl.position + new Vector3(0, y * 0.4f, 0));				
}
				

void CheckTemperature()
{
	Transform thermometer = tools.FindChild("Thermometer");
		
	thermometer.position = valveDown.FindChild("pos_meter").position;
	thermometer.parent = valveDown;
		
	thermometer.GetComponent<ThermometerHandle>().GoDown(ScrewThermometer);
}
	
	
void ScrewThermometer()
{
	Transform wrench = tools.FindChild("Wrench");
		
	Transform cyl = valveDown.FindChild("Cylinder_vert");
	float y = cyl.renderer.bounds.size.y;	
		
	wrench.GetComponent<Pipe_WrenchHandle>().Unscrew(new Action(DoneWrench), cyl.position + new Vector3(0, y * 0.4f, 0));		
}	
	
	
void DisconnectLeft()
{
	m_disconnectLeft = true;
	m_whichScrew = 1;
	m_canScrew = true;
}
	

void DisconnectRight()
{
	m_disconnectRight = true;
	m_whichScrew = 1;
	m_canScrew = true;
}
	
	
void OutOfSight(Transform t)
{
	t.position = tools.FindChild("pos_outOfSight").position;
}


void ScrewDrive(Transform valve, bool screwIn, String name)
{
	Transform screwdriver = tools.FindChild("Screwdriver");
	
	String screwName = "Screw_" + name;
	Transform screw = valve.FindChild(screwName);
		
	float dist = screw.GetComponent<Screw>().Distance;
		
	String pos = "pos_" + name + "_screwdriver";	
	screwdriver.position = valve.FindChild(pos).position;
			
	screwdriver.eulerAngles = screw.eulerAngles;
	
	if (screwIn)
	{
		screwdriver.GetComponent<Screw>().CalculateScrewDirection();
			
		screwdriver.Translate(Vector3.up * dist, Space.Self); 
	}
		
	
		
	if (screwIn)
	{
		screwdriver.GetComponent<Screw>().Approach(new Action<Transform, Transform>(ScrewDrvIn), screw, 2 * dist);		
	}
	else
	{	
		screwdriver.GetComponent<Screw>().Approach(new Action<Transform, Transform>(ScrewDrvOut), screw, 2 * dist);		
	}
}
	
	
void ScrewDrvOut(Transform screwdriver, Transform screw)
{
	screwdriver.GetComponent<Screw>().UnscrewMe(new Action(DoNothing));
	screw.GetComponent<Screw>().UnscrewMe(new Action(ScrewDriveCallback));
}
	
	
void ScrewDrvIn(Transform screwdriver, Transform screw)
{
	screwdriver.GetComponent<Screw>().ScrewMe(new Action(DoNothing));
	screw.GetComponent<Screw>().ScrewMe(new Action(ScrewDriveCallback));	
}	
	
	
void ScrewDriveCallback()
{
	m_whichScrew++;
	m_canScrew = true;	
		
	Transform screwdriver = tools.FindChild("Screwdriver");
	screwdriver.GetComponent<Screw>().RestoreStartPosition();
}
	

void DoNothing(){}	

	
void OnGUI() 
{		
	GUI.skin = guiSkin;	
		
	int s = (int)(Screen.width * 0.015625);	

	Font x = font20;
		
	if (s <= 9)
		x = font8;	
	else if ((s > 9) && (s <= 11))
		x = font10;
	else if ((s > 11) && (s <= 13))
		x = font12;
	else if ((s > 13) && (s <= 15))
		x = font14;
	else if ((s > 15) && (s <= 17))
		x = font16;
	else if ((s > 17) && (s <= 19))
		x = font16;
	else if ((s > 19) && (s <= 21))
		x = font18;
	else if ((s > 21) && (s <= 23))
		x = font20;
	else if ((s > 23) && (s <= 25))
		x = font22;
	else if (s > 25)
		x = fontHD;
		
	GUI.skin.button.font = 	x;
	GUI.skin.label.font = x;
	GUI.skin.font = x;
	GUI.skin.box.font = x;
		
	if (!m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_Title();	
	}
		
	if (!m_Gui_CMMS.Get_ConfirmQuit() && !m_Gui_CMMS.Get_ViewingFullscreen())
	{
		GUI_std();
		
		if (m_displayingSteps)
			GUI_steps();
		
		if (!m_displayingSteps)
			GUI_info();
		
		if (m_displayMsgBox)
			GUI_msgBox();
		
		if (AppData.GuiStuff_DisplayEndMsg)
			GUI_EndMsg();
	}
		
	if (AppData.GuiStuff_ClearMiddle_NEEDED)
	{
		ClearMiddle();
			
		AppData.GuiStuff_ClearMiddle_NEEDED = false;
	}
}
	
	
void GUI_EndMsg()
{
	m_Gui_CMMS.ClearMiddle();	

	GUIContent msg_gc = new GUIContent(" The steam pipe replacement \n process is complete!", m_info_Img);	
	Vector2 msg_size = guiSkin.GetStyle("Box").CalcSize(msg_gc);	
		
	Rect msgRect = new Rect(
			(Screen.width * 0.5f) - ((msg_size.x + 20) * 0.5f), 
			(Screen.height * 0.5f) - ((msg_size.y + 20) * 0.5f), 
			msg_size.x + 20, 
			msg_size.y + 20);		
		
	GUI.Box(msgRect, msg_gc);
	
	GUIContent restart_gc = new GUIContent(" Restart ", m_restart_Img);
	Vector2 restart_size = guiSkin.GetStyle("Button").CalcSize(restart_gc);
		
	Rect restartRect = new Rect(
			msgRect.x + (msgRect.width * 0.5f) - (Screen.width * GUI_MARGIN_PCNT * 0.5f) - (restart_size.x + 10), 
			msgRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f), 
			restart_size.x + 10, 
			restart_size.y + 10);
	
	if (GUI.Button(restartRect, restart_gc))
	{
		AppData.GuiStuff_DisplayEndMsg = false;
			
		m_currIndex = Pipe_StateRegistry.Step.ReadDocs;
			
		RestoreState(m_currIndex);
	}
	
	GUIContent ok_gc = new GUIContent(" OK", m_ok_Img);
	Vector2 ok_size = guiSkin.GetStyle("Button").CalcSize(ok_gc);
		
	Rect okRect = new Rect(
			msgRect.x + (msgRect.width * 0.5f) + (Screen.width * GUI_MARGIN_PCNT * 0.5f), 
			msgRect.yMax + (Screen.height * GUI_MARGIN_PCNT * 0.5f), 
			restartRect.width, 
			ok_size.y + 10);
		
	if (GUI.Button(okRect, ok_gc))
	{
		AppData.GuiStuff_DisplayEndMsg = false;	
	}
}
	
	
void GUI_msgBox()
{
	if (GUI.Button(new Rect(Screen.width * 0.4f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.1f), m_msg))
	{
		m_displayMsgBox = false;
		m_displayPlayBtn = true;
	}
}
	
	
void GUI_arrow()
{
	if (m_displayMsgBox)
	{
		if ((m_markers[m_currIndex] != null) && (TrackedRegistry.Tracked_ID != TrackedRegistry.INVALID_ID))
		{
			Camera cam = GameObject.Find("ARCamera").camera;
	
			arrow.position = cam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.35f, cam.nearClipPlane * 4));
				
			arrow.LookAt(m_markers[m_currIndex]);	
		}
		else
		{
			OutOfSight(arrow);	
		}
	}
	else
	{
		OutOfSight(arrow);	
	}
}
	
	
void GUI_Title()
{
	Rect titleRect = new Rect(Screen.width * 0.4f, Screen.height * 0.02f, Screen.width * 0.2f, Screen.height * 0.05f);
		
	GUI.Box(titleRect, "Steam Pipe Replacement");		
}
	
	
private Rect m_stepsRect;
	
void GUI_std()
{
	Rect playRect = new Rect(Screen.width * 0.45f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.067f);
	Rect quitRect = new Rect((Screen.width * (1 - GUI_MARGIN_PCNT)) - Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.067f);	
	Rect prevRect = new Rect(Screen.width * GUI_MARGIN_PCNT, Screen.height * 0.1f, Screen.width * 0.04f, Screen.height * 0.067f);
	Rect nextRect = new Rect(prevRect.xMax, Screen.height * 0.1f, Screen.width * 0.04f, Screen.height * 0.067f);
	m_stepsRect = new Rect(nextRect.xMax, Screen.height * 0.1f, Screen.width * 0.075f, Screen.height * 0.067f);
	Rect avatarLabelRect = new Rect(Screen.width * GUI_MARGIN_PCNT, Screen.height * (1 - GUI_MARGIN_PCNT - 0.04f), Screen.width * 0.14f, Screen.height * 0.04f);
	Rect avatarRect = new Rect(Screen.width * (GUI_MARGIN_PCNT + 0.03f), avatarLabelRect.y - (Screen.height * 0.12f), Screen.width * 0.08f, Screen.height * 0.12f);
		
			
	if ((m_currIndex > Pipe_StateRegistry.Step.WearGear) && !m_displayingSteps)
	{
		GUI.Box(avatarRect, new GUIContent(m_avatar_Img));
		GUI.Box(avatarLabelRect, new GUIContent("Protective gear: ON"));
	}
		

	GUI.enabled = (m_currIndex != Pipe_StateRegistry.Step.ReadDocs);
		
	if (GUI.Button(prevRect, new GUIContent(arrowBlueLeft)))
	{
		m_currIndex--;	
			
		RestoreState(m_currIndex);
	}
		
	GUI.enabled = true;	
		
		
	GUI.enabled = (m_currIndex != Pipe_StateRegistry.Step.OpenUpStream);	
	
	if (GUI.Button(nextRect, new GUIContent(arrowBlue)))
	{
		m_currIndex++;
			
		RestoreState(m_currIndex);
	}
	
	GUI.enabled = true;	
		
	if (GUI.Button(m_stepsRect, new GUIContent("  Steps", m_displayingSteps ? arrowDown : arrowUp)))
	{
		m_displayingSteps = !m_displayingSteps;		
	}
		
	if (m_displayPlayBtn)
	{
		GUI.enabled = !m_Gui_CMMS.Get_ShowingPopup();
			
		if (GUI.Button(playRect, new GUIContent("  Show me!", m_playBlue_Img)))
		{
			m_Gui_CMMS.ClearMiddle();
				
			if ((m_markers[m_currIndex] == null) || TrackedRegistry.HasTrackable(m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID))
			{
				RestoreState(m_currIndex);
				m_displayPlayBtn = false;
				//m_displayingInfo = false; //only for PC-Test
				m_actions[m_currIndex].Invoke();	
			}
			else
			{
				m_msg = "Need to track marker " + m_markers[m_currIndex].GetComponent<MarkerBehaviour>().Marker.MarkerID + " first!";
				m_displayMsgBox = true;	
				m_displayPlayBtn = false;	
			}	
		}
			
		GUI.enabled = true;	
	}
		
//	if (GUI.Button(quitRect, new GUIContent("  Quit", quit)))
//	{
//		Application.Quit();		
//	}
}

		
private void ClearMiddle()
{
	AppData.GuiStuff_DisplayEndMsg = false;	
	m_displayMsgBox = false;
	m_displayPlayBtn = true;		
}
	
	
void GUI_steps()
{
	Rect r1 = new Rect(Screen.width * 0.05f, Screen.height * 0.2f, Screen.width * 0.25f, Screen.height * 0.1f);	
		
	foreach (KeyValuePair<Pipe_StateRegistry.Step, String> kvp in m_titles)	
	{
		int i = (int)kvp.Key;	
			
		if (GUI.Button(
				new Rect(Screen.width * GUI_MARGIN_PCNT, m_stepsRect.yMax + (i * (r1.height / 2)), r1.width, r1.height / 2), 
				(m_currIndex == kvp.Key) ? new GUIContent("  Step " + (i + 1) + ": " + kvp.Value, m_redButton_Img) :
				new GUIContent("Step " + (i + 1) + ": " + kvp.Value)))
		{
			m_currIndex = kvp.Key;
				
			RestoreState(m_currIndex);
		}
	}
}
	

void GUI_info()
{
	Rect r1 = new Rect(Screen.width * GUI_MARGIN_PCNT, Screen.height * 0.25f, Screen.width * 0.25f, Screen.height * 0.067f);
	Rect r2 = new Rect(r1.x, r1.y + r1.height + Screen.height * 0.01f, r1.width, Screen.height * 0.4f);
	Rect r3 = new Rect(r2.x + Screen.width * 0.02f, r2.y + Screen.height * 0.02f, r2.width - (2 * Screen.width * 0.02f), r2.height - (2 * Screen.height * 0.02f));
		
	if (GUI.Button(r1, new GUIContent("  Step " + (int)(m_currIndex + 1) + ": " + m_titles[m_currIndex], m_displayingInfo ? arrowDown : arrowUp)))
	{
		m_displayingInfo = !m_displayingInfo;	
	}
		
	if (m_displayingInfo)
	{
		GUI.Box(r2, new GUIContent());		
			
		GUI.Label(r3, m_descriptions[m_currIndex]);		
	}	
}

	
}//class
