using UnityEngine;
using System.Collections;
using System;


public class ThermometerHandle : MonoBehaviour {

	private Vector3 m_startPosition;
	private Transform m_startParent;
	
	public Transform posMeter;
	public Transform valveDown;
	
	private Vector3 m_endPosition;
	
	private Action m_goDown_Action;
	
	
void Start() 
{
	m_startPosition = transform.position;
	m_startParent = transform.parent;
	
	Stop();
		
	CalculateEndPosition();	
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.DisconnectLeft, new Action(Step_DisconnectLeft_State));
}


public void GoDown(Action action)
{
	transform.parent = valveDown;
	
	transform.localPosition = m_endPosition;
		
	m_goDown_Action = action;
		
	GoingDown = true;	
}
	
	
private void Stop()
{
	GoingDown = false;
}
	
	
private bool GoingDown
{
	get;
	set;
}	
	
	
void Update()
{
	if (GoingDown)
	{
		transform.Translate(Vector3.down * 0.4f * Time.deltaTime, Space.Self);
			
		GoingDown = (transform.localPosition.y > posMeter.localPosition.y);
			
		if (!GoingDown)
		{
			m_goDown_Action.Invoke();	
		}
	}		
}
	

public void CalculateEndPosition()
{
	Vector3 tmp = transform.position;
	
	transform.position = posMeter.position;	
	transform.parent = valveDown;	
		
	float y = transform.FindChild("thermo_inner").FindChild("group_2").renderer.bounds.size.y;
		
	transform.Translate(Vector3.up * y, Space.Self);
		
	m_endPosition = transform.localPosition;
	
	transform.parent = m_startParent;	
	transform.position = tmp;		
}
	
	
void Step_Init_State()
{
	Stop();	
	transform.parent = m_startParent;
	transform.position = m_startPosition;		
}
	
	
void Step_DisconnectLeft_State()
{
	Stop();	
	transform.position = posMeter.position;
	transform.parent = valveDown;	
}
	
	
}//class
