using UnityEngine;
using System.Collections;
using System;


public class ReliefHandle : MonoBehaviour {

	private Vector3 m_startPosition;
	private Transform m_startParent;
	private Vector3 m_startAngles;
	
	private Vector3 m_endPosition;
	
	private Animation m_anim;
	
	private Action m_goDown_Action;
	
	
void Start() 
{
	m_startPosition = transform.localPosition;
	m_startParent = transform.parent;
	m_startAngles = transform.localEulerAngles;	
		
	m_anim = transform.FindChild("group_7").animation;
		
	Stop();	
		
	CalculateEndPosition();		
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Step_Init_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.CheckPressure, new Action(Step_CheckPress_State));
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.OpenDownStream, new Action(Step_Init_State));	
}
	
		
public void GoDown(Action action)
{
	m_goDown_Action = action;
		
	Stop();	
	
	transform.parent = m_startParent;
		
	transform.localEulerAngles = m_startAngles;	
		
	transform.localPosition = m_endPosition;	
		
	m_goDown_Action = action;
		
	GoingDown = true;	
}


public void CalculateEndPosition()
{
	Vector3 tmp = transform.position;
	
	float z = transform.renderer.bounds.size.z;
		
	transform.Translate(Vector3.forward * z, Space.Self);
		
	m_endPosition = transform.localPosition;
	
	transform.position = tmp;		
}

	
void Update()
{
	if (GoingDown)
	{
		transform.Translate(Vector3.back * 0.2f * Time.deltaTime, Space.Self);
			
		GoingDown = (transform.localPosition.x > m_startPosition.x);
			
		if (!GoingDown)
		{
			m_goDown_Action.Invoke();	
		}
	}
}
	

private void Stop()
{
	GoingDown = false;
		
	m_anim.Stop();	
			
	transform.FindChild("Particle System").particleSystem.Stop();	
}
	
	
private bool GoingDown
{
	get;
	set;
}
	
	
public void Step_Init_State()
{
	Stop();	
		
	transform.parent = m_startParent;
	transform.localPosition = m_startPosition;
	transform.localEulerAngles = m_startAngles;	
}
	
	
void Step_CheckPress_State()
{
	Stop();	
		
	transform.parent = null;	
	transform.position = GameObject.Find("/Scene/Tools/pos_outOfSight").transform.position;		
}
	
	
}//class
