using UnityEngine;
using System.Collections;
using System;


public class Pipe_ValveHandle : MonoBehaviour {
	
	private Action m_action;	
	
	private int m_timesPlayed;
	
	
void Start() 
{
	m_timesPlayed = 0;
		
	Pipe_StateRegistry.Register(transform, Pipe_StateRegistry.Step.ReadDocs, new Action(Stop));
}
	
	
public void Stop()
{
	m_timesPlayed = 0;
	
	GetComponent<Animation>().Stop();			
}
	
	
void OnCloseFinished()
{
	m_action.Invoke();
}
	
	
void OnAnimationFinished()
{
	m_timesPlayed++;
		
	if (m_timesPlayed % 2 == 0)
		OnCloseFinished();
}
	

public void Close(Action action)
{
	m_action = action;
		
	Animation an = GetComponent<Animation>();
	
	an.PlayQueued("RotateZaxisRight", QueueMode.PlayNow);
	an.PlayQueued("RotateZaxisRight", QueueMode.CompleteOthers);
}
	
	
public void Open(Action action)
{
	m_action = action;
		
	Animation an = GetComponent<Animation>();
	
	an.PlayQueued("RotateZaxisLeft", QueueMode.PlayNow);
	an.PlayQueued("RotateZaxisLeft", QueueMode.CompleteOthers);
}
		
	
}//class
